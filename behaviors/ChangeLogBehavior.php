<?php

namespace app\behaviors;

use Yii;
use yii\base\Behavior;
use yii\db\ActiveRecord;

class ChangeLogBehavior extends Behavior
{
    public const TABLE_NAME = 'sys_change_log';

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_UPDATE => 'addChangeLog',
            ActiveRecord::EVENT_BEFORE_DELETE => 'deleteChangeLogs',
        ];
    }

    public function addChangeLog($event)
    {
        $changedAttributes = $this->owner->getDirtyAttributes();
        $userId = Yii::$app->user ? (Yii::$app->user->id ?? null) : null;

        foreach ($changedAttributes as $attribute => $newValue) {

            if (in_array($attribute, ['updated_by', 'updated_at'])) {
                continue;
            }

            $oldValue = $this->owner->getOldAttribute($attribute);

            if ($oldValue != $newValue) {
                Yii::$app->db->createCommand()->insert(self::TABLE_NAME, [
                    'table_name' => $this->owner::tableName(),
                    'record_id' => $this->owner->getPrimaryKey(),
                    'attribute' => $attribute,
                    'old_value' => (string)$oldValue,
                    'new_value' => (string)$newValue,
                    'updated_by' => $userId,
                    'updated_at' => date('Y-m-d H:i:s'),
                ])->execute();
            }
        }
    }

    public function deleteChangeLogs($event)
    {
        Yii::$app->db->createCommand()->delete(self::TABLE_NAME, [
            'table_name' => $this->owner::tableName(),
            'record_id' => $this->owner->getPrimaryKey(),
        ])->execute();
    }
}