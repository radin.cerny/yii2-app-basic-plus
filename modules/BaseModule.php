<?php

namespace app\modules;

use Yii;

class BaseModule extends \yii\base\Module
{
    public function init()
    {
        if ($this->controllerNamespace === null) {
            $class = get_class($this);
            if (($pos = strrpos($class, '\\')) !== false) {
                $ctrlFolder = 'controllers';
                if (Yii::$app instanceof \yii\console\Application) {
                    $ctrlFolder = 'commands';
                } else if (Yii::$app instanceof \yii\web\Application) {
                    $ctrlFolder = 'controllers';
                }
                $this->controllerNamespace = substr($class, 0, $pos) . '\\' . $ctrlFolder;
            }
        }
    }
}