<?php

namespace app\modules\api\controllers;

use app\modules\user\models\User;
use Yii;
use yii\helpers\StringHelper;
use yii\rest\Controller;
use yii\web\UnauthorizedHttpException;

/**
 * Default controller for the `api` module
 */
class DefaultController extends Controller
{
    public function actionIndex()
    {
        return [1, 2, 3];
    }

    public function actionUpload()
    {
        return [1, 2, 3];
    }

    public function actionDownload()
    {
        return [1, 2, 3];
    }

    public function actionLogin()
    {
        $username = Yii::$app->request->post('username');
        $password = Yii::$app->request->post('password');

        $user = User::findByEmail($username);
        if (!$user || !$user->validateApiPassword($password)) {
            return ['error' => 'Invalid credentials'];
        }

        $expiration = 3600;
        $user->generateApiAccessToken($expiration, true);

        return ['access_token' => $user->api_access_token, 'expires_in_seconds' => $expiration];
    }

    public function actionTestBearer()
    {
        $authorizationHeader = Yii::$app->request->headers->get('Authorization');
        if (!StringHelper::startsWith($authorizationHeader, 'Bearer ')) {
            throw new UnauthorizedHttpException('Missing token.');
        }

        $apiAccessToken = str_replace('Bearer ', '', $authorizationHeader);

        $user = User::findIdentityByAccessToken($apiAccessToken);

        if (!$user) {
            throw new UnauthorizedHttpException('Invalid token.');
        }

        if ($user->api_access_token_expire < date('Y-m-d H:i:s')) {
            $user->removeApiAccessToken();
            throw new UnauthorizedHttpException('Expired token.');
        }

        return ['message' => 'Success'];

    }
}
