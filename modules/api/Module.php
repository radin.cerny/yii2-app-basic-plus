<?php

namespace app\modules\api;

use app\modules\BaseModule;
use Yii;
use yii\base\BootstrapInterface;
use yii\filters\Cors;

class Module extends BaseModule implements BootstrapInterface
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\api\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        Yii::$app->request->parsers = [
            'application/json' => 'yii\web\JsonParser'
        ];

        parent::init();

        // custom initialization code goes here
    }

    public function behaviors()
    {
        return [
            'corsFilter' => [
                // https://www.yiiframework.com/doc/api/2.0/yii-filters-cors
                'class' => Cors::class,
            ],
        ];
    }

    /**
     * Why bootstrapping the API module?
     * It the env variable BACKEND_ONLY=true then folder "controllers" is not used and all requests are redirected to modules/admin
     * But in case of module API this is not the desired behavior.
     * We want this module to be always available using URL myweb.com/api
     * Modules Gii and Debug do the same and they work at all the time. I just copied this idea.
     * Bootstrapping has its own independent URL rules
     * {@inheritdoc}
     */
    public function bootstrap($app)
    {
        if ($app instanceof \yii\web\Application) {
            $app->getUrlManager()->addRules([
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/login', 'route' => $this->id . '/default/login'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/download', 'route' => $this->id . '/default/download'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/upload', 'route' => $this->id . '/default/upload'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/uploadPhoto', 'route' => $this->id . '/default/uploadPhoto'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id, 'route' => $this->id . '/default/index'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>', 'route' => $this->id . '/<controller>'],
                ['class' => 'yii\web\UrlRule', 'pattern' => $this->id . '/<controller:[\w\-]+>/<action:[\w\-]+>', 'route' => $this->id . '/<controller>/<action>'],
            ], false);
        }
    }
}
