<?php

namespace app\modules\message\components;

use app\helpers\FileHelper;
use Yii;
use yii\base\BaseObject;

/**
 * If your components do not need events and behaviors, you may consider extending your component class from yii\base\BaseObject instead of yii\base\Component
 * Class MessageComponent
 * @package app\modules\message\components
 */
abstract class BaseSender extends BaseObject
{
    /**
     * Any string than can be used as the address of the recipient.
     * A phone number, email etc.
     * @var string
     */
    public string $from = '';

    /**
     * Each type of message has different folder. For example "welcome" (after registration).
     * @var string
     */
    public string $template = '';

    /**
     * Each template (above) has subfolders for different channels:
     * - welcome/email
     * - welcome/whatsapp
     * - etc
     * @var string
     */
    public $templateChannelSubfolder = '';

    /**
     * Goes to each view-file
     * @var string[]
     */
    public array $templateData = [];

    /**
     * Language of the template
     * @var string
     */
    public string $lang = 'en';

    /**
     * [
     *   ['role' => 'TO',  'to' => 'me@gmail.com'],
     *   ['role' => 'BCC', 'to' => 'you@gmail.com'],
     * ]
     * @var array
     */
    public array $messageRecipients = [];

    abstract public function send();

    protected function renderTemplates(string $template, string $templateChannelSubfolder, string $lang, array $templateData)
    {
        $origLang = Yii::$app->language;
        Yii::$app->language = $lang;

        $result = [];
        $templateFolder = Yii::getAlias(Yii::$app->messenger->templateFolder) . DIRECTORY_SEPARATOR . $template . DIRECTORY_SEPARATOR . $templateChannelSubfolder;
        $templates = FileHelper::findFiles($templateFolder, ['only' => ['*.php'], 'recursive' => false]);
        foreach ($templates as $filePath) {
            $key = FileHelper::getFilename($filePath, true);
            $result[$key] = Yii::$app->view->renderFile($filePath, $templateData);
        }

        Yii::$app->language = $origLang;
        return $result;
    }


}
