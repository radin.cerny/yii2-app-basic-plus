<?php

namespace app\modules\message\components;

use Yii;
use yii\base\BaseObject;

/**
 * If your components do not need events and behaviors, you may consider extending your component class from yii\base\BaseObject instead of yii\base\Component
 * Class MessageComponent
 * @package app\modules\message\components
 */
class WhatsappSender extends BaseObject
{

    /**
     * Each type of message has different folder. For example "welcome" (after registration).
     * @var string
     */
    public string $templateFolder = '';

    /** Goes to each view-file
     * @var string[]
     */
    public array $templateData = [];

    /**
     * @var array
     */
    public array $messageRecipients = [];

    /**
     * @var string
     */
    public string $lang = 'en';

    /**
     * @var string
     */
    public string $from = 'noreply@noreply.com';

    /**
     * This folder exist in $this->templateFolder
     * @var string
     */
    private $templateSubfolderName = 'email';

    /**
     * These templates will be used and required.
     *
     * @var string[]
     */
    private $templateList = [
        // Currently only default text is sent by Whatsapp servers
    ];

    public function send()
    {

        $views = $this->renderTemplates();

        foreach ($this->messageRecipients as $recipient) {
            Yii::$app->whatsapp->send($recipient['to'], 'hello_world', 'en_US');
        }

        return $views;
    }


    private function renderTemplates()
    {
        $origLang = Yii::$app->language;
        Yii::$app->language = $this->lang;

        $result = [];
        foreach ($this->templateList as $filename) {
            $result[$filename] = Yii::$app->view->renderFile(Yii::getAlias($this->templateFolder) . DIRECTORY_SEPARATOR . $this->templateSubfolderName . DIRECTORY_SEPARATOR . $filename . '.php', $this->templateData);
        }

        Yii::$app->language = $origLang;
        return $result;
    }


}
