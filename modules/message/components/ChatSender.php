<?php

namespace app\modules\message\components;

/**
 * If your components do not need events and behaviors, you may consider extending your component class from yii\base\BaseObject instead of yii\base\Component
 * Class MessageComponent
 * @package app\modules\message\components
 */
class ChatSender extends BaseSender
{
    /**
     * @var string
     */
    public $templateChannelSubfolder = 'chat';

    public function send()
    {
        return $this->renderTemplates($this->template, $this->templateChannelSubfolder, $this->lang, $this->templateData);
    }

}
