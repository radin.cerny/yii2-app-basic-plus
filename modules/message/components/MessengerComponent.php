<?php

namespace app\modules\message\components;

use app\modules\message\models\Message;
use yii\base\BaseObject;
use yii\helpers\Json;

/**
 * If your components do not need events and behaviors, you may consider extending your component class from yii\base\BaseObject instead of yii\base\Component
 * Class MessageComponent
 * @package app\modules\message\components
 */
class MessengerComponent extends BaseObject
{
    public $templateFolder = '@app/modules/message/templates';

    /**
     * @param string $template
     * @param array $templateData
     * @param array $channelConfig
     * @return Message
     * @throws \yii\db\Exception
     */
    public function email(string $template, array $templateData = [], array $channelConfig = [])
    {
        return $this->createNewMessage(EmailSender::class, $template, $templateData, $channelConfig);
    }

    public function chat(string $template, array $templateData = [], array $channelConfig = [])
    {
        return $this->createNewMessage(ChatSender::class, $template, $templateData, $channelConfig);
    }

    public function notification(string $template, array $templateData = [], array $channelConfig = [])
    {
        return $this->createNewMessage(NotificationSender::class, $template, $templateData, $channelConfig);
    }

    /**
     * @param $channel
     * @param $template
     * @param $templateData
     * @param $channelConfig
     * @return Message
     * @throws \yii\db\Exception
     */
    private function createNewMessage($channel, $template, $templateData, $channelConfig)
    {
        $message = new Message();
        $message->template = $template;
        $message->template_data_json = Json::encode($templateData);
        $message->channel_class = $channel;
        $message->channel_config_json = Json::encode($channelConfig);
        $message->save();
        return $message;
    }
}
