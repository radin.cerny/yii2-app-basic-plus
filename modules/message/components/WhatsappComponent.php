<?php

namespace app\modules\message\components;

use Yii;
use yii\base\Component;
use yii\base\Exception;

/**
 * Usage in web.php:
 * 'whatsapp' => [
 *   'class' => app\modules\message\components\WhatsappComponent::class,
 *   'id' => env('WHATSAPP_SENDER_PHONE_ID'),
 *   'token' => env('WHATSAPP_ACCESS_TOKEN'),
 *   'version' => env('WHATSAPP_API_VERSION'),
 *   'useFileTransport' => false, // if True, messages are not sent, but are saved to runtime/whatsapp
 * ],
 * Note: Special setup is needed in your FB profile: https://developers.facebook.com/apps
 *
 * Class WhatsappComponent
 * @package app\modules\message\components
 */
class WhatsappComponent extends Component
{
    const MSG_TEMPLATE_NEW_DATE = 'hello_world';
    const MSG_TEMPLATE_TAKE_OVER = 'hello_world';
    public string $id;

    // TODO: Templates do not exist yet. Probably only "Take over" will be needed.
    public string $token;
    public string $version;
    /**
     * Mechanism for saving to the runtime folder was copied from file
     * /vendor/yiisoft/yii2/mail/BaseMailer.php
     *
     * @var string the directory where the email messages are saved when [[useFileTransport]] is true.
     */
    public $fileTransportPath = '@runtime/whatsapp';

    /**
     * @var bool whether to save messages as files under [[fileTransportPath]] instead of sending them
     * to the actual recipients. This is usually used during development for debugging purpose.
     * @see fileTransportPath
     */
    public $useFileTransport = false;

    /**
     * @link https://business.facebook.com/select ... then menu "Accounts/WhatsApp Accounts/Settings/WhatsApp Manager/Template Manager"
     * @link https://business.facebook.com/wa/manage/message-templates .. this should go directly to the Template Manager
     *
     * @param $to Example: '420605123456'
     * @param $template Example: 'hello_world' - The template must exist on your business FB account (links above)
     * @param $language Example: 'en_US'
     * @throws Exception
     */
    public function send($to, $template, $language, $parameters = [])
    {

        $options = [];

        // Example of URL: 'https://graph.facebook.com/v17.0/1024xxx3089/messages'
        $options[CURLOPT_URL] = "https://graph.facebook.com/$this->version/$this->id/messages";
        $options[CURLOPT_RETURNTRANSFER] = 1; // do not automatically print returned data!
        $options[CURLOPT_POST] = 1;
        $options[CURLOPT_HTTPHEADER] = [
            'Authorization: Bearer ' . $this->token,
            'Content-Type: application/json',
        ];
        $options[CURLOPT_POSTFIELDS] = [
            "messaging_product" => "whatsapp",
            "to" => $to,
            "type" => "template",
            "template" => [
                "name" => $template,
                "language" => [
                    "code" => $language
                ]
            ],
            'parameters' => $parameters,
        ];

        if ($this->useFileTransport) {
            return $this->saveMessage(\json_encode($options, JSON_PRETTY_PRINT));
        }

        $options[CURLOPT_POSTFIELDS] = \json_encode($options[CURLOPT_POSTFIELDS]);
        $ch = curl_init();
        curl_setopt_array($ch, $options);

        $result = curl_exec($ch);

        if (curl_errno($ch)) {
            $msg = 'curl_errno:' . curl_errno($ch) . '; curl_error:' . curl_error($ch);
            Yii::error($result);
            Yii::error($msg);
            throw new \Exception($msg);
        }
        curl_close($ch);

        $resultDecoded = \json_decode($result);
        if (isset($resultDecoded->error)) {
            Yii::error($result);
        }
    }

    /**
     * Saving the message as string to the runtime folder
     *
     * @param $message
     * @return bool
     */
    protected function saveMessage($message)
    {
        $path = Yii::getAlias($this->fileTransportPath);
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        $file = $path . '/' . $this->generateMessageFileName();
        file_put_contents($file, $message);

        return true;
    }


}
