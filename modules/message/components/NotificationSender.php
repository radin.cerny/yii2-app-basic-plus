<?php

namespace app\modules\message\components;

/**
 * If your components do not need events and behaviors, you may consider extending your component class from yii\base\BaseObject instead of yii\base\Component
 * Class MessageComponent
 * @package app\modules\message\components
 */
class NotificationSender extends ChatSender
{
    /**
     * @var string
     */
    public $templateChannelSubfolder = 'notification';

}
