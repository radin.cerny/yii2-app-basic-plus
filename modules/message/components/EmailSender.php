<?php

namespace app\modules\message\components;

use app\modules\message\models\MessageRecipient;
use Yii;

/**
 * If your components do not need events and behaviors, you may consider extending your component class from yii\base\BaseObject instead of yii\base\Component
 * Class MessageComponent
 * @package app\modules\message\components
 */
class EmailSender extends BaseSender
{

    public $templateChannelSubfolder = 'email';

    public function send()
    {
        $views = $this->renderTemplates($this->template, $this->templateChannelSubfolder, $this->lang, $this->templateData);

        $to = [];
        $cc = [];
        $bcc = [];
        foreach ($this->messageRecipients as $recipient) {
            switch ($recipient['role']) {
                case MessageRecipient::ROLE_TO:
                    $to[] = $recipient['to'];
                    break;
                case MessageRecipient::ROLE_CC:
                    $cc[] = $recipient['to'];
                    break;
                case MessageRecipient::ROLE_BCC:
                    $bcc[] = $recipient['to'];
                    break;
            }
        }

        Yii::$app->mailer->compose()
            ->setFrom($this->from)
            ->setTo($to)
            ->setCc($cc)
            ->setBcc($bcc)
            ->setSubject($views['subject'])
            ->setTextBody($views['body_text'])
            ->setHtmlBody($views['body_html'])
            ->send();

        return $views;
    }

}
