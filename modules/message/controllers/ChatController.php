<?php

namespace app\modules\message\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\message\components\ChatSender;
use app\modules\message\models\ChatForm;
use app\modules\message\models\search\MessageRecipientSearch;
use app\modules\user\models\User;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * ChatController shows all chat messages
 */
class ChatController extends BaseController
{

    /**
     * Lists all MessageRecipient models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MessageRecipientSearch();

        $dataProvider = $searchModel->search($this->request->queryParams, ChatSender::class, Yii::$app->user->identity->id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Message-model including the related MessageRecipients
     * @return string|\yii\web\Response
     */
    public function actionCreateMessage()
    {
        $model = new ChatForm();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->validate()) {
                Yii::$app->messenger
                    ->chat('freetext', ['text' => $model->text])
                    ->setSender(User::findOne($model->id_sender))
                    ->addRecipientTo(User::findOne($model->id_recipient))
                    ->send();
                return $this->redirect(['index']);
            }
        }

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('create-message', [
                'model' => $model,
                'allUsers_listData' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
            ]);
        }

        return $this->render('create-message', [
            'model' => $model,
            'allUsers_listData' => ArrayHelper::map(User::find()->all(), 'id', 'username'),
        ]);
    }
}
