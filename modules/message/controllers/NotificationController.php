<?php

namespace app\modules\message\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\message\components\NotificationSender;
use app\modules\message\models\search\MessageRecipientSearch;
use Yii;

/**
 * NotificationController shows all notifications
 */
class NotificationController extends BaseController
{

    /**
     * Lists all MessageRecipient models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new MessageRecipientSearch();

        $dataProvider = $searchModel->search($this->request->queryParams, NotificationSender::class, Yii::$app->user->identity->id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
}
