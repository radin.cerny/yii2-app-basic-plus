<?php

use app\migrations\BaseMigration;
use app\modules\message\models\Message;

/**
 * Class m241023_093000_message
 */
class m241023_093000_message extends BaseMigration
{
    public $createColumnUserId = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // id_user = sender
        $this->createTable(Message::tableName(), [
            'template' => $this->text()->comment('Template is either a path to an existing folder that contains many templates for many languages or a particular text that will be sent. Both can contain {variables} that will be provided via "data_json"'),
            'template_data_json' => $this->text('Data for the template'),
            'from' => $this->string()->comment('Address of the sender'), // Can be auto detected? ... later
            'channel_class' => $this->string()->comment('Classpath of the Sender object. It will be instantiated and channel_config will be injected'),
            'channel_config_json' => $this->text()->defaultValue('[]')->comment('Default configuration for the Sender object (channel_class)'),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Message::tableName());
    }

}
