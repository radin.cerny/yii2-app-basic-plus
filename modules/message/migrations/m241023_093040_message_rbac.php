<?php

use app\modules\message\controllers\MessageController;
use app\modules\message\controllers\MessageRecipientController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m241023_093040_message_rbac
 */
class m241023_093040_message_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            MessageController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter', 'canAccessAll'],
                ],
            ],
            MessageRecipientController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate', 'actionCopy'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter', 'canAccessAll'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        RbacHelper::deleteAuthItemsForController(MessageController::class);
        RbacHelper::deleteAuthItemsForController(MessageRecipientController::class);
    }
}
