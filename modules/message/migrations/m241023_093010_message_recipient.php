<?php

use app\migrations\BaseMigration;
use app\modules\message\models\Message;
use app\modules\message\models\MessageRecipient;

/**
 * Class m241023_093010_message_recipient
 */
class m241023_093010_message_recipient extends BaseMigration
{
    public $createColumnUserId = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // id_user = recipient
        $this->createTable(MessageRecipient::tableName(), [
            'id_message' => $this->integer()->notNull(),
            'to' => $this->string()->comment('Message will be sent to this address.'), // Can be auto detected? ... later
            'role' => $this->string()->comment('Default is TO. But can be CC or BCC in case of emails. All recipients of one Message will receive one common group-message.'),
            'template_output_json' => $this->text()->comment('All templates rendered'),
            'sent_at' => $this->dateTime(),
            'seen_at' => $this->dateTime()->comment('Some types of messages (internal app notifications or messages) can have this info'),
        ]);

        $this->addForeignKey('fk-message-id', MessageRecipient::tableName(), ['id_message'], Message::tableName(), ['id'], 'NO ACTION', 'NO ACTION',);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(MessageRecipient::tableName());
    }

}
