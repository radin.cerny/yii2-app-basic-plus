<?php

use app\modules\intl\models\Translation;
use app\modules\intl\models\TranslationSource;
use yii\db\Migration;

/**
 * Class m241023_093020_message_translations
 */
class m241023_093020_message_translations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(TranslationSource::tableName(), ['category', 'message'], [
            ['app.message', 'Message Recipients'],
            ['app.message', 'Create Message Recipient'],
            ['app.message', 'Template'],
            ['app.message', 'Template Data Json'],
            ['app.message', 'From'],
            ['app.message', 'Channel Class'],
            ['app.message', 'Channel Config Json'],
            ['app.message', 'Message ID'],
            ['app.message', 'To'],
            ['app.message', 'Template Output'],
            ['app.message', 'Sent At'],
            ['app.message', 'Seen At'],
            ['app.message', 'Update Message Recipient: {name}'],
            ['app.message', 'Create Message'],
            ['app.message', 'Messages'],
            ['app.message', 'Update Message: {name}'],
            ['app.message', 'Role'],

        ]);

        // getLastInsertID() returns ID of the first batch-inserted row!
        // INFO: If getLastInsertID() returns incorrect number after batchInsert() to the 1st table,
        // ... insert only one row in batchInsert(), then call getLastInsertID() and then insert the rest using batchInsert()
        // 2nd table should be always OK
        $firstId = $this->db->getLastInsertID();

        $id = $firstId;

        // Order must be identical to order in the previous batchInsert()
        $this->batchInsert(Translation::tableName(), ['id', 'language', 'translation'], [
            [$id++, 'cs', 'Příjemci zpráv'],
            [$id++, 'cs', 'Vytvořit příjemce'],
            [$id++, 'cs', 'Předloha'],
            [$id++, 'cs', 'Json data pro předlohu'],
            [$id++, 'cs', 'Od'],
            [$id++, 'cs', 'Odesílací třída'],
            [$id++, 'cs', 'Konfigurace odeslání'],
            [$id++, 'cs', 'ID zprávy'],
            [$id++, 'cs', 'Pro'],
            [$id++, 'cs', 'Výstup předlohy'],
            [$id++, 'cs', 'Odesláno'],
            [$id++, 'cs', 'Přečteno'],
            [$id++, 'cs', 'Upravit příjemce: {name}'],
            [$id++, 'cs', 'Vytvořit zprávu'],
            [$id++, 'cs', 'Zprávy'],
            [$id++, 'cs', 'Upravit zprávu: {name}'],
            [$id++, 'cs', 'Role'],
        ]);

        $id = $firstId;

        // Order must be identical to order in the previous batchInsert()
        $this->batchInsert(Translation::tableName(), ['id', 'language', 'translation'], [
            [$id++, 'de', 'Nachrichtenempfänger'],
            [$id++, 'de', 'Nachrichtenempfänger erstellen'],
            [$id++, 'de', 'Vorlage'],
            [$id++, 'de', 'Vorlagedaten JSON'],
            [$id++, 'de', 'Von'],
            [$id++, 'de', 'Kanal-Klasse'],
            [$id++, 'de', 'Kanal-Konfiguration JSON'],
            [$id++, 'de', 'Nachrichten-ID'],
            [$id++, 'de', 'An'],
            [$id++, 'de', 'Vorlagenausgabe'],
            [$id++, 'de', 'Gesendet am'],
            [$id++, 'de', 'Gesehen am'],
            [$id++, 'de', 'Nachrichtenempfänger aktualisieren: {name}'],
            [$id++, 'de', 'Nachricht erstellen'],
            [$id++, 'de', 'Nachrichten'],
            [$id++, 'de', 'Nachricht aktualisieren: {name}'],
            [$id++, 'de', 'Rolle'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";

        return false;
    }

}
