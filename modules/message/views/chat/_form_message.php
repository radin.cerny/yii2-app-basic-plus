<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\message\models\Message $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $allUsers_listData */
?>

<div class="message-form">

    <?php Pjax::begin([
        //'id' => 'user-detail-Pjax',
        //'enablePushState' => false,
        //'enableReplaceState' => false,
    ]); ?>

    <?php $form = ActiveForm::begin([
        //'id' => 'user-detail-ActiveForm',
        //'enableAjaxValidation' => false,
        //'enableClientValidation' => false,
        //'options' => ['data-pjax' => 1]
    ]); ?>

    <?= $form->field($model, 'id_sender')->dropDownList($allUsers_listData, [
        'prompt' => '— ' . Yii::t('app', 'Select a sender') . ' —',
    ]) ?>

    <?= $form->field($model, 'id_recipient')->dropDownList($allUsers_listData, [
        'prompt' => '— ' . Yii::t('app', 'Select a recipient') . ' —',
    ]) ?>

    <?= $form->field($model, 'text')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php Pjax::end(); ?>

</div>
