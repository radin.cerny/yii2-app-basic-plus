<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\message\models\Message $model */
/** @var array $allUsers_listData */

$this->title = Yii::t('app.message', 'Create Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.message', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_message', [
        'model' => $model,
        'allUsers_listData' => $allUsers_listData,
    ]) ?>

</div>
