<?php

use app\modules\message\models\MessageRecipient;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\message\models\search\MessageRecipientSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app.message', 'Message Recipients');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="message-recipient-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('message\MessageRecipient.actionCreate')): ?>
        <p>
            <?= Html::a(Yii::t('app.message', 'Create Message Recipient'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?php Pjax::begin([/*'id'=>'pjaxId'*/]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('id', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'id_user',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('id_user', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'id_message',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('id_message', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'to',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('to', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'role',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('role', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'template_output_json',
                'format' => 'ntext',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('template_output_json', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'sent_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('sent_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'seen_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('seen_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'created_by',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('created_by', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'updated_by',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('updated_by', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'deleted_by',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('deleted_by', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'created_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('created_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'updated_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('updated_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'deleted_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('deleted_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, MessageRecipient $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 8rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                //'template' => '{view} {update} {delete} {copy}',
                //'buttons' => [
                //    'copy' => function ($url, $model, $key) {
                //        return Html::a(
                //            '<i class="mdi mdi-content-copy"></i>',
                //            $url,
                //            [
                //                'title' => Yii::t('app', 'Copy'),
                //                //'data-pjax' => '0',
                //            ]
                //       );
                //    },
                //],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('message\MessageRecipient.actionView');
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('message\MessageRecipient.actionUpdate');
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('message\MessageRecipient.actionDelete');
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>