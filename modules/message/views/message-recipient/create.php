<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\message\models\MessageRecipient $model */

$this->title = Yii::t('app.message', 'Create Message Recipient');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.message', 'Message Recipients'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-recipient-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>


</div>
