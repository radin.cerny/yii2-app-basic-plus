<?php

use app\modules\message\models\Message;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\message\models\search\MessageSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app.message', 'Messages');
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin([/*'id'=>'pjaxId'*/]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('id', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'id_user',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('id_user', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'template',
                'format' => 'ntext',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('template', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'template_data_json',
                'format' => 'ntext',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('template_data_json', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'from',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('from', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'channel_class',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('channel_class', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'channel_config_json',
                'format' => 'ntext',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('channel_config_json', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'format' => 'text',
                'label' => 'Recipients',
                //'filter' => ['id' => 'Some text'],
                //'filter' => Message::getFilterData('updated_by', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (Message $model, $key, $index, $column) {
                    return count($model->messageRecipients);
                },
            ],
//                [
//                    'attribute' => 'deleted_by',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filter' => Message::getFilterData('deleted_by', 'name'),
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'created_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filter' => Message::getFilterData('created_at', 'name'),
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'updated_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filter' => Message::getFilterData('updated_at', 'name'),
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'deleted_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filter' => Message::getFilterData('deleted_at', 'name'),
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, Message $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 8rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                //'template' => '{view} {update} {delete} {copy}',
                //'buttons' => [
                //    'copy' => function ($url, $model, $key) {
                //        return Html::a(
                //            '<i class="mdi mdi-content-copy"></i>',
                //            $url,
                //            [
                //                'title' => Yii::t('app', 'Copy'),
                //                //'data-pjax' => '0',
                //            ]
                //       );
                //    },
                //],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('message\Message.actionView');
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('message\Message.actionUpdate');
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('message\Message.actionDelete');
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>