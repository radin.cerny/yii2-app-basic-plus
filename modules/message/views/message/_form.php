<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\message\models\Message $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="message-form">

    <?php yii\widgets\Pjax::begin([
        //'id' => 'user-detail-Pjax',
        //'enablePushState' => false,
        //'enableReplaceState' => false,
    ]); ?>

    <?php $form = ActiveForm::begin([
        //'id' => 'user-detail-ActiveForm',
        //'enableAjaxValidation' => false,
        //'enableClientValidation' => false,
        //'options' => ['data-pjax' => 1]
    ]); ?>

    <?= $form->field($model, 'id_user')->textInput() ?>

    <?= $form->field($model, 'template')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'template_data_json')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'from')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'channel_class')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'channel_config_json')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'created_by')->textInput() ?>

    <?= $form->field($model, 'updated_by')->textInput() ?>

    <?= $form->field($model, 'deleted_by')->textInput() ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <?= $form->field($model, 'updated_at')->textInput() ?>

    <?= $form->field($model, 'deleted_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php //yii\widgets\Pjax::end(); ?>

</div>
