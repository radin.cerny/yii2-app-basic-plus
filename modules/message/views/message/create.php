<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\message\models\Message $model */

$this->title = Yii::t('app.message', 'Create Message');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.message', 'Messages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="message-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
