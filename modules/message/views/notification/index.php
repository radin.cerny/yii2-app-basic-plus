<?php

use app\modules\message\models\MessageRecipient;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\message\models\search\MessageRecipientSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app.message', 'Notifications');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="message-recipient-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin([/*'id'=>'pjaxId'*/]); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        //'filterModel' => $searchModel,
        'columns' => [
            [
                'format' => 'text',
                'label' => Yii::t('app', 'ID'),
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('id_message', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (MessageRecipient $model, $key, $index, $column) {
                    return $model->id_message . '.' . $model->id;
                },
            ],
            [
                'attribute' => 'message.user.username',
                'format' => 'text',
                'label' => Yii::t('app', 'From'),
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('id', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'to',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('to', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'role',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('role', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'template_output_json',
                'format' => 'html',
                'label' => Yii::t('app', 'Text'),
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('template_output_json', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (MessageRecipient $model, $key, $index, $column) {
                    return \yii\helpers\Json::decode($model->template_output_json)['body'] ?? null;
                },
            ],
            [
                'attribute' => 'sent_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('sent_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'seen_at',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filter' => MessageRecipient::getFilterData('seen_at', 'name'),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, MessageRecipient $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 8rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'template' => '{seen}',
                'buttons' => [
                    'seen' => function ($url, $model, $key) {
                        return Html::a(
                            '<i class="bi bi-check2-circle"></i>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Copy'),
                                //'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'visibleButtons' => [
                    'seen' => function (MessageRecipient $model, $key, $index) {
                        return empty($model->seen_at);
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>