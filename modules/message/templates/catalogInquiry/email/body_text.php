<?php

use yii\helpers\Html;

/** @var integer $id */
/** @var array $urlArray */
/** @var string $name */
/** @var string $propertyTitle */
/** @var string $phone */
/** @var string $email */
/** @var string $message */
/** @var string $created_at */
?>
You have a new inquiry at the <?= Yii::$app->params['webName'] ?> web:

-<?= Html::a($propertyTitle, $urlArray, ['target' => '_blank']) ?>

-<?= $name ?></li>

-<?= $phone ?></li>

-<?= $email ?></li>

-<?= $created_at ?></li>

<?= $message ?>
