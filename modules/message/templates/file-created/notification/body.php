<?php

/** @var string $jobName */
/** @var string $fileName */
/** @var int $documentUrl */
?>
You file `<?= $fileName ?>` has been created by job <?= $jobName ?>.
You can download it <?= \yii\helpers\Html::a('here', $documentUrl) ?>.
