<?php

namespace app\commands;

use app\modules\message\jobs\SendAllMessagesJob;
use app\modules\message\jobs\SendMessageJob;
use Yii;
use yii\console\Controller;

class CmdController extends Controller
{

    public function actionSendAllMessages()
    {
        Yii::$app->queue->push(new SendAllMessagesJob());
    }

    public function actionSendMessage(int $id)
    {
        Yii::$app->queue->push(new SendMessageJob([
            'id' => $id,
        ]));
    }
}
