<?php

namespace app\modules\message\jobs;

use app\modules\message\models\Message;
use yii\base\BaseObject;
use yii\queue\JobInterface;
use yii\web\NotFoundHttpException;

/**
 * Usage:
 * \Yii::$app->queue->push(new \app\modules\message\jobs\SendMessageJob());
 *
 * Class DemoQueueJob
 * @package app\jobs
 */
class SendMessageJob extends BaseObject implements JobInterface
{
    public int $idMessage;

    public function execute($queue)
    {
        if (!isset($this->idMessage)) {
            throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
        }

        $model = $this->findModel($this->idMessage);
        $model->send();

    }

    protected function findModel($id)
    {
        if (($model = Message::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(\Yii::t('app', 'The requested page does not exist.'));
    }
}
