<?php

namespace app\modules\message\jobs;

use app\modules\message\models\Message;
use app\modules\message\models\MessageRecipient;
use yii\base\BaseObject;
use yii\console\ExitCode;
use yii\queue\JobInterface;

class SendAllMessagesJob extends BaseObject implements JobInterface
{
    public function execute($queue)
    {
        $messagesToBeSent = [];
        /** @var MessageRecipient $recipient */
        foreach (MessageRecipient::find()->notSent()->all() as $recipient) {
            // Duplicates are no problem
            $messagesToBeSent[] = $recipient->id_message;
        }

        if (empty($messagesToBeSent)) {
            return ExitCode::OK;
        }

        /** @var Message $message */
        foreach (Message::find()->byId($messagesToBeSent)->all() as $message) {
            $message->send();
        }
    }
}
