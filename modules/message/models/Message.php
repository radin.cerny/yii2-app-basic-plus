<?php

namespace app\modules\message\models;

use app\models\BaseModel;
use app\modules\message\components\ChatSender;
use app\modules\message\components\EmailSender;
use app\modules\message\components\NotificationSender;
use app\modules\message\components\WhatsappSender;
use app\modules\message\jobs\SendMessageJob;
use app\modules\message\models\query\MessageQuery;
use app\modules\user\models\User;
use DateTime;
use Exception;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "{{%sys_message}}".
 *
 * @property int $id
 * @property int|null $id_user
 * @property string|null $template Template is either a path to an existing folder that contains many templates for many languages or a particular text that will be sent. Both can contain {variables} that will be provided via "data_json"
 * @property string|null $template_data_json
 * @property string|null $from Address of the sender
 * @property string|null $channel_class Classpath of the Sender object. It will be instantiated and channel_config will be injected
 * @property string|null $channel_config_json Default configuration for the Sender object (channel_class)
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property User $createdBy
 * @property User $deletedBy
 * @property MessageRecipient[] $MessageRecipients
 * @property User $updatedBy
 * @property User $user
 */
class Message extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sys_message}}';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\message\models\query\MessageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['template', 'template_data_json', 'channel_config_json'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['from', 'channel_class'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Sender ID'),
            'template' => Yii::t('app.message', 'Template'),
            'template_data_json' => Yii::t('app.message', 'Template Data Json'),
            'from' => Yii::t('app.message', 'From'),
            'channel_class' => Yii::t('app.message', 'Channel Class'),
            'channel_config_json' => Yii::t('app.message', 'Channel Config Json'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::class, ['id' => 'deleted_by']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    public function enqueue()
    {
        Yii::$app->queue->push(new SendMessageJob([
            'idMessage' => $this->id,
        ]));
    }

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function send()
    {
        $senderConfig = [
            'class' => $this->channel_class,
            'templateData' => Json::decode($this->template_data_json) ?? [],
            'template' => $this->template,
            'messageRecipients' => $this->getMessageRecipients()->asArray()->all(),
            'lang' => 'en',
            'from' => $this->from,
        ];
        $senderConfig = array_merge($senderConfig, Json::decode($this->channel_config_json) ?? []);
        $sender = Yii::createObject($senderConfig);

        $views = $sender->send();

        foreach ($this->messageRecipients as $recipient) {
            $recipient->sent_at = (new DateTime('now'))->format('Y-m-d H:i:s');
            $recipient->template_output_json = Json::encode($views);
            $recipient->save(false, ['sent_at', 'template_output_json']);
        }
    }

    /**
     * Gets query for [[MessageRecipients]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\MessageRecipientQuery
     */
    public function getMessageRecipients()
    {
        return $this->hasMany(MessageRecipient::class, ['id_message' => 'id']);
    }

    /**
     * @param $recipient
     * @throws Exception
     */
    public function addRecipientTo($recipient)
    {
        return $this->addRecipient(MessageRecipient::ROLE_TO, $recipient);
    }

    /**
     * @param $sender
     * @return $this
     * @throws \yii\db\Exception
     */
    public function setSender($sender)
    {
        $idUser = null;
        $from = $sender;
        if ($sender instanceof User) {
            $idUser = $sender->id;
            $from = $this->getContactByChannelClass($sender);
        }

        $this->id_user = $idUser;
        $this->from = $from;
        $this->save(false, ['id_user', 'from']);
        return $this;
    }

    public function getContactByChannelClass(User $user)
    {
        switch ($this->channel_class) {
            case EmailSender::class:
                return $user->email;
            case WhatsappSender::class:
                return $user->phone;
            case ChatSender::class:
            case NotificationSender::class:
                return $user->username;
        }
        throw new NotSupportedException('Sender class ' . $this->channel_class . ' does not have and contact defined in User.getContactBySender()');
    }

    /**
     * @param $recipient
     * @throws Exception
     */
    public function addRecipientCc($recipient)
    {
        return $this->addRecipient(MessageRecipient::ROLE_CC, $recipient);
    }

    /**
     * @param $recipient
     * @throws Exception
     */
    public function addRecipientBcc($recipient)
    {
        return $this->addRecipient(MessageRecipient::ROLE_BCC, $recipient);
    }

    /**
     * @param $role
     * @param $recipient
     * @return $this
     * @throws \yii\db\Exception
     */
    private function addRecipient($role, $recipient)
    {
        if ($this->getIsNewRecord()) {
            throw new Exception('Recipients can be only added to Messages that have already been saved.');
        }

        $idUser = null;
        $to = $recipient;
        if ($recipient instanceof User) {
            $idUser = $recipient->id;
            $to = $this->getContactByChannelClass($recipient);
        }

        $messageRecipient = new MessageRecipient();
        $messageRecipient->id_user = $idUser;
        $messageRecipient->id_message = $this->id;
        $messageRecipient->to = $to;
        $messageRecipient->role = $role;
        $messageRecipient->template_output_json = Json::encode([]);
        if (!$messageRecipient->save()) {
            throw new Exception("Recipient '$messageRecipient->to' with role '$role' could not be added to Message ID " . $this->id);
        }

        return $this;
    }

}
