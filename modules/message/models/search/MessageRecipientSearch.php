<?php

namespace app\modules\message\models\search;

use app\modules\message\models\MessageRecipient;
use app\modules\message\models\query\MessageQuery;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * MessageRecipientSearch represents the model behind the search form of `app\modules\message\models\MessageRecipient`.
 */
class MessageRecipientSearch extends MessageRecipient
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_user', 'id_message', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['to', 'role', 'template_output_json', 'sent_at', 'seen_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $channelClass, $idSenderRecipient)
    {
        $query1 = MessageRecipient::find();
        $query1->joinWith(['message' => function (MessageQuery $messageQuery) use ($channelClass) {
            $messageQuery->byChannelClass($channelClass);
            $messageQuery->with('user');
        }]);
        $query1->andFilterWhere([
            self::tableName() . '.id_user' => $idSenderRecipient,
        ]);

        $query2 = MessageRecipient::find();
        $query2->joinWith(['message' => function (MessageQuery $messageQuery) use ($channelClass, $idSenderRecipient) {
            $messageQuery->byChannelClass($channelClass);
            $messageQuery->bySender($idSenderRecipient);
            $messageQuery->with('user');
        }]);

        // Why union?
        // Because we are combining incoming and outgouing messages and both are obtained using slightly different parameters
        // But both queries return the same columns so they can be displayed in 1 GridView
        $unionQuery = MessageRecipient::find()
            ->from(['my_union' => $query1->union($query2)]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $unionQuery,
            'sort' => [
                'defaultOrder' => [
                    'sent_at' => SORT_DESC,
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}
