<?php

namespace app\modules\message\models;

use app\models\BaseModel;
use app\modules\message\models\query\MessageRecipientQuery;
use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%sys_message_recipient}}".
 *
 * @property int $id
 * @property int|null $id_user
 * @property int $id_message
 * @property string|null $to Message will be sent to this address.
 * @property string|null $role Default is TO. But can be CC or BCC in case of emails. All recipients of one Message will receive one common group-message.
 * @property string|null $template_output_json All templates rendered
 * @property string|null $sent_at
 * @property string|null $seen_at
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property User $createdBy
 * @property User $deletedBy
 * @property Message $message
 * @property User $updatedBy
 * @property User $user
 */
class MessageRecipient extends BaseModel
{
    const ROLE_TO = 'TO';
    const ROLE_CC = 'CC';
    const ROLE_BCC = 'BCC';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sys_message_recipient}}';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\message\models\query\MessageRecipientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new MessageRecipientQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_user', 'id_message', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['id_message'], 'required'],
            [['template_output_json'], 'string'],
            [['sent_at', 'seen_at', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['id_message'], 'exist', 'skipOnError' => true, 'targetClass' => Message::class, 'targetAttribute' => ['id_message' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_user' => Yii::t('app', 'Recipient ID'),
            'id_message' => Yii::t('app.message', 'Message ID'),
            'to' => Yii::t('app.message', 'To'),
            'role' => Yii::t('app.message', 'Role'),
            'template_output_json' => Yii::t('app.message', 'Template Output'),
            'sent_at' => Yii::t('app.message', 'Sent At'),
            'seen_at' => Yii::t('app.message', 'Seen At'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
        ];
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::class, ['id' => 'deleted_by']);
    }

    /**
     * Gets query for [[Message]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\MessageQuery
     */
    public function getMessage()
    {
        return $this->hasOne(Message::class, ['id' => 'id_message']);
    }

    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\message\models\query\UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

}
