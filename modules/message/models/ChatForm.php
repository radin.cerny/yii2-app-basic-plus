<?php

namespace app\modules\message\models;

use app\modules\user\models\User;
use yii\base\Model;

/**
 * ChatForm is the model for creating new chat message
 *
 * @property-read User|null $user
 *
 */
class ChatForm extends Model
{
    public $id_sender;
    public $id_recipient;
    public $text;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['id_sender'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_sender' => 'id']],
            [['id_recipient'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_recipient' => 'id']],
            [['id_sender', 'id_recipient', 'text'], 'required'],
        ];
    }

}
