<?php

namespace app\modules\message\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\message\models\MessageRecipient]].
 *
 * @see \app\modules\message\models\MessageRecipient
 */
class MessageRecipientQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\message\models\MessageRecipient[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\message\models\MessageRecipient|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function notSent()
    {
        return $this->andWhere(['sent_at' => null]);
    }
}
