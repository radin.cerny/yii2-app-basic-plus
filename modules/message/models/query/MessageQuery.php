<?php

namespace app\modules\message\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\message\models\Message]].
 *
 * @see \app\modules\message\models\Message
 */
class MessageQuery extends \yii\db\ActiveQuery
{
    public function byChannelClass($classPath)
    {
        [$alias, $tableName] = $this->getTableNameAndAlias();
        return $this->andWhere([$alias . '.channel_class' => $classPath]);
    }

    public function bySender($userId)
    {
        [$alias, $tableName] = $this->getTableNameAndAlias();
        return $this->andWhere([$alias . '.id_user' => $userId]);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\message\models\Message[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\message\models\Message|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }
}
