<?php

namespace app\modules\documents\models;

use app\models\BaseModel;
use app\modules\documents\models\query\DocumentGroupItemQuery;
use Yii;

/**
 * This is the model class for table "{{%document_group_item}}".
 *
 * @property int $id
 * @property int $document_group_id
 * @property int $document_id
 * @property int $order Values like 10000, 20000 etc will simplify reordering
 * @property int $status Visible, hidden, standard, highlighted etc
 * @property int is_cover The main document. Its thumbnail is the icon of the group. If more images have this flag, they can rotate.
 * @property int|null $active_from
 * @property string|null $hidden_from
 * @property string|null $title
 * @property string|null $description
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Document $document
 * @property DocumentGroup $documentGroup
 */
class DocumentGroupItem extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%document_group_item}}';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\documents\models\query\DocumentGroupItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentGroupItemQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['document_group_id', 'document_id', 'position'], 'required'],
            [['document_group_id', 'document_id', 'position', 'status'], 'integer'],
            [['active_from', 'hidden_from'], 'safe'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => Document::class, 'targetAttribute' => ['document_id' => 'id']],
            [['document_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentGroup::class, 'targetAttribute' => ['document_group_id' => 'id']],
            [['document_group_id'], 'validateDocGroupSize'],
        ];
    }

    public function validateDocGroupSize($attribute, $params)
    {
        if ($this->documentGroup->maxFilesReached()) {
            $this->addError($attribute, Yii::t('app.documents', 'Document group (gallery) cannot take more documents. The limit was reached.'));
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'document_group_id' => Yii::t('app.documents', 'Document Group ID'),
            'document_id' => Yii::t('app.documents', 'Document ID'),
            'status' => Yii::t('app', 'Status'),
            'active_from' => Yii::t('app.documents', 'Active from'),
            'hidden_from' => Yii::t('app.documents', 'Hidden from'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            // Default columns added by BaseMigration:
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'lang' => Yii::t('app', 'Language'),
            'position' => Yii::t('app', 'Position'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
        ];
    }

    /**
     * Gets query for [[Document]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\documents\models\query\DocumentQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Document::class, ['id' => 'document_id']);
    }

    /**
     * Gets query for [[DocumentGroup]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\documents\models\query\DocumentGroupQuery
     */
    public function getDocumentGroup()
    {
        return $this->hasOne(DocumentGroup::class, ['id' => 'document_group_id']);
    }
}
