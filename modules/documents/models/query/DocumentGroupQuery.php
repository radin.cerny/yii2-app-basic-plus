<?php

namespace app\modules\documents\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\documents\models\DocumentGroup]].
 *
 * @see \app\modules\documents\models\DocumentGroup
 */
class DocumentGroupQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\documents\models\DocumentGroup[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\documents\models\DocumentGroup|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
