<?php

namespace app\modules\documents\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\documents\models\DocumentGroupItem]].
 *
 * @see \app\modules\documents\models\DocumentGroupItem
 */
class DocumentGroupItemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\documents\models\DocumentGroupItem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\documents\models\DocumentGroupItem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function isCover()
    {
        return $this->andWhere(['is_cover' => 1]);
    }
}
