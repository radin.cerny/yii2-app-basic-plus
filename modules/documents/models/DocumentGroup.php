<?php

namespace app\modules\documents\models;

use app\models\BaseModel;
use app\modules\documents\models\query\DocumentGroupQuery;
use app\modules\user\models\User;
use Yii;

/**
 * This is the model class for table "{{%document_group}}".
 *
 * @property int $id
 * @property int $group_type
 * @property int $id_user
 * @property int $order Values like 10000, 20000 etc will simplify reordering
 * @property int $status Visible, hidden, standard, highlighted etc
 * @property int|null $active_from
 * @property string|null $hidden_from
 * @property string|null $title
 * @property string|null $description
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property DocumentGroupItem[] $documentGroupItems
 * @property User $user
 */
class DocumentGroup extends BaseModel
{
    public const TYPE_GALLERY = 1;

    public const STATUS_ACTIVE = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%document_group}}';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\documents\models\query\DocumentGroupQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DocumentGroupQuery(get_called_class());
    }

    public static function getType_listData()
    {
        return [
            self::TYPE_GALLERY => Yii::t('app.documents', 'Gallery'),
        ];
    }

    public static function getStatus_listData()
    {
        return [
            self::STATUS_ACTIVE => Yii::t('app', 'Active'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['group_type', 'position', 'status'], 'integer'],
            [['id_user'], 'required'],
            [['active_from', 'hidden_from'], 'safe'],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [['id_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['id_user' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'group_type' => Yii::t('app.documents', 'Group Type'),
            'id_user' => Yii::t('app', 'User ID'),
            'status' => Yii::t('app', 'Status'),
            'active_from' => Yii::t('app', 'Active from'),
            'hidden_from' => Yii::t('app.documents', 'Hidden from'),
            'title' => Yii::t('app.documents', 'Title'),
            'description' => Yii::t('app.documents', 'Description'),
            // Default columns added by BaseMigration:
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'lang' => Yii::t('app', 'Language'),
            'position' => Yii::t('app', 'Position'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
        ];
    }

    /**
     * Gets query for [[DocumentGroupItems]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\documents\models\query\DocumentGroupItemQuery
     */
    public function getItems()
    {
        return $this->hasMany(DocumentGroupItem::class, ['document_group_id' => 'id'])
            ->orderBy(['position' => SORT_ASC]);
    }

    public function getDocuments()
    {
        return $this->hasMany(Document::class, ['id' => 'document_id'])
            ->via('items');
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery|app\modules\admin\models\query\UserQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'id_user']);
    }

    public function maxFilesReached()
    {
        return count($this->items) >= $this->maxFiles();
    }

    /**
     * Returns max number of files in a group. If you need to change this value for certain groups, you can do in this method.
     * @return int
     */
    public function maxFiles()
    {
        return 20;
    }
}
