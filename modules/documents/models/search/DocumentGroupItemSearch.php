<?php

namespace app\modules\documents\models\search;

use app\modules\documents\models\DocumentGroupItem;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * DocumentGroupItemSearch represents the model behind the search form of `app\modules\documents\models\DocumentGroupItem`.
 */
class DocumentGroupItemSearch extends DocumentGroupItem
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'document_group_id', 'document_id', 'position', 'status'], 'integer'],
            [['active_from', 'hidden_from', 'title', 'description', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = DocumentGroupItem::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'document_group_id' => $this->document_group_id,
            'document_id' => $this->document_id,
            'position' => $this->position,
            'status' => $this->status,
            'active_from' => $this->active_from,
            'hidden_from' => $this->hidden_from,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'description', $this->description]);

        return $dataProvider;
    }
}
