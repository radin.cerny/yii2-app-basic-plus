<?php

use app\modules\documents\controllers\DocumentController;
use app\modules\documents\controllers\DocumentGroupController;
use app\modules\documents\controllers\DocumentGroupItemController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m240319_130000_rbac
 */
class m240319_130000_doc_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            DocumentController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate', 'actionDropzoneUpload'],
                    'reader' => ['actionGetUrl', 'actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
            DocumentGroupController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionGallery', 'actionIndex', 'actionView'],
                    'updater' => ['actionUpdate', 'actionSort'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
            DocumentGroupItemController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate', 'actionDropzoneUpload'],
                    'reader' => ['actionGetUrl', 'actionIndex', 'actionView'],
                    'updater' => ['actionUpdate', 'actionSetAsGroupCover', 'actionToggle'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";
        return false;
    }
}
