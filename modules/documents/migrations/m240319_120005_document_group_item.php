<?php

use app\migrations\BaseMigration;
use app\modules\documents\models\Document;
use app\modules\documents\models\DocumentGroup;
use app\modules\documents\models\DocumentGroupItem;

/**
 * Class m240319_120005_document_group_item
 */
class m240319_120005_document_group_item extends BaseMigration
{
    public $createColumnUserId = true;
    public $createColumnLang = true;
    public $createColumnPosition = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable(DocumentGroupItem::tableName(), [
            'document_group_id' => $this->integer()->notNull(),
            'document_id' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1)->comment('Visible, hidden, standard, highlighted etc'),
            'is_cover' => $this->integer()->notNull()->defaultValue(0)->comment('The main document. Its thumbnail is the icon of the group. If more images have this flag, they can rotate.'),
            'title' => $this->string(),
            'description' => $this->text(),
            'active_from' => $this->dateTime(),
            'hidden_from' => $this->dateTime(),
        ]);

        $this->addForeignKey('fk-document_group_item-document_group', DocumentGroupItem::tableName(), 'document_group_id', DocumentGroup::tableName(), 'id');
        $this->addForeignKey('fk-document_group_item-document', DocumentGroupItem::tableName(), 'document_id', Document::tableName(), 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(DocumentGroupItem::tableName());
    }
}
