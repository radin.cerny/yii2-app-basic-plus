<?php

use app\migrations\BaseMigration;
use app\modules\documents\models\Document;

/**
 * Class m240319_120003_document
 */
class m240319_120003_document extends BaseMigration
{
    public $createColumnUserId = true;
    public $createColumnLang = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Document::tableName(), [
            'name' => $this->string()->notNull()->comment('The original filename'),
            'extension' => $this->string()->notNull(),
            'public' => $this->tinyInteger()->notNull()->defaultValue(0)->comment('Public items are visible to all, non-public can have restrictions.'),
            'title' => $this->string()->comment('Custom title to be displayed to users. If empty, name will be used.'),
            'description' => $this->text()->comment('Custom long text that can describe the document.'),
            'bytes' => $this->integer()->notNull(),
            'mime' => $this->string()->notNull(),
            'width' => $this->integer()->comment('If the document is an image'),
            'height' => $this->integer()->comment('If the document is an image'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Document::tableName());
    }
}
