<?php

use app\migrations\BaseMigration;
use app\modules\documents\models\DocumentGroup;

/**
 * Class m240319_120004_document_group
 */
class m240319_120004_document_group extends BaseMigration
{
    public $createColumnUserId = true;
    public $createColumnLang = true;
    public $createColumnPosition = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable(DocumentGroup::tableName(), [
            'group_type' => $this->integer()->notNull(),
            'status' => $this->integer()->notNull()->defaultValue(1)->comment('Visible, hidden, standard, highlighted etc'),
            'title' => $this->string(),
            'public' => $this->tinyInteger()->notNull()->defaultValue(0)->comment('Public items are visible to all, non-public can have restrictions.'),
            'description' => $this->text(),
            'active_from' => $this->dateTime(),
            'hidden_from' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(DocumentGroup::tableName());
    }
}
