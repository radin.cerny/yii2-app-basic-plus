<?php

use app\modules\intl\models\Translation;
use app\modules\intl\models\TranslationSource;
use yii\db\Migration;

/**
 * Class m240319_120006_documents_translations
 */
class m240319_120006_documents_translations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(TranslationSource::tableName(), ['category', 'message'], [
            ['app.documents', 'User ID'],
            ['app.documents', 'Original Name'],
            ['app.documents', 'Extension'],
            ['app.documents', 'Title'],
            ['app.documents', 'Description'],
            ['app.documents', 'Bytes'],
            ['app.documents', 'Mime'],
            ['app.documents', 'Width'],
            ['app.documents', 'Height'],
            ['app.documents', 'Pick a file'],
            ['app.documents', 'Gallery'],
            ['app.documents', 'Group Type'],
            ['app.documents', 'Active from'],
            ['app.documents', 'Hidden from'],
            ['app.documents', 'Document Group ID'],
            ['app.documents', 'Document ID'],
            ['app.documents', 'Create Document Group Item'],
            ['app.documents', 'Document Group Items'],
            ['app.documents', 'Update Document Group Item: {name}'],
            ['app.documents', 'Create Document Group'],
            ['app.documents', 'Document Groups'],
            ['app.documents', 'Update Document Group: {name}'],
            ['app.documents', 'Create Document'],
            ['app.documents', 'Documents'],
            ['app.documents', 'Update Document: {name}'],
            ['app.documents', 'Add files'],
            ['app.documents', 'Start upload'],
            ['app.documents', 'Cancel upload'],
            ['app.documents', 'Filename'],
            ['app.documents', 'Enter title'],
            ['app.documents', 'Enter description'],
            ['app.documents', 'Processing'],
            ['app.documents', 'Drag and Drop Files Here'],
            ['app.documents', 'Show as gallery'],
            ['app.documents', 'Preview'],
            ['app.documents', 'Document group (gallery) cannot take more documents. The limit was reached.'],
            ['app.documents', 'Table'],
            ['app.documents', 'Layout'],
            ['app.documents', 'More'],
            ['app.documents', 'Filesize'],
            ['app.documents', 'Set as cover image'],
            ['app.documents', 'Public'],
            ['app.documents', 'Secured'],
            ['app.documents', 'Select a file'],
        ]);

        // getLastInsertID() returns ID of the first batch-inserted row!
        // INFO: If getLastInsertID() returns incorrect number after batchInsert() to the 1st table,
        // ... insert only one row in batchInsert(), then call getLastInsertID() and then insert the rest using batchInsert()
        // 2nd table should be always OK
        $firstId = $this->db->getLastInsertID();

        $id = $firstId;

        // Order must be identical to order in the previous batchInsert()
        $this->batchInsert(Translation::tableName(), ['id', 'language', 'translation',], [
            [$id++, 'cs', 'ID uživatele'],
            [$id++, 'cs', 'Originální jméno'],
            [$id++, 'cs', 'Přípona'],
            [$id++, 'cs', 'Nadpis'],
            [$id++, 'cs', 'Popis'],
            [$id++, 'cs', 'Bytů'],
            [$id++, 'cs', 'Mime'],
            [$id++, 'cs', 'Šířka'],
            [$id++, 'cs', 'Výška'],
            [$id++, 'cs', 'Zvolte soubor'],
            [$id++, 'cs', 'Galerie'],
            [$id++, 'cs', 'Druh skupiny'],
            [$id++, 'cs', 'Aktivní od'],
            [$id++, 'cs', 'Skrytý od'],
            [$id++, 'cs', 'ID skupiny dokumentů'],
            [$id++, 'cs', 'ID dokumentu'],
            [$id++, 'cs', 'Vytvořit položku skupiny dokumentů'],
            [$id++, 'cs', 'Položky skupiny dokumentů'],
            [$id++, 'cs', 'Upravit položku skupiny dokumentů: {name}'],
            [$id++, 'cs', 'Vytvořit skupinu dokumentů'],
            [$id++, 'cs', 'Skupiny dokumentů'],
            [$id++, 'cs', 'Upravit skupinu dokumentů: {name}'],
            [$id++, 'cs', 'Vytvořit dokument'],
            [$id++, 'cs', 'Dokumenty'],
            [$id++, 'cs', 'Upravit document: {name}'],
            [$id++, 'cs', 'Vybrat soubory'],
            [$id++, 'cs', 'Odeslat soubory'],
            [$id++, 'cs', 'Zrušit odesílání'],
            [$id++, 'cs', 'Jméno souboru'],
            [$id++, 'cs', 'Zadejte nadpis'],
            [$id++, 'cs', 'Zadejte popis'],
            [$id++, 'cs', 'Zpravovávám'],
            [$id++, 'cs', 'Přetáhněte sem soubory myší'],
            [$id++, 'cs', 'Zobrazit jako galerii'],
            [$id++, 'cs', 'Náhled'],
            [$id++, 'cs', 'Skupina dokumentů (galerie) nepojme více souborů. Limit dosažen.'],
            [$id++, 'cs', 'Tabulka'],
            [$id++, 'cs', 'Rozložení'],
            [$id++, 'cs', 'Více'],
            [$id++, 'cs', 'Velikost souboru'],
            [$id++, 'cs', 'Jako hlavní obrázek'],
            [$id++, 'cs', 'Veřejný'],
            [$id++, 'cs', 'Zabezpečený'],
            [$id++, 'cs', 'Zvolte soubor'],
        ]);

        $id = $firstId;

        // Order must be identical to order in the previous batchInsert()
        $this->batchInsert(Translation::tableName(), ['id', 'language', 'translation',], [
            [$id++, 'de', 'Benutzer-ID'],
            [$id++, 'de', 'Ursprünglicher Name'],
            [$id++, 'de', 'Erweiterung'],
            [$id++, 'de', 'Titel'],
            [$id++, 'de', 'Beschreibung'],
            [$id++, 'de', 'Bytes'],
            [$id++, 'de', 'Mime'],
            [$id++, 'de', 'Breite'],
            [$id++, 'de', 'Höhe'],
            [$id++, 'de', 'Datei auswählen'],
            [$id++, 'de', 'Galerie'],
            [$id++, 'de', 'Gruppentyp'],
            [$id++, 'de', 'Aktiv ab'],
            [$id++, 'de', 'Versteckt ab'],
            [$id++, 'de', 'Dokumentengruppen-ID'],
            [$id++, 'de', 'Dokumenten-ID'],
            [$id++, 'de', 'Dokumentengruppen-Element erstellen'],
            [$id++, 'de', 'Dokumentengruppen-Elemente'],
            [$id++, 'de', 'Dokumentengruppen-Element aktualisieren: {name}'],
            [$id++, 'de', 'Dokumentengruppe erstellen'],
            [$id++, 'de', 'Dokumentengruppen'],
            [$id++, 'de', 'Dokumentengruppe aktualisieren: {name}'],
            [$id++, 'de', 'Dokument erstellen'],
            [$id++, 'de', 'Dokumente'],
            [$id++, 'de', 'Dokument aktualisieren: {name}'],
            [$id++, 'de', 'Dateien hinzufügen'],
            [$id++, 'de', 'Upload starten'],
            [$id++, 'de', 'Upload abbrechen'],
            [$id++, 'de', 'Dateiname'],
            [$id++, 'de', 'Titel eingeben'],
            [$id++, 'de', 'Beschreibung eingeben'],
            [$id++, 'de', 'Verarbeitung'],
            [$id++, 'de', 'Dateien hier ablegen'],
            [$id++, 'de', 'Als Galerie anzeigen'],
            [$id++, 'de', 'Vorschau'],
            [$id++, 'de', 'Die Dokumentengruppe (Galerie) kann keine weiteren Dokumente aufnehmen. Das Limit wurde erreicht.'],
            [$id++, 'de', 'Tabelle'],
            [$id++, 'de', 'Layout'],
            [$id++, 'de', 'Mehr'],
            [$id++, 'de', 'Dateigröße'],
            [$id++, 'de', 'Als Titelbild festlegen'],
            [$id++, 'de', 'Öffentlich'],
            [$id++, 'de', 'Gesichert'],
            [$id++, 'de', 'Wählen Sie eine Datei aus'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";
        return false;
    }
}
