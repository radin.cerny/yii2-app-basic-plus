<?php


namespace app\modules\documents\widgets;

use app\modules\documents\widgets\assets\DropzoneAsset;
use app\modules\documents\widgets\assets\DropzoneWidgetAsset;
use yii\base\Widget;

// composer require enyo/dropzone
// https://www.dropzone.dev/
// Usage: echo \app\modules\documents\widgets\Dropzone::widget();

class Dropzone extends Widget
{
    public $url = [''];
    public $text = 'Drop a file to upload';
    public $id = 'myDropzone';
    public $inputName = 'Document[uploadedFile]';
    public $smallDropzone = false;
    public $formExtraClass = '';

    /**
     * Used only if $reloadContainerId is empty
     * @var string JS code to be executed. For example location.reload()
     */
    public $successJs = '';

    /**
     * If used, $successJs is not executed
     * @var string
     */
    public $reloadContainerId = '';

    public function init()
    {
        parent::init();
        DropzoneAsset::register($this->getView());
    }

    public function run()
    {
        return $this->render('dropzone/dropzone', [
            'url' => $this->url,
            'text' => $this->text,
            'id' => $this->id,
            'formExtraClass' => $this->formExtraClass,
            'inputName' => $this->inputName,
            'successJs' => $this->getSuccessJs(),
            'smallDropzone' => $this->smallDropzone,
        ]);
    }

    public function getSuccessJs()
    {
        if ($this->reloadContainerId) {
            return $this->getDefaultSuccessJs("$.pjax.reload({container:'#$this->reloadContainerId'});");
        }
        return $this->getDefaultSuccessJs($this->successJs);
    }

    public function getDefaultSuccessJs($jsCode)
    {
        $js = <<<JS
if (this.getUploadingFiles().length===0) {
  $jsCode
  this.removeAllFiles(true);
  this.element.classList.remove('dropzone-highlight')
}
JS;

        return $js;
    }
}
