<?php

namespace app\modules\documents\widgets\assets;

use yii\web\AssetBundle;

class DropzoneAsset extends AssetBundle
{
    public $sourcePath = '@vendor/enyo/dropzone/dist';

    public $css = [
        ['dropzone.css'],
    ];

    public $js = [
        ['dropzone-min.js'],
    ];

    public $depends = [
        'yii\web\YiiAsset',
    ];
}
