<?php
/**
 * @var $url string
 * @var $text string
 * @var $id string
 * @var $formExtraClass string
 * @var $inputName string
 * @var $successJs string
 * @var $smallDropzone bool
 */

use yii\helpers\Html;

if ($smallDropzone) {
    $formExtraClass .= ' smallDropzone';
}
echo Html::beginForm($url, 'post', ['class' => 'dropzone ' . $formExtraClass, 'id' => $id]);
echo Html::endForm();

// Many other options can be used. See documentation here:
// https://docs.dropzone.dev/configuration/basics/configuration-options
// or in file vendor/enyo/dropzone/dist/dropzone.js (search for $defaultOptions)
$this->registerJs(<<<JS
(new Dropzone("#$id", {
    dictDefaultMessage: "$text",
    acceptedFiles: "", // Example: 'image/*,application/pdf,.psd'
    dictRemoveFile: '<i class="bi bi-x-circle-fill" style="font-size: 2rem"></i>',
    dictCancelUpload: '<i class="bi bi-stop-circle-fill" style="font-size: 2rem"></i>',
    dictCancelUploadConfirmation: 'Really?',
    paramName: 'Document[uploadedFile]',
}))

// Arrow-functions cannot work with "this". Standard methods can. 
.on("success", function (file) { $successJs })

// Highlight drop area when file is dragged over
.on("dragenter", function() {
  document.getElementById("$id").classList.add("dropzone-highlight");
})

// Remove highlight when file is dragged out
.on("dragleave", function() {
  document.getElementById("$id").classList.remove("dropzone-highlight");
});
    
JS
);

$this->registerCss(<<<CSS
  .dropzone-highlight {
    border: 3px solid red;
  }
  form.dropzone.smallDropzone {
    max-height: 2rem !important;
    height: 2rem !important;
    min-height: 2rem !important;
    padding: 0.1rem 0px 0px 0px !important;
  }
  form.dropzone.smallDropzone div.dz-default.dz-message {
      margin: 0px !important;
  }
CSS
);
