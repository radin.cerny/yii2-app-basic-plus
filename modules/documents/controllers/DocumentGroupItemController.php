<?php

namespace app\modules\documents\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\documents\models\DocumentGroupItem;
use app\modules\documents\models\search\DocumentGroupItemSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * DocumentGroupItemController implements the CRUD actions for DocumentGroupItem model.
 */
class DocumentGroupItemController extends BaseController
{
    public $moreCoverDocumentsAllowed = false;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all DocumentGroupItem models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentGroupItemSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DocumentGroupItem model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DocumentGroupItem model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($groupId = null, $pjaxId = null)
    {
        $model = new DocumentGroupItem();

        if ($groupId) {
            $model->document_group_id = $groupId;
        }

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                if (Yii::$app->request->isAjax) {
                    return '<script>'
                        . "$.pjax.reload({container:'#$pjaxId'});"
                        . "$('#myModalDialog').modal('hide');"
                        . '</script>';
                }

                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DocumentGroupItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id, $pjaxId = null)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            if (Yii::$app->request->isAjax && $pjaxId) {
                return '<script>'
                    . "$.pjax.reload({container:'#$pjaxId'});"
                    . "$('#myModalDialog').modal('hide');"
                    . '</script>';
            }

            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing DocumentGroupItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $pjaxId = null)
    {
        $this->findModel($id)->delete();

        if ($pjaxId) {
            return '<script>'
                . "$.pjax.reload({container:'#$pjaxId'});"
                . "$('#myModalDialog').modal('hide');"
                . '</script>';
        }

        return $this->redirect(['index']);
    }

    public function actionSetAsGroupCover($id, $pjaxId)
    {
        $item = $this->findModel($id);

        if (!$this->moreCoverDocumentsAllowed) {
            DocumentGroupItem::updateAll(['is_cover' => 0], [
                'document_group_id' => $item->document_group_id,
            ]);
        }

        $item->is_cover = !$item->is_cover;
        $item->save(false, ['is_cover']);

        return '<script>'
            . "$.pjax.reload({container:'#$pjaxId'});"
            . "$('#myModalDialog').modal('hide');"
            . '</script>';
    }

    public function actionToggle($id, $pjaxId)
    {
        $item = $this->findModel($id);
        $item->status = !$item->status;
        $item->save(false, ['status']);

        return '<script>'
            . "$.pjax.reload({container:'#$pjaxId'});"
            . "$('#myModalDialog').modal('hide');"
            . '</script>';
    }

    /**
     * Finds the DocumentGroupItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return DocumentGroupItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DocumentGroupItem::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
