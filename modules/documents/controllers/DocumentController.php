<?php

namespace app\modules\documents\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\documents\models\Document;
use app\modules\documents\models\search\DocumentSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * DocumentController implements the CRUD actions for Document model.
 */
class DocumentController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Document models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Document model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Document model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate($groupId = null)
    {
        $model = Yii::createObject(Document::class);
        if ($this->request->isPost) {
            if ($model->uploadAndSave(Yii::$app->user->identity->id)) {
                return $this->redirect(['index']);
            }

            if ($model->hasErrors()) {
                // Only when uploaded via Drag&drop?
                return $this->asJson([
                        'files' => [
                            [
                                'filename' => $model->getOrigFilename(),
                                'errors' => $model->getErrors('uploadedFile'),
                            ]
                        ]
                    ]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Document model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            //return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Document model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionDropzoneUpload()
    {
        $document = new Document();
        if ($document->uploadAndSave(Yii::$app->user->identity->id)) {
            // here we can modify some related object
        } else {
            $error = $document->getErrorSummary(false);
            Yii::$app->session->setFlash('error', $error);
        }
    }

    public function actionGetUrl($id, $thumb = 0)
    {
        $model = $this->findModel($id);

        $path = $model->getPath();
        if ($thumb) {
            $path = $model->getThumbnailPath();
        }

        header('Content-Type: ' . $model->mime);

        if (trim($model->mime) == '') {
            header('Content-Type: application/octet-stream');
        }

        if (!file_exists($path)) {
            $path = realpath(__DIR__ . '/../unknown_file.svg');
            header('Content-Type: image/svg+xml');
        }

        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=' . $model->getOrigFilename()); // basename($path)
        header('Content-Transfer-Encoding: binary');
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($path));
        if (ob_get_contents()) {
            ob_clean();
        }
        flush();
        readfile($path);

        // Or do we want to use fpassthru() instead of readfile().
        // @link https://www.php.net/manual/en/function.fpassthru.php
        // ...  But:
        // If you just want to dump the contents of a file to the output buffer,
        // without first modifying it or seeking to a particular offset,
        // you may want to use the readfile(), which saves you the fopen() call.

        exit;
    }

    /**
     * Finds the Document model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Document the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Document::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
