<?php

namespace app\modules\documents\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\documents\models\DocumentGroup;
use app\modules\documents\models\search\DocumentGroupItemSearch;
use app\modules\documents\models\search\DocumentGroupSearch;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * DocumentGroupController implements the CRUD actions for DocumentGroup model.
 */
class DocumentGroupController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all DocumentGroup models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new DocumentGroupSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single DocumentGroup model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new DocumentGroup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new DocumentGroup();

        if ($this->request->isPost) {
            $model->id_user = Yii::$app->user->identity->id;
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['update', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing DocumentGroup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);


        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        $searchModel_items = new DocumentGroupItemSearch();
        $dataProvider_items = $searchModel_items->search(['id_group' => $this->id]);
        $dataProvider_items->sort = [
            'defaultOrder' => [
                'position' => SORT_ASC,
            ]
        ];
        return $this->render('update', [
            'model' => $model,
            'searchModel_items' => $searchModel_items,
            'dataProvider_items' => $dataProvider_items,
        ]);
    }

    /**
     * Deletes an existing DocumentGroup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGallery($id)
    {
        $model = $this->findModel($id);

        $galleryDataProvider = new ArrayDataProvider([
            'allModels' => $model->items,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('gallery', [
            'galleryDataProvider' => $galleryDataProvider,
            'model' => $model,
        ]);
    }

    public function actionSort($id, $pjaxId = null)
    {
        $group = $this->findModel($id);
        $items = ArrayHelper::index($group->items, 'id');
        foreach (Yii::$app->request->post('position') as $i => $docGroupItemId) {
            if (isset($items[$docGroupItemId])) {
                $items[$docGroupItemId]->position = ($i + 1) * 1000;
                $items[$docGroupItemId]->save(false, ['position']);
            }
        }

        if ($pjaxId) {
            return '<script>'
                . "$.pjax.reload({container:'#$pjaxId'});"
                . "$('#myModalDialog').modal('hide');"
                . '</script>';
        }
    }

    /**
     * Finds the DocumentGroup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return DocumentGroup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = DocumentGroup::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
