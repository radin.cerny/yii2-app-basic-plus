<?php

namespace app\modules\documents;

use app\modules\BaseModule;

class Module extends BaseModule
{
    public $defaultRoute = 'document/index';
    public string $docUploadFolder = '@app/uploads';
    public string $docUploadFolderPublic = '@app/web/uploads';
}
