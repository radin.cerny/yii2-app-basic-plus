<?php

use app\modules\documents\widgets\FileUpload;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroup $model */
/** @var app\modules\documents\models\DocumentGroup $model */

$this->title = Yii::t('app.documents', 'Update Document Group: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Document Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

//echo FileUpload::widget([
//    'uploadUrl' => Url::to(['document/create', 'groupId' => $model->id]),
//]);
?>
<br>
<div class="card">

    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>

<div class="card">

    <div class="card-body">

        <?= $this->render('/document-group-item/index', [
            'searchModel' => $searchModel_items,
            'dataProvider' => $dataProvider_items,
            'showBreadcrumbs' => false,
            'groupId' => $model->id,
            'title' => $this->title,
        ]); ?>

    </div>

</div>
