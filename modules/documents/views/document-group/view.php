<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroup $model */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Document Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="card">

    <div class="card-body">

        <p>
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ]) ?>
        </p>

        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'group_type',
                'id_user',
                'position',
                'status',
                'active_from',
                'hidden_from',
                'title',
                'description:ntext',
                'created_at',
                'updated_at',
                'deleted_at',
            ],
        ]) ?>

    </div>
</div>
