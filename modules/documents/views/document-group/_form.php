<?php

use app\modules\documents\models\DocumentGroup;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroup $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="document-group-form">

    <?php $form = ActiveForm::begin([
    ]); ?>

    <?= $form->field($model, 'group_type')->dropDownList(DocumentGroup::getType_listData()) ?>

    <?php // $form->field($model, 'id_user')->textInput() ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(DocumentGroup::getStatus_listData()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active_from')->widget(app\widgets\DatePicker::class); ?>

    <?= $form->field($model, 'hidden_from')->widget(app\widgets\DatePicker::class); ?>

    <?php // $form->field($model, 'deleted_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
