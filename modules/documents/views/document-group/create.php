<?php

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroup $model */

$this->title = Yii::t('app.documents', 'Create Document Group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Document Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">

    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
