<?php

use app\modules\documents\widgets\FileUpload;
use app\widgets\GalleryListView;
use yii\widgets\Pjax;

/** @var \yii\data\ArrayDataProvider $galleryDataProvider */
/** @var \app\modules\documents\models\DocumentGroup $model */

$this->title = Yii::t('app.documents', 'Gallery') . ' : ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Document Groups'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

//echo FileUpload::widget([
//    'uploadUrl' => Url::to(['document/create', 'groupId' => $model->id]),
//]);

echo '<br>';

Pjax::begin([
    'id' => 'gallery-pjax2',
    'enablePushState' => false,
    'enableReplaceState' => false
]);

echo GalleryListView::widget([
    'dataProvider' => $galleryDataProvider,
    'pjaxId' => 'gallery-pjax2',
    'galleryId' => $model->id,
]);

Pjax::end();
