<?php

use app\modules\documents\models\DocumentGroup;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\search\DocumentGroupSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app.documents', 'Document Groups');
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="document-group-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app.documents', 'Create Document Group'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'format' => 'html',
                'label' => Yii::t('app', 'Thumbnail'),
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function ($model, $key, $index, $column) {
                    $cover = $model->getItems()->isCover()->with('document')->one();
                    if ($cover) {
                        return Html::img($cover->document->getUrl(true), ['style' => 'width: 100px']);
                    }
                    return 'N/A';
                },
            ],
            [
                'attribute' => 'group_type',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'id_user',
                'format' => 'text',
                'label' => Yii::t('app', 'User ID'),
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'position',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'status',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
//                [
//                    'attribute' => 'active_from',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'hidden_from',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
            [
                'attribute' => 'title',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'description',
                'format' => 'ntext',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                //'attribute' => 'description',
                'format' => 'ntext',
                'label' => '#',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function ($model, $key, $index, $column) {
                    return count($model->items);
                },
            ],
//                [
//                    'attribute' => 'created_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'updated_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'deleted_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, DocumentGroup $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 9rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'template' => '{view} {update} {delete} {gallery}',
                'buttons' => [
                    'gallery' => function ($url, $model, $key) {
                        return Html::a(
                            '<i class="icon icon-gallery"></i>',
                            ['document-group/gallery', 'id' => $model->id],
                            [
                                'title' => Yii::t('app.documents', 'Show as gallery'),
                                //'data-pjax' => '0',
                            ]
                        );
                    },
                ],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroup:view');
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroup:update');
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroup:delete');
                    },
                    'gallery' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroup:gallery');
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
