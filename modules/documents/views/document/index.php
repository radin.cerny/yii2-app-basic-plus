<?php

use app\modules\documents\models\Document;
use app\modules\documents\widgets\Dropzone;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\search\DocumentSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app.documents', 'Documents');
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
//echo \app\modules\documents\widgets\FileUpload::widget([
//    'uploadUrl' => Url::to(['document/create']),
//]);
?>
<br>

<div class="document-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= Dropzone::widget([
        'url' => ['dropzone-upload'],
        'successJs' => 'location.reload()',
        'formExtraClass' => 'mb-3',
    ]); ?>

    <p>
        <?= Html::a(Yii::t('app.documents', 'Create Document'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin([
        'id' => 'uploadPjax',
//            'enablePushState' => false,
//            'enableReplaceState' => false,
    ]); ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        //'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'id_user',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'format' => 'html',
                'label' => Yii::t('app.documents', 'Preview'),
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (Document $model, $key, $index, $column) {
                    // https://stackoverflow.com/questions/20569658/html5-video-tag-not-working-on-firefox
                    return Html::img($model->getUrl(true), ['width' => '100px']);
                },
            ],
            [
                'attribute' => 'name',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'extension',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'title',
                'format' => 'text',
                //'visible' => true,
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'description',
                'format' => 'ntext',
//                'visible' => true,
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'bytes',
                'format' => 'raw',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function ($model, $key, $index, $column) {
                    $title = Yii::$app->formatter->asInteger($model->bytes) . ' B';
                    $value = Yii::$app->formatter->asShortSize($model->bytes, 0);
                    return '<span title="' . $title . '" style="cursor:pointer">' . $value . '</span>';
                },
            ],
            [
                'attribute' => 'mime',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'width',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'height',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, Document $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 8rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                //'template' => '{view} {update} {delete} {newBtn}',
                //'buttons' => [
                //    'newBtn' => function ($url, $model, $key) {
                //        return Html::a(
                //            '<i class="fa-solid fa-key"></i>',
                //            $url,
                //            [
                //                'title' => Yii::t('app', 'Text'),
                //                //'data-pjax' => '0',
                //            ]
                //       );
                //    },
                //],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/Document:view');
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/Document:update');
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/Document:delete');
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>

