<?php

/** @var yii\web\View $this */
/** @var app\modules\documents\models\Document $model */

$this->title = Yii::t('app.documents', 'Create Document');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Documents'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">

    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
