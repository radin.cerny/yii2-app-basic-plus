<?php

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroupItem $model */

$this->title = Yii::t('app.documents', 'Update Document Group Item: {name}', [
    'name' => $model->title,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Document Group Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="card">

    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
