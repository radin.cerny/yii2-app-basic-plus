<?php

use app\modules\documents\models\Document;
use app\modules\documents\models\DocumentGroup;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroupItem $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="document-group-item-form">

    <?php \yii\widgets\Pjax::begin([
        'id' => 'document-group-item-Pjax',
        'enablePushState' => false,
        'enableReplaceState' => false,
    ]) ?>

    <?php $form = ActiveForm::begin([
        'id' => 'document-group-item-ActiveForm',
        'options' => ['data-pjax' => 1]
    ]); ?>

    <?php echo $form->field($model, 'document_group_id')->hiddenInput()->label(false) ?>

    <?php echo $form->field($model, 'document_id')->dropDownList(Document::getFilterData('id', function ($model, $defaultValue) {
        return $model->getOrigFilename();
    })) ?>

    <?= $form->field($model, 'position')->textInput() ?>

    <?= $form->field($model, 'status')->dropDownList(DocumentGroup::getStatus_listData()) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'active_from')->widget(app\widgets\DatePicker::class); ?>

    <?php //= $form->field($model, 'hidden_from')->widget(app\widgets\DatePicker::class); ?>

    <?php // echo $form->field($model, 'created_at')->textInput() ?>

    <?php // echo $form->field($model, 'updated_at')->textInput() ?>

    <?php // echo $form->field($model, 'deleted_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <?php \yii\widgets\Pjax::end() ?>

</div>
