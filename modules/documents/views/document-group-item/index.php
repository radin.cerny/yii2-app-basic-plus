<?php

use app\modules\documents\models\DocumentGroup;
use app\modules\documents\models\DocumentGroupItem;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

;

/** @var yii\web\View $this */
/** @var app\modules\documents\models\search\DocumentGroupItemSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = $title ?? Yii::t('app.documents', 'Document Group Items');
if ($showBreadcrumbs ?? true) {
    $this->params['breadcrumbs'][] = $this->title;
}
$pjaxId = 'document-group-item-Pjax';
?>


<div class="document-group-item-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app.documents', 'Create Document Group Item'), ['document-group-item/create', 'groupId' => $groupId ?? null, 'pjaxId' => $pjaxId], ['class' => 'btn btn-success openInMyModal']) ?>
    </p>

    <?php \yii\widgets\Pjax::begin([
        'id' => $pjaxId,
//            'enablePushState' => false,
//            'enableReplaceState' => false,
    ]) ?>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'id' => 'document-group-item-GridView',
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'pager' => [
            'class' => 'yii\bootstrap5\LinkPager' // Otherwise pager has no style. Why?
        ],
        //'filterModel' => $searchModel,
        'columns' => [
////                [
//                    'attribute' => 'document_group_id',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
            [
                'attribute' => 'document_id',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'format' => 'html',
                'label' => 'Preview',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                'headerOptions' => ['style' => 'width: 10rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function ($model, $key, $index, $column) {
                    return Html::img($model->document->getUrl(true), ['width' => '100px']);
                },
            ],
            [
                'attribute' => 'position',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'status',
                'format' => 'text',
                //'label' => '',
                'filter' => DocumentGroup::getStatus_listData(),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],

            [
                'attribute' => 'title',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 15rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
//                [
//                    'attribute' => 'description',
//                    'format' => 'ntext',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'created_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'updated_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],
//                [
//                    'attribute' => 'deleted_at',
//                    'format' => 'text',
//                    //'label' => '',
//                    //'filter' => ['id' => 'Some text'],
//                    //'filterInputOptions' => [
//                    //    'class' => 'form-control',
//                    //    'prompt' => '=== ALL ===',
//                    //],
//                    //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
//                    //'contentOptions' => ['style' => 'text-align:center;'],
//                    //'value' => function ($model, $key, $index, $column) {
//                    //},
//                ],

            [
                'attribute' => 'active_from',
                'format' => 'text',
                //'label' => '',
                'filter' => DocumentGroup::getStatus_listData(),
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'hidden_from',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                'headerOptions' => ['style' => 'width: 10rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],

            [
                'class' => ActionColumn::class,
                'buttonOptions' => [
                    'class' => 'openInMyModal',
                ],
                'urlCreator' => function ($action, DocumentGroupItem $model, $key, $index, $column) use ($pjaxId) {
                    return Url::toRoute(['document-group-item/' . $action, 'id' => $model->id, 'pjaxId' => $pjaxId]);
                },
                'headerOptions' => ['style' => 'width: 8rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                //'template' => '{view} {update} {delete} {newBtn}',
                //'buttons' => [
                //    'newBtn' => function ($url, $model, $key) {
                //        return Html::a(
                //            '<i class="fa-solid fa-key"></i>',
                //            $url,
                //            [
                //                'title' => Yii::t('app', 'Text'),
                //                //'data-pjax' => '0',
                //            ]
                //       );
                //    },
                //],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroupItem:view');
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroupItem:update');
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('documents/DocumentGroupItem:delete');
                    },
                ],
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>