<?php

/** @var yii\web\View $this */
/** @var app\modules\documents\models\DocumentGroupItem $model */

$this->title = Yii::t('app.documents', 'Create Document Group Item');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.documents', 'Document Group Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">

    <div class="card-body">

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>

</div>
