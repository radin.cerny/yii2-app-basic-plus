<?php

namespace app\modules\intl\components;

use app\modules\intl\models\Currency;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\UrlRule;

class UrlManager extends \yii\web\UrlManager
{
    /**
     * For conversion in url rules:
     * myweb.com/en/user/list => myweb.com/user/list?{langUrlParamName}=en
     * @var string
     */
    public string $langUrlParamName = 'urlLang';

    public string $langCodeRegex = '[a-z]{2}';
    /**
     * Customer might want to use unusual language code in the URL instead of the official code.
     * For example value ['cz' => 'cs']
     * ... if CZ is found in the URL it is interpreted as CS.
     * ... if URL is built then CS is replaced with CZ
     * (CS is the official code for the Czech language, but it is outdated, as our country has code CZ since 1993. Therefore both codes must be supported not to confuse users.)
     * @var array|string[]
     */
    public array $langConversion = [];
    /**
     * Which languages do we really wanna support
     * @var array|string[]
     */
    public array $supportedLanguages = ['en'];

    public function __construct($config = [])
    {
        parent::__construct($config);

        $lang = $this->langUrlParamName;
        $regex = $this->langCodeRegex;
        $langCode = "<$lang:$regex>/";

        /** @var UrlRule $rule */
        foreach ($this->rules as $rule) {
            $this->addRules([$langCode . $rule->name => $rule->route]);
        }

        $this->addRules([
            // Standard URLs with the language code
            $langCode . '' => '',
            $langCode . '<a>' => '<a>',
            $langCode . '<a>/<b>' => '<a>/<b>',
            $langCode . '<a>/<b>/<c>' => '<a>/<b>/<c>',
            $langCode . '<a>/<b>/<c>/<d>' => '<a>/<b>/<c>/<d>',
        ]);
    }

    public function createUrl($params)
    {
        // All Links will contain current language. See "urlManager" in web.php for rules.
        if (!isset($params[$this->langUrlParamName])) {
            $params[$this->langUrlParamName] = strtolower($this->getCurrentLanguage());
        }

        $params[$this->langUrlParamName] = $this->convertLanguageCodeForUi($params[$this->langUrlParamName]);

        return parent::createUrl($params);
    }

    public function getCurrentLanguage($default = 'en')
    {
        return explode('-', Yii::$app->language)[0] ?? $default;
    }

    public function convertLanguageCodeForUi($internalLanguageCode)
    {
        $result = array_search($internalLanguageCode, $this->langConversion);
        if ($result) {
            return $result;
        }
        return $internalLanguageCode;
    }

    public function parseRequest($request)
    {
        $defaultLanguage = Yii::$app->sourceLanguage;
        [$route, $params] = parent::parseRequest($request);

        // @link https://www.geeksforgeeks.org/how-to-detect-browser-language-in-php/
        $browserLang = Yii::$app->request->headers->get('accept-language', $defaultLanguage);
        $browserLang = substr($browserLang, 0, 2);

        $newLang = ArrayHelper::getValue($params, $this->langUrlParamName, $browserLang);
        $newLang = strtolower($newLang);
        $newLang = $this->langConversion[$newLang] ?? $newLang;

        if (!in_array($newLang, $this->supportedLanguages)) {
            $newLang = $defaultLanguage;
        }

        Yii::$app->language = $newLang;

        return [$route, $params];
    }

    public function getCurrentUrlInLanguage($lang)
    {
        $actionId = Yii::$app->controller->action->getUniqueId();
        $getParams = Yii::$app->request->get();
        $getParams[0] = '/' . $actionId;
        $getParams[$this->langUrlParamName] = $lang;
        return Url::to($getParams);
    }
}
