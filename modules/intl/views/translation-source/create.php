<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\intl\models\TranslationSource $model */

$this->title = Yii::t('app', 'Create Translation Source');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Translation Sources'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-source-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
