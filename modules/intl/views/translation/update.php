<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\intl\models\Translation $model */

$this->title = Yii::t('app', 'Update Translation: {name}', [
    'name' => $model->pk,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Translations'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pk, 'url' => ['view', 'pk' => $model->pk]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="translation-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
