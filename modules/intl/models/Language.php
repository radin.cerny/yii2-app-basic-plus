<?php

namespace app\modules\intl\models;

use app\helpers\Html;
use app\models\BaseModel;
use app\modules\intl\models\query\LanguageQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%language}}".
 *
 * @property int $id
 * @property string $code 2 or 3 letter lowercase code according to ISO-639
 * @property string $name_native Native name of the language
 * @property string $name_english English name of the language
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Translation[] $translations
 */
class Language extends BaseModel
{
    public const FLAG_PATH = '/img/flags/';

    public static function getFlagPathByLangCode($langCode)
    {
        return Language::FLAG_PATH . $langCode . '.svg';
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%intl_language}}';
    }

    public static function getEnabledLanguages_listData()
    {
        return ArrayHelper::map(Language::find()->asArray()->all(), 'code', 'name_native');
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\query\LanguageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new LanguageQuery(get_called_class());
    }

    public static function getLanguageSwitchData()
    {
        $result = [];
        foreach (Language::find()->active()->all() as $language) {
            $flagPath = Language::getFlagPathByLangCode($language->code);
            $text = strtoupper(Yii::$app->urlManager->convertLanguageCodeForUi($language->code));
            $url = Yii::$app->urlManager->getCurrentUrlInLanguage($language->code);
            $result[] = [
                'label' => Html::getMenuLangLink($text, $flagPath),
                'url' => $url,
                'encode' => false,
            ];
        }
        return $result;
    }

    public function getFlagPath()
    {
        return Language::FLAG_PATH . $this->code . '.svg';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'name_native', 'name_english'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['code', 'name_native', 'name_english'], 'string', 'max' => 255],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app.intl', 'Code'),
            'name_native' => Yii::t('app.intl', 'Native name'),
            'name_english' => Yii::t('app.intl', 'English name'),
            // Default columns added by BaseMigration:
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'lang' => Yii::t('app', 'Language'),
            'position' => Yii::t('app', 'Position'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
        ];
    }

    /**
     * Gets query for [[Translations]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\intl\models\query\TranslationQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['language' => 'code']);
    }
}
