<?php

namespace app\modules\intl\models;

use app\models\BaseModel;
use app\modules\intl\models\query\TranslationSourceQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%intl_translation_source}}".
 *
 * @property int $id
 * @property string|null $category
 * @property string|null $message
 *
 * @property Translation[] $translations
 */
class TranslationSource extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%intl_translation_source}}';
    }

    public static function getCategoryListData()
    {
        return ArrayHelper::map(self::find()->select('category')->distinct()->asArray()->all(), 'category', 'category');
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\query\TranslationSourceQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TranslationSourceQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['category'], 'string', 'max' => 255],
            [['message', 'category'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'category' => Yii::t('app', 'Category'),
            'message' => Yii::t('app.intl', 'Original text'),
        ];
    }

    /**
     * Gets query for [[Translations]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\intl\models\query\TranslationQuery
     */
    public function getTranslations()
    {
        return $this->hasMany(Translation::class, ['id' => 'id']);
    }
}
