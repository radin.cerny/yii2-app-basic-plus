<?php

namespace app\modules\intl\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\intl\models\Translation]].
 *
 * @see \app\modules\intl\models\Translation
 */
class TranslationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\Translation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\Translation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
