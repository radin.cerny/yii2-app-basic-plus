<?php

namespace app\modules\intl\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\intl\models\Language]].
 *
 * @see \app\modules\intl\models\Language
 */
class LanguageQuery extends \yii\db\ActiveQuery
{
    public function active()
    {
        return $this->andWhere('[[active]]=1');
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\Language[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\Language|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
