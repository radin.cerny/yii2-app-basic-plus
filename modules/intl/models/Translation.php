<?php

namespace app\modules\intl\models;

use app\models\BaseModel;
use app\modules\intl\models\query\TranslationQuery;
use Yii;

/**
 * This is the model class for table "{{%intl_translation}}".
 *
 * @property int $id
 * @property string $language
 * @property string|null $translation
 *
 * @property TranslationSource $translationSource
 */
class Translation extends BaseModel
{
    /**
     * Should return the same list that is in file config/message-extract.php
     * It is a list of "secondary" languages that this web is translated to. Primary language is English.
     * @return string[]
     */
    public static function getAllTargetLangCodes()
    {
        $messageConfig = require Yii::getAlias('@app/config/message-extract.php');
        return $messageConfig['languages'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%intl_translation}}';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\intl\models\query\TranslationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new TranslationQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['language', 'translation'], 'required'],
            [['translation'], 'string'],
            [['language'], 'string', 'max' => 16],
            [['id', 'language'], 'unique', 'targetAttribute' => ['id', 'language']],
            [['id'], 'exist', 'skipOnError' => true, 'targetClass' => TranslationSource::class, 'targetAttribute' => ['id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pk' => Yii::t('app', 'ID'),
            'id' => Yii::t('app.intl', 'Original text ID'),
            'language' => Yii::t('app', 'Language'),
            'translation' => Yii::t('app.intl', 'Translation'),
        ];
    }

    /**
     * Gets query for [[Id0]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\intl\models\query\TranslationSourceQuery
     */
    public function getTranslationSource()
    {
        return $this->hasOne(TranslationSource::class, ['id' => 'id']);
    }
}
