<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

use app\migrations\BaseMigration;
use app\modules\intl\models\Language;
use app\modules\intl\models\Translation;
use app\modules\intl\models\TranslationSource;

/**
 * Class m240319_120002_translation
 */
class m240319_120002_translation extends BaseMigration
{
    public $idColName = 'pk';

    public function up()
    {
        $this->createTable(Translation::tableName(), [
            'id' => $this->integer()->notNull()->comment('Points to translation_source.id as it is hardcoded in Yii. Column ' . $this->idColName . ' is the real local ID.'),
            'language' => $this->string()->notNull(),
            'translation' => $this->string(1000),
        ]);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            // 'NO ACTION' is equivalent to 'RESTRICT' in MSSQL
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey('fk_translation_to_source', Translation::tableName(), 'id', TranslationSource::tableName(), 'id', 'NO ACTION', $onUpdateConstraint);
        $this->createIndex('idx_translation_language', Translation::tableName(), 'language');
        $this->createIndex('unique_id_lang', Translation::tableName(), ['id', 'language'], true);

        $this->addForeignKey('fk_translation_language', Translation::tableName(), 'language', Language::tableName(), 'code');
    }

    public function down()
    {
        $this->dropForeignKey('fk_translation_language', Translation::tableName());
        $this->dropTable(Translation::tableName());
    }
}
