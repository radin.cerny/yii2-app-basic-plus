<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

use app\migrations\BaseMigration;
use app\modules\intl\models\TranslationSource;

/**
 * Class m240319_120001_translation_source
 */
class m240319_120001_translation_source extends BaseMigration
{
    public function up()
    {
        $this->createTable(TranslationSource::tableName(), [
            'category' => $this->string(),
            'message' => $this->string(1000),

        ]);

        $this->createIndex('idx_translation_category', TranslationSource::tableName(), 'category');
        $this->createIndex('unique_category_message', TranslationSource::tableName(), ['category', 'message'], true);
    }

    public function down()
    {
        $this->dropForeignKey('fk_message_source_message', TranslationSource::tableName());
        $this->dropTable(TranslationSource::tableName());
    }
}
