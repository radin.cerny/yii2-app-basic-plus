<?php

use app\modules\intl\controllers\LanguageController;
use app\modules\intl\controllers\TranslationController;
use app\modules\intl\controllers\TranslationSourceController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m240319_130000_rbac
 */
class m240319_130000_intl_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            LanguageController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
            TranslationController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
            TranslationSourceController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate',],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";
        return false;
    }
}
