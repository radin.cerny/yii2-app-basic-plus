<?php

use app\migrations\BaseMigration;
use app\modules\intl\models\Language;

/**
 * Class m240319_120000_language
 */
class m240319_120000_language extends BaseMigration
{
    public $createColumnActive = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable(Language::tableName(), [
            'code' => $this->string()->notNull()->unique()->comment('2 or 3 letter lowercase code according to ISO-639'), // @link https://www.loc.gov/standards/iso639-2/php/code_list.php
            'name_native' => $this->string()->notNull()->comment('Native name of the language'),
            'name_english' => $this->string()->notNull()->comment('English name of the language'),
            'is_source_language' => $this->boolean()->comment('Do not change this value. See topic Yii2 Internationalization for sourceLanguage details.'),
        ]);

        $this->createIndex('unique-language', Language::tableName(), ['code'], true);

        $this->batchInsert(Language::tableName(), ['code', 'name_native', 'name_english', 'active', 'is_source_language'], [
            ['en', 'English', 'English', 1, 1],
            ['cs', 'Čeština', 'Czech', 1, 0],
            ['de', 'Deutsch', 'German', 0, 0],
            ['fr', 'Français', 'French', 0, 0],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Language::tableName());
    }
}
