<?php

use app\modules\settings\models\Settings;
use yii\db\Migration;

/**
 * Class m241022_090010_settings_data
 */
class m241022_090010_settings_data extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_STRING,
            'section' => 'demo',
            'key' => 'string',
            'value' => 'some text',
            'status' => 1,
            'user_edit' => 1,
            'description' => 'Demo value',
            'delimiter' => null,
            'lang' => null,
        ]);

        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_BOOL,
            'section' => 'demo',
            'key' => 'bool',
            'value' => 1,
            'status' => 1,
            'user_edit' => 1,
            'description' => 'Demo value',
            'delimiter' => null,
            'lang' => null,
        ]);

        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_INT,
            'section' => 'demo',
            'key' => 'int',
            'value' => 123,
            'status' => 1,
            'user_edit' => 1,
            'description' => 'Demo value',
            'delimiter' => null,
            'lang' => null,
        ]);

        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_FLOAT,
            'section' => 'demo',
            'key' => 'float',
            'value' => 123.45,
            'status' => 1,
            'user_edit' => 1,
            'description' => 'Demo value',
            'delimiter' => null,
            'lang' => null,
        ]);

        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_STRING,
            'section' => 'demo',
            'key' => 'translated',
            'value' => 'Hello',
            'status' => 1,
            'user_edit' => 1,
            'description' => 'Demo value',
            'delimiter' => null,
            'lang' => 'en',
        ]);

        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_STRING,
            'section' => 'demo',
            'key' => 'translated',
            'value' => 'Ahoj',
            'status' => 1,
            'user_edit' => 1,
            'description' => 'Demo value',
            'delimiter' => null,
            'lang' => 'cs',
        ]);

        $this->insert(Settings::tableName(), [
            'type' => Settings::TYPE_BOOL,
            'section' => 'app',
            'key' => 'mainMenuVertical',
            'value' => true,
            'status' => 1,
            'user_edit' => 1,
            'description' => 'If TRUE, vertical main menu bar is shown',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->truncateTable(Settings::tableName());
    }
}
