<?php

use app\modules\settings\controllers\SettingsController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m241022_090030_settings_rbac
 */
class m241022_090030_settings_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            SettingsController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate', 'actionCopy'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter', 'canAccessAll'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        RbacHelper::deleteAuthItemsForController(SettingsController::class);
    }
}
