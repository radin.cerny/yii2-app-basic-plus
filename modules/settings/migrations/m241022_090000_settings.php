<?php

use app\migrations\BaseMigration;
use app\modules\settings\models\Settings;

/**
 * Class m241022_090000_settings
 */
class m241022_090000_settings extends BaseMigration
{
    public $createColumnLang = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable(Settings::tableName(), [
            'section' => $this->string()->notNull(),
            'key' => $this->string()->notNull(),
            'user_edit' => $this->boolean()->notNull()->defaultValue(0)->comment('Some users can access the list of settings and can edit records where user_edit=1'),
            'type' => $this->string(10)->notNull()->defaultValue(Settings::TYPE_STRING),
            'delimiter' => $this->string()->comment('The delimiter if the value should returned as an array'),
            'value' => $this->text()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'description' => $this->string(),
        ]);

        $this->createIndex('settings_section_key_unique', Settings::tableName(), ['section', 'key', 'lang'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable(Settings::tableName());
    }
}
