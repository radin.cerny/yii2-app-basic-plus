<?php

use app\modules\intl\models\Translation;
use app\modules\intl\models\TranslationSource;
use yii\db\Migration;

/**
 * Class m241022_090020_settings_translations
 */
class m241022_090020_settings_translations extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert(TranslationSource::tableName(), ['category', 'message'], [
            ['app.settings', 'Create Settings'],
            ['app.settings', 'Settings'],
            ['app.settings', 'Update Settings: {name}'],
            ['app.settings', 'Can be edited by users'],
            ['app.settings', 'Decimal number (1,1 or 1.2)'],
            ['app.settings', 'Html'],
            ['app.settings', 'Integer'],
            ['app.settings', 'Text - multi line'],
            ['app.settings', 'Text - one line'],
            ['app.settings', 'Yes (1) / No (0)'],
        ]);

        // getLastInsertID() returns ID of the first batch-inserted row!
        // INFO: If getLastInsertID() returns incorrect number after batchInsert() to the 1st table,
        // ... insert only one row in batchInsert(), then call getLastInsertID() and then insert the rest using batchInsert()
        // 2nd table should be always OK
        $firstId = $this->db->getLastInsertID();

        $id = $firstId;

        // Order must be identical to order in the previous batchInsert()
        $this->batchInsert(Translation::tableName(), ['id', 'language', 'translation',], [
            [$id++, 'cs', 'Vytvořit nastavení'],
            [$id++, 'cs', 'Nastavení'],
            [$id++, 'cs', 'Upravit nastavení: {name}'],
            [$id++, 'cs', 'Může být upraveno uživateli'],
            [$id++, 'cs', 'Desetinné číslo (1,1 nebo 1.2)'],
            [$id++, 'cs', 'Html'],
            [$id++, 'cs', 'Celé číslo'],
            [$id++, 'cs', 'Text - více řádků'],
            [$id++, 'cs', 'Text - jeden řádek'],
            [$id++, 'cs', 'Ano (1) / Ne (0)'],
        ]);

        $id = $firstId;

        // Order must be identical to order in the previous batchInsert()
        $this->batchInsert(Translation::tableName(), ['id', 'language', 'translation',], [
            [$id++, 'de', 'Einstellungen erstellen'],
            [$id++, 'de', 'Einstellungen'],
            [$id++, 'de', 'Einstellungen aktualisieren: {name}'],
            [$id++, 'de', 'Kann von Benutzern bearbeitet werden'],
            [$id++, 'de', 'Dezimalzahl (1,1 oder 1.2)'],
            [$id++, 'de', 'Html'],
            [$id++, 'de', 'Ganze Zahl'],
            [$id++, 'de', 'Text - mehrere Zeilen'],
            [$id++, 'de', 'Text - eine Zeile'],
            [$id++, 'de', 'Ja (1) / Nein (0)'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        TranslationSource::deleteAll(['category' => 'app.settings']);
    }
}
