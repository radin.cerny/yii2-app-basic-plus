<?php

namespace app\modules\settings;

use app\modules\BaseModule;

class Module extends BaseModule
{
    public $defaultRoute = 'settings';
}
