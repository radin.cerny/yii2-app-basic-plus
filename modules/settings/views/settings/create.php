<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\settings\models\Settings $model */

$this->title = Yii::t('app.settings', 'Create Settings');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
