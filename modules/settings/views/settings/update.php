<?php

/** @var yii\web\View $this */
/** @var bool $onlyValue */
/** @var app\modules\settings\models\Settings $model */

$this->title = Yii::t('app.settings', 'Update Settings: {name}', [
    'name' => $model->id,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');

if ($onlyValue) {
    $this->params['breadcrumbs'] = [];
    if ($model->lang) {
        $this->params['breadcrumbs'][] = ['label' => strtoupper($model->lang)];
    }
    $this->params['breadcrumbs'][] = ['label' => $model->description];
}

?>
<div class="settings-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
