<?php

use app\modules\settings\models\Settings;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\models\search\SettingsSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app.settings', 'Settings');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->user->can('settings\Settings.actionCreate')): ?>
        <p>
            <?= Html::a(Yii::t('app.settings', 'Create Settings'), ['create'], ['class' => 'btn btn-success']) ?>
        </p>
    <?php endif; ?>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'tableOptions' => ['class' => 'table table-hover table-striped'],
        'columns' => [
            [
                'attribute' => 'id',
                'format' => 'text',
                //'label' => '',
                //'filter' => ['id' => 'Some text'],
                //'filterInputOptions' => [
                //    'class' => 'form-control',
                //    'prompt' => '=== ALL ===',
                //],
                //'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
                //'contentOptions' => ['style' => 'text-align:center;'],
                //'value' => function ($model, $key, $index, $column) {
                //},
            ],
            [
                'attribute' => 'type',
                'format' => 'text',
            ],
            [
                'attribute' => 'section',
                'format' => 'text',
            ],
            [
                'attribute' => 'key',
                'format' => 'text',
            ],
            [
                'attribute' => 'value',
                'format' => 'ntext',
                'value' => function ($model, $key, $index, $column) {
                    return \yii\helpers\StringHelper::truncateWords($model->value, 10);
                },
            ],
            [
                'attribute' => 'lang',
            ],
            [
                'attribute' => 'status',
                'format' => 'text',
                'headerOptions' => ['style' => 'width: 6rem; text-align:center;'],
            ],
            [
                'attribute' => 'user_edit',
                'format' => 'text',
                'label' => 'User edit',
                'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
            ],
            [
                'attribute' => 'description',
                'format' => 'text',
            ],
            [
                'class' => ActionColumn::class,
                'urlCreator' => function ($action, Settings $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'headerOptions' => ['style' => 'width: 8rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'template' => '{view} {update} {delete} {copy}',
                'buttons' => [
                    'copy' => function ($url, $model, $key) {
                        return Html::a(
                            '<i class="bi bi-copy"></i>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Copy'),
                            ]
                        );
                    },
                ],
                'visibleButtons' => [
                    'view' => function ($model, $key, $index) {
                        return Yii::$app->user->can('settings\Settings.actionView');
                    },
                    'update' => function ($model, $key, $index) {
                        return Yii::$app->user->can('settings\Settings.actionUpdate');
                    },
                    'delete' => function ($model, $key, $index) {
                        return Yii::$app->user->can('settings\Settings.actionDelete');
                    },
                    'copy' => function ($model, $key, $index) {
                        return Yii::$app->user->can('settings\Settings.actionCopy');
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>

