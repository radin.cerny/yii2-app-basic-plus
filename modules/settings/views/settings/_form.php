<?php

use app\modules\intl\models\Language;
use app\modules\settings\models\Settings;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\settings\models\Settings $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="settings-form">

    <?php
    $form = ActiveForm::begin();

    if ($model->isScenario(Settings::SCENARIO_UPDATE_VALUE)) {
        echo $form->field($model, 'value')->textarea(['rows' => 15]);
    } else {
        echo $form->field($model, 'type')->dropDownList(Settings::getType_listData());

        echo $form->field($model, 'delimiter')->dropDownList(Settings::getAllowedDelimiters_listData(), [
            'prompt' => '— ' . Yii::t('app', 'No delimiter') . ' —'
        ]);

        echo $form->field($model, 'section')->textInput(['maxlength' => true]);

        echo $form->field($model, 'key')->textInput(['maxlength' => true]);

        echo $form->field($model, 'value')->textarea(['rows' => 15]);

        echo $form->field($model, 'status')->textInput();

        echo $form->field($model, 'user_edit')->dropDownList([
            0 => Yii::t('app', 'No'),
            1 => Yii::t('app', 'Yes'),
        ]);

        echo $form->field($model, 'description')->textInput(['maxlength' => true]);

        echo $form->field($model, 'lang')->dropDownList(Language::getEnabledLanguages_listData(), [
            'prompt' => Yii::t('app', '-- Any --')
        ]);
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
