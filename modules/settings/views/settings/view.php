<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/** @var yii\web\View $this */
/** @var app\modules\settings\models\Settings $model */

$this->title = $model->section . ' > ' . $model->key;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app.settings', 'Settings'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="settings-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'type',
            'section',
            'key',
            'lang',
            'value:ntext',
            'status',
            'description',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
