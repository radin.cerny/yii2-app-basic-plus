<?php

namespace app\modules\settings\components;

use app\modules\settings\models\Settings;
use Yii;
use yii\base\BaseObject;

/**
 * Class Settings
 *
 */
class SettingsComponent extends BaseObject
{
    /**
     * @var array list of sections that were already queried. They are stored so they can be reused.
     */
    protected array $settings = [];

    public static function getCurrentLanguage($default = 'en')
    {
        return explode('-', Yii::$app->language)[0] ?? $default;
    }

    /**
     * Get the value for the given section and key.
     *
     * @param string $section
     * @param string $key
     * @param null $default
     * @param null $lang
     *
     * @return mixed
     */
    public function get($section, $key, $lang = null, $default = null, $encode = true)
    {
        if (!isset($this->settings[$section][$key][$lang])) {
            $this->settings[$section] = $this->loadSectionFromDb($section);
        }

        if (!isset($this->settings[$section][$key])) {
            return $default;
        }

        $lang = $lang ?? $this->getCurrentLanguage();
        /** @var Settings $model */
        $model = $this->settings[$section][$key][$lang] ?? $this->settings[$section][$key][null] ?? null;

        if ($model) {
            return $model->convertToDatatype($encode);
        }

        return $default;
    }

    /**
     * Add a new setting or update an existing one.
     *
     * @param null $section
     * @param string $key
     * @param string $value
     * @param null $type
     *
     * @return bool
     */
    public function set($section, $key, $value = null, $lang = null, $delimiter = null, $type = null, $description = null): bool
    {
        if (isset($this->settings[$section][$key][$lang])) {
            // updating
            $model = $this->settings[$section][$key][$lang] ?? null;
            if (!$model) {
                return false;
            }
            if ($type) {
                $model->type = $type;
            }
            if ($delimiter) {
                $model->delimiter = $delimiter;
            }
            if ($value) {
                $model->value = $value;
            }
            if ($description) {
                $model->description = $description;
            }
            return $model->save();
        } else {
            $model = new Settings();
            $model->type = $type;
            $model->delimiter = $delimiter;
            $model->section = $section;
            $model->key = $key;
            $model->value = $value;
            $model->description = $description;
            $model->lang = $lang;
            if ($model->save()) {
                $this->settings[$section][$key][$lang] = $model;
                return true;
            }
            return false;
        }
    }

    private function loadSectionFromDb($section)
    {
        $result = [];
        foreach (Settings::find()->bySection($section)->all() as $model) {
            $result[$model->key][$model->lang] = $model;
        }
        return $result;
    }
}
