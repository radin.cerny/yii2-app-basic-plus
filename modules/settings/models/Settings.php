<?php

namespace app\modules\settings\models;

use app\models\BaseModel;
use app\modules\settings\models\query\SettingsQuery;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "{{%settings}}".
 *
 * @property int $id
 * @property string $type
 * @property string $delimiter
 * @property string $section
 * @property string $key
 * @property string $value
 * @property string $lang
 * @property int $status
 * @property bool $user_edit
 * @property string|null $description
 * @property string $created_at
 * @property string|null $updated_at
 */
class Settings extends BaseModel
{
    // (integer) is an alias of (int)
    // (boolean) is an alias of (bool)
    // (binary) is an alias of (string)
    // (double) and (real) are aliases of (float)
    // These are not recommended.

    public const TYPE_INT = 'int';
    public const TYPE_BOOL = 'bool';
    public const TYPE_FLOAT = 'float';

    /**
     * More lines of simple text, new lines allowed, no tags
     */
    public const TYPE_TEXT = 'text';

    /**
     * One line of simple text, no new lines, no tags
     */
    public const TYPE_STRING = 'string';

    /**
     * Any HTML code
     */
    public const TYPE_HTML = 'html';

    //const TYPE_NULL = 'null';

    public const DELIMITER_NEW_LINE = '<newline>';
    public const DELIMITER_SEMICOLON = ';';

    public const SCENARIO_UPDATE_VALUE = 'SCENARIO_UPDATE_VALUE';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%sys_settings}}';
    }

    public static function getType_listData()
    {
        return [
            self::TYPE_INT => Yii::t('app.settings', 'Integer'),
            self::TYPE_BOOL => Yii::t('app.settings', 'Yes (1) / No (0)'),
            self::TYPE_FLOAT => Yii::t('app.settings', 'Decimal number (1,1 or 1.2)'),
            self::TYPE_STRING => Yii::t('app.settings', 'Text - one line'),
            self::TYPE_TEXT => Yii::t('app.settings', 'Text - multi line'),
            self::TYPE_HTML => Yii::t('app.settings', 'Html'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\settings\models\query\SettingsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SettingsQuery(get_called_class());
    }

    public static function getAllowedDelimiters_listData()
    {
        return [
            self::DELIMITER_SEMICOLON => '; ' . Yii::t('app', 'Semicolon'),
            self::DELIMITER_NEW_LINE => Yii::t('app', 'New line'),
        ];
    }

    public function convertToDatatype($encode = true)
    {
        $exploded = [$this->value];

        if ($this->delimiter) {
            if ($this->delimiter == self::DELIMITER_NEW_LINE) {
                $exploded = preg_split('/\r\n|\r|\n/', $this->value);
            } else {
                $exploded = explode($this->delimiter, $this->value);
            }
        }

        foreach ($exploded as $k => $v) {
            switch ($this->type) {
                case Settings::TYPE_INT:
                    $exploded[$k] = (int)$v;
                    break;
                case Settings::TYPE_BOOL:
                    $exploded[$k] = (bool)$v;
                    break;
                case Settings::TYPE_FLOAT:
                    $exploded[$k] = (float)$v;
                    break;
                case Settings::TYPE_HTML:
                case Settings::TYPE_STRING:
                case Settings::TYPE_TEXT:
                    if ($encode) {
                        $exploded[$k] = Html::encode($v);
                    }
                    break;
            }
        }

        if ($this->delimiter) {
            return $exploded;
        }

        return $exploded[0];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_UPDATE_VALUE] = ['value'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'section', 'key', 'value'], 'required'],
            [['value'], 'formatValue'],
            [['status'], 'integer'],
            [['user_edit'], 'boolean'],
            [['delimiter'], 'in', 'range' => array_keys(self::getAllowedDelimiters_listData())],
            [['type'], 'string', 'max' => 10],
            [['section', 'key', 'description', 'lang'], 'string', 'max' => 255],
            [['lang', 'delimiter'], 'default', 'value' => null],
        ];
    }

    public function formatValue($attribute, $params)
    {
        switch ($this->type) {
            case Settings::TYPE_STRING:
                $this->value = strip_tags($this->value);
                $this->value = str_replace(["\r\n"], ' ', $this->value);
                $this->value = str_replace(["\n", "\r"], ' ', $this->value);
                break;
            case Settings::TYPE_TEXT:
                // new lines allowed
                $this->value = strip_tags($this->value);
                break;
            case Settings::TYPE_FLOAT:
                $this->value = (float)str_replace(',', '.', $this->value);
                break;
            case Settings::TYPE_BOOL:
                $this->value = (int)(bool)$this->value;
                break;
            case Settings::TYPE_INT:
                $this->value = (int)$this->value;
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'section' => Yii::t('app', 'Section'),
            'key' => Yii::t('app', 'Key'),
            'value' => Yii::t('app', 'Value'),
            'status' => Yii::t('app', 'Status'),
            'user_edit' => Yii::t('app.settings', 'Can be edited by users'),
            'description' => Yii::t('app', 'Description'),
            'active' => Yii::t('app', 'Active'),
            'created_at' => Yii::t('app', 'Created at'),
            'created_by' => Yii::t('app', 'Created by'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'lang' => Yii::t('app', 'Language'),
            'position' => Yii::t('app', 'Position'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'updated_by' => Yii::t('app', 'Updated by'),
        ];
    }
}
