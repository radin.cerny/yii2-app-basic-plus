<?php

namespace app\modules\settings\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Settings]].
 *
 * @see \app\models\Settings
 */
class SettingsQuery extends \yii\db\ActiveQuery
{
    /**
     * {@inheritdoc}
     * @return \app\models\Settings[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Settings|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function bySection($section)
    {
        return $this->andWhere([
            'section' => $section,
        ]);
    }
}
