<?php

namespace app\modules\admin\components;

use Yii;
use yii\base\BaseObject;

/**
 * Setup in web.php:
 * 'components' => [
 *   'csvExporter' => [
 *     'class' => 'app\modules\admin\components\CsvExporterComponent',
 *   ],
 * ],
 *
 * Usage:
 * - Yii::$app->csvExporter->toResponse(User::find()->all(), 'file.csv');
 * - Yii::$app->csvExporter->toArray(User::find()->all());
 *
 * Models must implement:
 * - public function getCsvRowArray()
 * - public function getCsvHeaderArray()
 *
 */
class CsvExporterComponent extends BaseObject
{

    /**
     * @var string
     */
    public $separator = ';';

    /**
     * @var string
     */
    public $newLine = "\n";

    /**
     * @param array $models
     * @param string $filename
     * @param bool $inline
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function toResponse(array $models, string $filename, bool $inline = false)
    {
        $this->sendAsResponse($this->toArray($models), $filename, $inline);
    }

    /**
     * @param array $models
     * @return string[]
     */
    public function toArray(array $models)
    {
        if (empty($models)) {
            return [];
        }

        $rows = [
            $this->getFirstRowPrefix() . implode($this->separator, $models[0]->getCsvHeaderArray()),
        ];

        foreach ($models as $m) {
            $rows[] = trim(implode($this->separator, $m->getCsvRowArray()));
        }
        return $rows;

    }

    /**
     * UTF-8 header = chr(0xEF) . chr(0xBB) . chr(0xBF)
     *
     * @return string
     */
    public function getFirstRowPrefix()
    {
        return chr(0xEF) . chr(0xBB) . chr(0xBF);
    }

    /**
     * @param $rows
     * @param $filename
     * @param false $inline
     * @return \yii\console\Response|\yii\web\Response
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    private function sendAsResponse($rows, $filename, $inline = false)
    {
        return Yii::$app->response->sendContentAsFile(implode($this->newLine, $rows), $filename, [
            'mimeType' => 'application/csv',
            'inline' => $inline
        ]);
    }

}
