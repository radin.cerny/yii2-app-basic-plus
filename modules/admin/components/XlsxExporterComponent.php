<?php

namespace app\modules\admin\components;

use app\helpers\FileHelper;
use app\modules\admin\jobs\XlsxExporterQueueJob;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Yii;
use yii\base\BaseObject;

/**
 * Setup in web.php:
 * 'components' => [
 *   'xlsExporter' => [
 *     'class' => 'app\modules\admin\components\XlsxExporterComponent',
 *   ],
 * ],
 *
 * Usage:
 * - Yii::$app->xlsxExporter->toResponse(User::find()->all(), 'file.xlsx');
 * - Yii::$app->xlsxExporter->toArray(User::find()->all());
 *
 * Models must implement:
 * - public function getXlsxRowArray()
 * - public function getXlsxHeaderArray()
 *
 */
class XlsxExporterComponent extends BaseObject
{

    public function createXlsx(array $models, string $filename, bool $sendXlsxToBrowser = true)
    {
        $headers = $models[0]->getXlsxHeaderArray();

        $spreadsheet = new Spreadsheet();
        $activeWorksheet = $spreadsheet->getActiveSheet();
        $activeWorksheet->fromArray($headers, null, 'A1', false);

        foreach ($models as $i => $m) {
            $rowData = $m->getXlsxRowArray();
            $activeWorksheet->fromArray($rowData, null, 'A' . ($i + 2), false);
        }

        $writer = new Xlsx($spreadsheet);

        if ($sendXlsxToBrowser) {

            ob_start();
            ob_implicit_flush(false);
            $writer->save('php://output');
            $file = ob_get_clean();

            return Yii::$app->response->sendContentAsFile($file, $filename, [
                'mimeType' => FileHelper::getMimeTypeByExtension($filename),
                'inline' => false
            ]);
        }

        $writer->save($filename);

    }

    public function createXlsxQueue(array $modelIds, string $fileName, int $idUserRequester)
    {
        Yii::$app->queue->push(new XlsxExporterQueueJob([
            'modelIds' => $modelIds,
            'fileName' => $fileName,
            'idUserRequester' => $idUserRequester,
        ]));
    }
}
