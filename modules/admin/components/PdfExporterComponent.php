<?php

namespace app\modules\admin\components;

use yii\base\BaseObject;

/**
 * Setup in web.php:
 * 'components' => [
 *   'pdfExporter' => [
 *     'class' => 'app\modules\admin\components\PdfExporterComponent',
 *   ],
 * ],
 *
 * Usage:
 * - $pdf = Yii::$app->pdfExporter->createPdf();
 * - $pdf->writeHtml($model->getPdfHtml());
 * - $pdf->Output('name.pdf', 'I');
 *
 * Models must implement:
 * - public function getPdfHtml()
 *
 */
class PdfExporterComponent extends BaseObject
{

    public $orientation = 'P';
    public $unit = 'mm';
    public $format = 'A4';
    public $unicode = true;
    public $encoding = 'UTF-8';
    public $diskcache = false;
    public $pdfa = false;

    public $creator = '';
    public $author = '';
    public $title = '';
    public $subject = '';

    public $marginLeft = 5;
    public $marginTop = 5;
    public $marginRight = 5;
    public $keepMargins = true;

    public $pageBreak = true;
    public $pageBreakMargin = 5;

    public $fontFamily = 'dejavusanscondensed';
    public $fontStyle = '';
    public $fontSize = 10;

    public function createPdf(): \TCPDF
    {
        // create new PDF document
        $pdf = new \TCPDF($this->orientation, $this->unit, $this->format, $this->unicode, $this->encoding, $this->diskcache, $this->pdfa);

        // set document information
        $pdf->SetCreator($this->creator);
        $pdf->SetAuthor($this->author);
        $pdf->SetTitle($this->title);
        $pdf->SetSubject($this->subject);

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        $pdf->SetMargins($this->marginLeft, $this->marginTop, $this->marginRight, $this->keepMargins);

        // set auto page breaks
        $pdf->SetAutoPageBreak($this->pageBreak, $this->pageBreakMargin);

        $pdf->SetFont($this->fontFamily, $this->fontStyle, $this->fontSize);

        $pdf->AddPage();

        return $pdf;
    }
}
