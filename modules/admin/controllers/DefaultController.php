<?php

namespace app\modules\admin\controllers;

use app\jobs\DemoQueueJob;
use Yii;
use yii\web\Controller;

/**
 * Default controller for the `admin` module
 */
class DefaultController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionQueue()
    {
        Yii::$app->queue->push(new DemoQueueJob([
            'text' => 'Hello world',
        ]));
    }

    public function actionEmail()
    {
        // If you are using your Gmail account, your gmail will always be the sender.
        // You cannot change it.
        Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo(Yii::$app->params['adminEmail'])
            ->setSubject('Yii test email')
            ->setTextBody('Plain text content')
            ->setHtmlBody('<b>HTML content</b>')
            ->send();
    }
}
