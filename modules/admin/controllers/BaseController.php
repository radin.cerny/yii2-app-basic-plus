<?php

namespace app\modules\admin\controllers;

use app\modules\user\helpers\RbacHelper;
use Yii;

class BaseController extends \yii\web\Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        if (!env('RBAC_ENABLED')) {
            return []; // uncomment to disable RBAC
        }

        $currentActionMethod = Yii::$app->controller->action->actionMethod; // actionUpdate
        $controller = RbacHelper::reduceControllerClassPath(get_class($this)); // user\UserManager

        return [
            'access' => [
                'class' => \yii\filters\AccessControl::class,
                'rules' => [
                    [
                        // If current user has access rights ...
                        // See auth_* tables in the DB (Note that only ROLES are assigned to users. Never individual permissions!)
                        // Only check if user has individual permissions, never roles!
                        // @link https://www.yiiframework.com/doc/guide/2.0/en/security-authorization
                        'allow' => true,
                        'permissions' => [$controller . RbacHelper::PREFIX_PERMISSION_NAME . $currentActionMethod],
                    ],
                ],
            ],
        ];
    }
}
