<?php

use app\migrations\BaseMigration;
use app\modules\documents\controllers\DocumentController;
use app\modules\documents\controllers\DocumentGroupController;
use app\modules\documents\controllers\DocumentGroupItemController;
use app\modules\intl\controllers\LanguageController;
use app\modules\intl\controllers\TranslationController;
use app\modules\intl\controllers\TranslationSourceController;
use app\modules\location\controllers\AddressController;
use app\modules\location\controllers\CompanyController;
use app\modules\location\controllers\LocationController;
use app\modules\message\controllers\MessageController;
use app\modules\message\controllers\MessageRecipientController;
use app\modules\settings\controllers\SettingsController;
use app\modules\user\controllers\UserManagerController;
use app\modules\user\helpers\RbacHelper;

/**
 * Class m990000_140000_rbac_mapping
 */
class m990000_140000_rbac_mapping extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $authManager = Yii::$app->authManager;

        $admin = $authManager->getRole(RbacHelper::ROLE_GLOBAL_ADMIN);
        $creator = $authManager->getRole(RbacHelper::ROLE_GLOBAL_CREATOR);
        $reader = $authManager->getRole(RbacHelper::ROLE_GLOBAL_READER);
        $updater = $authManager->getRole(RbacHelper::ROLE_GLOBAL_UPDATER);
        $deleter = $authManager->getRole(RbacHelper::ROLE_GLOBAL_DELETER);

        $controllers = [
            AddressController::class,
            CompanyController::class,
            DocumentController::class,
            DocumentGroupController::class,
            DocumentGroupItemController::class,
            LanguageController::class,
            LocationController::class,
            MessageController::class,
            MessageRecipientController::class,
            SettingsController::class,
            TranslationController::class,
            TranslationSourceController::class,
            UserManagerController::class,
        ];

        /** @var ?int $value */
        foreach ($controllers as $classPath) {
            $controller = RbacHelper::reduceControllerClassPath($classPath);
            $authManager->addChild($admin, $authManager->getRole($controller . RbacHelper::PREFIX_ROLE_NAME . 'admin'));
            $authManager->addChild($creator, $authManager->getRole($controller . RbacHelper::PREFIX_ROLE_NAME . 'creator'));
            $authManager->addChild($reader, $authManager->getRole($controller . RbacHelper::PREFIX_ROLE_NAME . 'reader'));
            $authManager->addChild($updater, $authManager->getRole($controller . RbacHelper::PREFIX_ROLE_NAME . 'updater'));
            $authManager->addChild($deleter, $authManager->getRole($controller . RbacHelper::PREFIX_ROLE_NAME . 'deleter'));
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";
        return false;
    }
}
