<?php

namespace app\modules\admin\jobs;

use app\modules\documents\models\Document;
use app\modules\user\models\User;
use Yii;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class XlsxExporterQueueJob extends BaseObject implements JobInterface
{
    public $modelIds;
    public $fileName;
    public $idUserRequester;

    public function execute($queue)
    {
        $users = User::find()
            ->andWhere(['id' => $this->modelIds])
            ->all();

        $jobOutputFolder = Yii::getAlias('@app/modules/admin/jobs/output/');
        $filePath = $jobOutputFolder . $this->fileName;

        Yii::$app->xlsxExporter->createXlsx($users, $filePath, false);

        $document = Document::createFromFile($this->idUserRequester, $filePath);

        $systemUser = User::findOne(Yii::$app->params['systemUserId']);

        Yii::$app->messenger
            ->notification('file-created', [
                'jobName' => 'XLSX export',
                'fileName' => $this->fileName,
                'documentUrl' => $document->getUrl(false, true),
            ])
            ->setSender($systemUser)
            ->addRecipientTo(User::findOne($this->idUserRequester))
            ->send(); // You can omit this if you want to send later.
    }
}
