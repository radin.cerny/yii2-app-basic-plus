<?php

use app\modules\location\models\Location;
use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var Location $model */

?>

<div class="location-map-detail">
    <h1><?= Html::encode($model->name) ?></h1>
</div>
