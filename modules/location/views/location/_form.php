<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\location\models\Location $model */
/** @var yii\widgets\ActiveForm $form */

$colClass = 'col-12 col-md-4 col-lg-3 col-xl-2';
?>

<div class="location-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'id_company')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'id_parent')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'id_address')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'id_type')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'descr')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'x')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'xx')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'y')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'yy')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'z')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'zz')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'floor')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'rotation')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'transparent')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'fill_color')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'stroke_color')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'max_load')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'gps_lat')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'gps_lng')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'zindex')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'children_offset_x')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'children_offset_y')->textInput() ?>
        </div>
        <div class="<?= $colClass ?>">
            <?= $form->field($model, 'active')->textInput() ?>
        </div>
    </div>

    <?= $form->field($model, 'svg')->textarea(['style' => 'height:20rem;']) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
