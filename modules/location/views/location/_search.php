<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\location\models\search\LocationSearch $model */
/** @var yii\widgets\ActiveForm $form */
?>

<div class="location-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'id_company') ?>

    <?= $form->field($model, 'id_parent') ?>

    <?= $form->field($model, 'id_address') ?>

    <?= $form->field($model, 'name') ?>

    <?php // echo $form->field($model, 'descr') ?>

    <?php // echo $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'x') ?>

    <?php // echo $form->field($model, 'xx') ?>

    <?php // echo $form->field($model, 'y') ?>

    <?php // echo $form->field($model, 'yy') ?>

    <?php // echo $form->field($model, 'z') ?>

    <?php // echo $form->field($model, 'zz') ?>

    <?php // echo $form->field($model, 'floor') ?>

    <?php // echo $form->field($model, 'max_load') ?>

    <?php // echo $form->field($model, 'gps_lat') ?>

    <?php // echo $form->field($model, 'gps_lng') ?>

    <?php // echo $form->field($model, 'active') ?>

    <?php // echo $form->field($model, 'created_by') ?>

    <?php // echo $form->field($model, 'updated_by') ?>

    <?php // echo $form->field($model, 'deleted_by') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'updated_at') ?>

    <?php // echo $form->field($model, 'deleted_at') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('app', 'Reset'), ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
