<?php

use app\modules\location\helpers\LocationRenderer;
use app\modules\location\models\Location;
use app\widgets\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\YiiAsset;

/** @var yii\web\View $this */
/** @var Location $model */
/** @var LocationRenderer $locationRenderer */
/** @var array $displayLevelsItems */
/** @var array $labelLevelsItems */
/** @var array $floorItems */
/** @var array $parentLocations_listData */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Map'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
YiiAsset::register($this);


$mapXX = 1700; // $model->xx / 400;
$mapYY = $mapXX * ($model->yy / $model->xx);
$cssUnit = 'px';
$mapXX .= $cssUnit;
$mapYY .= $cssUnit;

$parentCount = count($parentLocations_listData);
?>
<div class="location-map" style="height:1000px">

    <h1><?= Html::encode($model->name) ?></h1>


    <div class="container text-center alert alert-light">
        <div class="row">
            <div class="col-2">
                <?php
                if ($parentCount) {
                    if ($parentCount == 1) {
                        echo Html::a('<i class="bi bi-arrow-90deg-up"></i>', ['map', 'id' => array_key_first($parentLocations_listData)], ['class' => 'btn btn-warning']);
                    } else {
                        echo Html::beginForm(['map'], 'get', ['csrf' => false, 'class' => 'd-inline']);
                        echo Html::dropDownList('id', null, $parentLocations_listData, ['class' => 'form-select d-inline mx-1', 'style' => 'width:8rem;']);
                        echo Html::submitButton('<i class="bi bi-arrow-90deg-up"></i>', ['class' => 'btn btn-warning']);
                        echo Html::endForm();
                    }
                }
                ?>

            </div>
            <div class="col">
                <?= Html::beginForm(); ?>
                <div class="row">
                    <div class="col">
                        <?= Html::dropDownList('floor', $locationRenderer->floor, $floorItems, ['class' => 'form-select']); ?>
                    </div>
                    <div class="col">
                        <?= Html::dropDownList('displayLevels', $locationRenderer->displayLevels, $displayLevelsItems, ['class' => 'form-select']); ?>
                    </div>
                    <div class="col">
                        <?= Html::dropDownList('labelLevels', $locationRenderer->labelLevels, $labelLevelsItems, ['class' => 'form-select']); ?>
                    </div>
                    <div class="col-1 px-0">
                        <?= Html::submitButton('OK', ['class' => 'btn btn-success']); ?>
                        <?= Html::a('<i class="bi bi-info-circle"></i>', ['map-detail', 'id' => $model->id], ['class' => 'btn btn-info ' . Modal::openInModalClass]) ?>
                    </div>
                </div>
                <?= Html::endForm(); ?>
            </div>
        </div>
    </div>

    <div id="map" class="mx-auto p-0 position-relative" style="width: <?= $mapXX ?>; height: <?= $mapYY ?>;">
        <?php
        $model->x = 0;
        $model->y = 0;
        echo $locationRenderer->render($model);
        ?>
    </div>

</div>

<script>
    const mapLocations = document.querySelectorAll('#map .<?=$locationRenderer->clickableClass?>')
    let highlightColor = 'yellow'
    mapLocations.forEach((mapLocation, index) => {
        mapLocation.addEventListener('click', (event) => {
            let clickedElement = event.target
            if (clickedElement.nodeName.toLowerCase() == 'div') {
                // DIV was clicked
                let origBackground = mapLocation.style.background
                mapLocation.style.background = highlightColor
                setTimeout(() => {
                    mapLocation.style.background = origBackground
                }, 500)
            } else {
                if (clickedElement.nodeName.toLowerCase() == 'text') {
                    // if an SVG text was clicked, we want to highlight the underlying object
                    clickedElement = clickedElement.parentElement.querySelector('.<?=$locationRenderer->clickableClass?>:not(text)')
                }
                // SVG image was clicked. <path>, <rectangle> etc
                let origFill = clickedElement.style.fill
                clickedElement.style.fill = highlightColor
                setTimeout(() => {
                    clickedElement.style.fill = origFill
                }, 500)
            }
            location.href = '<?=Url::to(['map', 'id' => ''])?>' + mapLocation.getAttribute('data-location-id')
        })
    })

    // Recalculating viewBox, because <svg> tags are dynamically resized
    const svgs = document.querySelectorAll('svg');
    svgs.forEach(svg => {
        try {
            const bbox = svg.getBBox();
            svg.setAttribute('viewBox', `${bbox.x} ${bbox.y} ${bbox.width} ${bbox.height}`);
            svg.setAttribute('preserveAspectRatio', 'xMidYMid meet');
        } catch (error) {
            console.warn(`Failed to process SVG:`, svg, error);
        }
    });
</script>