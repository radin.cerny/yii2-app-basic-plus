<?php

use app\modules\location\models\Address;
use app\modules\location\models\Company;
use app\modules\location\models\Location;
use app\modules\location\models\LocationType;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\location\models\search\LocationSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Locations');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="location-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a(Yii::t('app', 'Create Location'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['style' => 'width: 5rem; text-align:center;'],
            ],
            [
                'attribute' => 'id_company',
                'filter' => Company::getCompanies_listData(),
                'filterInputOptions' => [
                    'class' => 'form-select text-center',
                    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                ],
                'headerOptions' => ['style' => 'width: 15rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (Location $model, $key, $index, $column) {
                    return $model->company ? $model->company->name : null;
                },
            ],
            [
                'attribute' => 'id_parent',
                'format' => 'html',
                'headerOptions' => ['style' => 'width: 15rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (Location $model, $key, $index, $column) {
                    return $model->parent ? $model->parent->name : null;
                },
            ],
            [
                'attribute' => 'id_address',
                'format' => 'html',
                'filter' => Address::getAddresses_listData(),
                'filterInputOptions' => [
                    'class' => 'form-select text-center',
                    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                ],
                'headerOptions' => ['style' => 'width: 15rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (Location $model, $key, $index, $column) {
                    return $model->address ? $model->address->getAddressLine('{street_nr}, {city}<br>{zip}, {country}') : null;
                },
            ],
            'name',
            [
                'attribute' => 'id_type',
                'format' => 'html',
                'filter' => LocationType::getFilterData('id', 'name'),
                'filterInputOptions' => [
                    'class' => 'form-select text-center',
                    'prompt' => ['text' => '— ' . Yii::t('app', 'All') . ' —', 'options' => ['value' => '']],
                ],
                'headerOptions' => ['style' => 'width: 10rem; text-align:center;'],
                'contentOptions' => ['style' => 'text-align:center;'],
                'value' => function (Location $model, $key, $index, $column) {
                    $locationType = $model->type ?? null;
                    if ($locationType) {
                        return '<span class="badge rounded-pill text-bg-primary">' . $locationType->name . '</span>';
                    }
                    return $locationType;
                },
            ],
            //'descr',
            //'x',
            //'xx',
            //'y',
            //'yy',
            //'z',
            //'zz',
            //'floor',
            //'max_load',
            //'gps_lat',
            //'gps_lng',
            //'active',
            //'created_by',
            //'updated_by',
            //'deleted_by',
            //'created_at',
            //'updated_at',
            //'deleted_at',
            [
                'class' => ActionColumn::className(),
                'template' => '{map} {view} {update} {delete}',
                'contentOptions' => ['style' => 'text-align:right;'],
                'urlCreator' => function ($action, Location $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                },
                'buttons' => [
                    'map' => function ($url, $model, $key) {
                        return Html::a(
                            '<i class="bi bi-columns-gap"></i>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Map'),
                            ]
                        );
                    },
                ],
                'visibleButtons' => [
                    'map' => function (Location $model, $key, $index) {
                        return !empty($model->children);
                    },
                ],
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
