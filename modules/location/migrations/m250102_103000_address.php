<?php

use app\migrations\BaseMigration;

/**
 * Class m250102_103000_address
 */
class m250102_103000_address extends BaseMigration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%loc_address}}';

        $this->createTable($tableName, [
            'street_nr' => $this->string(),
            'zip' => $this->string(),
            'city' => $this->string(),
            'country' => $this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = '{{%loc_address}}';
        $this->dropTable($tableName);
    }
}
