<?php

use app\modules\location\controllers\AddressController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m250102_103010_address_rbac
 */
class m250102_103010_address_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            AddressController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        RbacHelper::deleteAuthItemsForController(AddressController::class);
    }
}
