<?php

use app\modules\location\controllers\CompanyController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m250102_103110_company_rbac
 */
class m250102_103110_company_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            CompanyController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        RbacHelper::deleteAuthItemsForController(CompanyController::class);
    }
}
