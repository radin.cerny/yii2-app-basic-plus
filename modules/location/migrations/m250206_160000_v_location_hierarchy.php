<?php

use app\modules\location\models\VLocationHierarchy;
use yii\db\Migration;

/**
 * Class m250206_160000_v_location_hierarchy
 */
class m250206_160000_v_location_hierarchy extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $delimiter = VLocationHierarchy::DELIMITER;

        $sql = <<<SQL
CREATE VIEW v_location_hierarchy AS
WITH RECURSIVE LocationHierarchy AS
(
  SELECT 
   id, 
   id_parent, 
   id_company, 
   id_type, 
   `name` AS name_path, 
   CAST(id AS VARCHAR(255)) as id_path, 
   1 AS depth
  FROM loc_location
  WHERE id_parent IS NULL
    
  UNION
  
  SELECT 
   child.id,
   child.id_parent,
   child.id_company,
   child.id_type,
   CONCAT(parent.name_path, '$delimiter', child.name) AS name_path,
   CONCAT(parent.id_path, '$delimiter', child.id) AS id_path,
   parent.depth + 1 AS depth
  FROM loc_location child
  INNER JOIN LocationHierarchy parent ON parent.id = child.id_parent
  
)
SELECT *
FROM LocationHierarchy
SQL;

        $this->execute($sql);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->execute('DROP VIEW v_location_hierarchy');
    }
}
