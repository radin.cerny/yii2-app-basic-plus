<?php

use app\migrations\BaseMigration;

/**
 * Class m250102_103100_company
 */
class m250102_103100_company extends BaseMigration
{

    public $createColumnActive = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%loc_company}}';
        $tableNameTrim = 'loc_company';

        $this->createTable($tableName, [
            'id_address' => $this->integer(),
            'name' => $this->string(),
            'shortname' => $this->string(),
            'crn' => $this->string()->comment('Company Registration Number (CZ = IČO)'),
            'vatin' => $this->string()->comment('VAT Identification Number (CZ = DIČ)'),
        ]);

        $this->addForeignKey("fk-$tableNameTrim-address", $tableName, 'id_address', '{{%loc_address}}', 'id');

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = '{{%loc_company}}';
        $this->dropTable($tableName);
    }
}
