<?php

use app\migrations\BaseMigration;

/**
 * Class m250102_103200_location_type
 */
class m250102_103200_location_type extends BaseMigration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%loc_location_type}}';

        $this->createTable($tableName, [
            'name' => $this->string(),
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = '{{%loc_location_type}}';
        $this->dropTable($tableName);
    }
}
