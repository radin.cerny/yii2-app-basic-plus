<?php

use app\migrations\BaseMigration;

/**
 * Class m250102_103205_location
 */
class m250102_103205_location extends BaseMigration
{

    public $createColumnActive = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%loc_location}}';
        $tableNameTrim = 'loc_location';

        $this->createTable($tableName, [
            'id_company' => $this->integer(),
            'id_parent' => $this->integer(),
            'id_address' => $this->integer(),
            'id_type' => $this->integer(),
            'name' => $this->string(),
            'descr' => $this->string(),
            'x' => $this->integer()->comment('X position [mm] (relative to the parent)'),
            'xx' => $this->integer()->comment('X dimension [mm]'),
            'y' => $this->integer()->comment('Y position  [mm](relative to the parent)'),
            'yy' => $this->integer()->comment('Y dimension [mm]'),
            'z' => $this->integer()->comment('Z position  [mm](relative to the parent)'),
            'zz' => $this->integer()->comment('Z dimension [mm]'),
            'floor' => $this->integer()->defaultValue(0)->notNull()->comment('Zero = ground floor'),
            'rotation' => $this->integer()->defaultValue(0)->comment('Clockwise rotation'),
            'fill_color' => $this->string()->defaultValue('silver')->comment('Background color'),
            'stroke_color' => $this->string()->defaultValue('black')->comment('Border color'),
            'max_load' => $this->integer()->comment('Max [kg/m2]'),
            'gps_lat' => $this->decimal(9, 6)->comment('GPS latitude, decimal number'),
            'gps_lng' => $this->decimal(9, 6)->comment('GPS longitude, decimal number'),
            'svg' => $this->text()->comment('SVG for the map of locations. Its circumscribed rectangle is given by cols: x,xx,y,yy. Rectangles can overlap.'),
            'zindex' => $this->integer()->comment('If a parent location has more overlapping children, you can specify the z-index to each child to make it clickable.'),
            'children_offset_x' => $this->integer()->defaultValue(0)->notNull()->comment('Offset for all children'),
            'children_offset_y' => $this->integer()->defaultValue(0)->notNull()->comment('Offset for all children'),
        ]);

        $this->addForeignKey("fk-$tableNameTrim-parent", $tableName, 'id_parent', '{{%loc_location}}', 'id');
        $this->addForeignKey("fk-$tableNameTrim-company", $tableName, 'id_company', '{{%loc_company}}', 'id');
        $this->addForeignKey("fk-$tableNameTrim-address", $tableName, 'id_address', '{{%loc_address}}', 'id');
        $this->addForeignKey("fk-$tableNameTrim-type", $tableName, 'id_type', '{{%loc_location_type}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = '{{%loc_location}}';
        $this->dropTable($tableName);
    }
}
