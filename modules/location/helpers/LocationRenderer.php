<?php

namespace app\modules\location\helpers;

use app\modules\location\models\Location;
use DOMDocument;
use DOMElement;
use Generator;
use yii\helpers\Html;

class LocationRenderer
{
    public int $floor = 0;
    public int $displayLevels;
    public int $labelLevels;
    public string $clickableClass = 'mapLocation';

    private int $currentLevel;
    private int $topParentXX;
    private int $topParentYY;

    public function render(Location $location)
    {
        $this->currentLevel = 1;
        $this->topParentXX = $location->xx;
        $this->topParentYY = $location->yy;

        $allMapElements = $this->getLocationHtml($location);
        $allMapElements = iterator_to_array($allMapElements, false);
        return implode('', $allMapElements);
    }

    /**
     * @param Location $location
     * @return Generator
     */
    private function getLocationHtml(Location $location): Generator
    {

        $y = 'top:' . ($location->y / $this->topParentYY) * 100 . '%;';
        $x = 'left:' . ($location->x / $this->topParentXX) * 100 . '%;';
        $width = 'width:' . ($location->xx / $this->topParentXX) * 100 . '%;';
        $height = 'height:' . ($location->yy / $this->topParentYY) * 100 . '%;';
        $absolute = 'position:absolute;';
        $zIndex = 'z-index:' . (($this->currentLevel * 100) + $location->zindex) . ';';

        $rotation = $location->rotation ? 'transform: rotate(' . $location->rotation . 'deg);' : '';

        if ($location->svg) {
            $dom = new DOMDocument();
            $dom->loadXML($location->svg);
            $svgElement = $dom->documentElement;
            $this->setSvgTitle($dom, $svgElement, $location->name);
            $svgElement->removeAttribute('width'); // removing w+h helps to fill the parent div
            $svgElement->removeAttribute('height');
            $svgElement->setAttribute('style', $absolute . 'display:block;' . $rotation . $y . $x . $width . $height . $zIndex); // Some SVGs were shifted, probably thanks to the CSS baseline.
            $svgElement->setAttribute('title', $location->name);

            $elements = $dom->getElementsByTagName('path');
            if ($elements->length > 0) {
                $element = $elements->item(0);

                $this->setDomElementStyle($element, 'fill', null);
                $element->setAttribute('fill', $location->fill_color);

                $this->setDomElementStyle($element, 'stroke', null);
                $element->setAttribute('stroke', $location->stroke_color);

                $this->setDomElementStyle($element, 'stroke-width', null);
                $element->setAttribute('stroke-width', '0.5');

                $element->setAttribute('class', $this->clickableClass);
                $element->setAttribute('data-location-id', $location->id);
            }

            if ($this->currentLevel <= $this->labelLevels) {
                $text = $dom->createElement('text', $location->name);
                $text->setAttribute('class', $this->clickableClass);
                $text->setAttribute('data-location-id', $location->id);
                $text->setAttribute('x', '50%');
                $text->setAttribute('y', '50%');
                $text->setAttribute('font-size', '30%');
                $text->setAttribute('fill', 'black');
                $text->setAttribute('text-anchor', 'middle');
                $text->setAttribute('dominant-baseline', 'middle');
//                if ($location->angle_text) {
//                    $text->setAttribute('transform', 'rotate(' . $location->angle_text . ')');
//                }
                $svgElement->appendChild($text);
            }
            yield $dom->saveXML();
        } else {
            $style = [];
            $style[] = 'font-size: 1rem;';
            $style[] = 'text-align:center;';
            $style[] = $absolute;
            $style[] = 'vertical-align:middle;';
            $style[] = $y;
            $style[] = $x;
            $style[] = $width;
            $style[] = $height;
            $style[] = 'display: flex;justify-content: center;align-items: center;';
            $style[] = $zIndex;
            $style[] = 'border:1px solid ' . $location->stroke_color . ';';
            $style[] = 'background-color:' . $location->fill_color . ';';
            $style[] = $rotation;

            $name = '';
            if ($this->currentLevel <= $this->labelLevels) {
                $name = '<div class="position-absolute" style="z-index: 0">' . $location->name . '</div>';
            }

            yield Html::tag('div', $name, [
                'data-location-id' => $location->id,
                'class' => $this->clickableClass,
                'style' => implode('', $style),
                'title' => $location->name,
            ]);
        }

        if ($this->currentLevel < $this->displayLevels) {
            /** @var Location $child */
            foreach ($location->children as $child) {
                if ($child->floor != $this->floor) {
                    continue;
                }
                $child->x += $location->x + (int)$location->children_offset_x;
                $child->y += $location->y + (int)$location->children_offset_y;
                $this->currentLevel++;
                yield from $this->getLocationHtml($child);
                $this->currentLevel--;
            }
        }
    }

    private function setDomElementStyle(DOMElement $path, $attribute, $newValue)
    {
        // Parse the 'style' attribute
        $style = $path->getAttribute('style');
        $styles = explode(';', $style);

        // Modify the 'fill' property
        $newStyles = [];
        foreach ($styles as $styleRule) {
            $styleParts = explode(':', $styleRule);
            if (count($styleParts) == 2) {
                $property = trim($styleParts[0]);
                $value = trim($styleParts[1]);
                if ($property === $attribute) {
                    if ($newValue) {
                        $newStyles[] = "$property:$newValue";
                    }
                } else {
                    $newStyles[] = "$property:$value";
                }
            }
        }

        $newStyle = implode(';', $newStyles);
        $path->setAttribute('style', $newStyle);
    }

    private function getDomElementStyle(DOMElement $path, $attribute)
    {
        $style = $path->getAttribute('style');
        $styles = explode(';', $style);

        foreach ($styles as $styleRule) {
            $styleParts = explode(':', $styleRule);
            if (count($styleParts) == 2) {
                $property = trim($styleParts[0]);
                $value = trim($styleParts[1]);
                if ($property === $attribute) {
                    return $value;
                }
            }
        }

        return null;
    }

    private function setSvgTitle(DOMDocument $dom, DOMElement $svg, $text)
    {
        $existingTitle = $svg->getElementsByTagName('title')->item(0);
        if ($existingTitle) {
            $existingTitle->nodeValue = $text;
        } else {
            $titleElement = $dom->createElement('title', $text);
            $svg->insertBefore($titleElement, $svg->firstChild);
        }
    }
}