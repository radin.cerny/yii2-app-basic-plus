<?php

namespace app\modules\location\models;

use app\models\BaseModel;
use app\modules\location\models\query\VLocationHierarchyQuery;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "v_location_hierarchy".
 *
 * @property int|null $id
 * @property int|null $id_parent
 * @property int|null $id_company
 * @property int|null $id_type
 * @property string|null $name_path
 * @property string|null $id_path
 * @property int|null $depth
 */
class VLocationHierarchy extends BaseModel
{

    /**
     * Is used in m240813_060049_v_location_hierarchy.php.
     * If the DELIMITER is changed, modify also the migration and/or the view in DB.
     */
    public const DELIMITER = ' > ';

    public static function primaryKey()
    {
        return ['id'];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'v_location_hierarchy';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\location\models\query\VLocationHierarchyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new VLocationHierarchyQuery(get_called_class());
    }

    /**
     *
     * @param $idCompany The owner of the location
     * @param null $locationType The Location type based on table LocationType
     * @param null $idParent Only return locations that have this parent somewhere in their hierarchy
     * @return mixed
     */
    public static function getListData($idCompany, $locationType = null, $idParent = null)
    {
        $delimiter = self::DELIMITER;

        $data = VLocationHierarchy::find()
            ->andWhere(['id_company' => $idCompany])
            ->andFilterWhere(['id_type' => $locationType])
            ->andFilterWhere(['LIKE', 'CONCAT("' . $delimiter . '",id_path,"' . $delimiter . '")', $idParent ? $delimiter . $idParent . $delimiter : null])
            ->orderBy(['name_path' => SORT_ASC])
            ->all();

        $parentNamePathLen = 0;
        if ($idParent) {
            $parentNamePath = VLocationHierarchy::find()->byId($idParent)->select(['name_path'])->scalar();
            $parentNamePathLen = strlen($parentNamePath);
            $parentNamePathLen += strlen($delimiter);
        }

        return ArrayHelper::map($data, 'id', function ($model) use ($parentNamePathLen) {
            return substr($model->name_path, $parentNamePathLen);
        });
    }

    public function getParentNamePath($asArray = false)
    {
        $exploded = explode(VLocationHierarchy::DELIMITER, $this->name_path);
        if (count($exploded) > 0) {
            unset($exploded[count($exploded) - 1]);
        }
        return $asArray ? $exploded : implode(VLocationHierarchy::DELIMITER, $exploded);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'id_company']);
    }

    public function getLocation()
    {
        return $this->hasOne(Location::class, ['id' => 'id']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_parent', 'id_company', 'id_type', 'depth'], 'integer'],
            [['name_path', 'id_path'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_parent' => Yii::t('app', 'Parent'),
            'id_company' => Yii::t('app', 'Company'),
            'id_type' => Yii::t('app', 'Type'),
            'name_path' => Yii::t('app', 'Name Path'),
            'id_path' => Yii::t('app', 'Id Path'),
            'depth' => Yii::t('app', 'Depth'),
        ];
    }
}
