<?php

namespace app\modules\location\models\search;

use app\modules\location\models\Location;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * LocationSearch represents the model behind the search form of `app\modules\location\models\Location`.
 */
class LocationSearch extends Location
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'id_company', 'id_address', 'id_type', 'x', 'xx', 'y', 'yy', 'z', 'zz', 'floor', 'max_load', 'active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'descr', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['gps_lat', 'gps_lng'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Location::find();
        $query->with(['company', 'address', 'children', 'parents', 'type']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_company' => $this->id_company,
            'id_address' => $this->id_address,
            'id_type' => $this->id_type,
            'x' => $this->x,
            'xx' => $this->xx,
            'y' => $this->y,
            'yy' => $this->yy,
            'z' => $this->z,
            'zz' => $this->zz,
            'floor' => $this->floor,
            'max_load' => $this->max_load,
            'gps_lat' => $this->gps_lat,
            'gps_lng' => $this->gps_lng,
            'active' => $this->active,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'descr', $this->descr]);

        return $dataProvider;
    }
}
