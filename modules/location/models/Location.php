<?php

namespace app\modules\location\models;

use app\models\BaseModel;
use Yii;

/**
 * This is the model class for table "loc_location".
 *
 * @property int $id
 * @property int|null $id_parent
 * @property int|null $id_company
 * @property int|null $id_address
 * @property int|null $id_type
 * @property string|null $name
 * @property string|null $descr
 * @property int|null $x X position [mm] (relative to the parent)
 * @property int|null $xx X dimension [mm]
 * @property int|null $y Y position  [mm](relative to the parent)
 * @property int|null $yy Y dimension [mm]
 * @property int|null $z Z position  [mm](relative to the parent)
 * @property int|null $zz Z dimension [mm]
 * @property int|null $floor Zero = ground floor
 * @property int $rotation
 * @property string $fill_color
 * @property string $stroke_color
 * @property int|null $max_load Max [kg/m2]
 * @property float|null $gps_lat GPS latitude for carriers, decimal number
 * @property float|null $gps_lng GPS longitude for carriers, decimal number
 * @property string $svg
 * @property int $zindex
 * @property int $children_offset_x
 * @property int $children_offset_y
 * @property int $active
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property Address $address
 * @property Company $company
 * @property User $createdBy
 * @property User $deletedBy
 * @property Location[] $children
 * @property Location $parent
 * @property VLocationHierarchy $hierarchy
 * @property User $updatedBy
 * @property LocationType $type
 */
class Location extends BaseModel
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'loc_location';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\location\models\query\LocationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\location\models\query\LocationQuery(get_called_class());
    }

    /**
     * Can be used to insert locations in bulk.
     *
     * $data = [
     *     [
     *         'name' => 'Branch 1',
     *         'id_type' => 1,
     *         'id_address' => 1,
     *         'children' => [
     *             [
     *                 'name' => 'Gate 1',
     *                 'id_type' => 2,
     *                 'id_address' => 1,
     *                 'gps_lat' => 50.223285,
     *                 'gps_lng' => 12.883131,
     *                 'children' => [
     *                     [
     *                        // etc ... recursively
     *                     ],
     *                 ],
     *             ],
     *         ],
     *     ],
     * ];
     *
     * Location::import($data, 1);
     *
     * @param $data
     * @param $idCompany
     * @param null $idParent
     * @throws \yii\db\Exception
     */
    public static function import($data, $idCompany, $idParent = null)
    {
        $table = self::tableName();

        foreach ($data as $columns) {
            $columns['id_company'] = $idCompany;
            $columns['id_parent'] = $idParent;
            $children = $columns['children'] ?? [];
            unset($columns['children']);
            Yii::$app->db->createCommand()->insert($table, $columns)->execute();
            self::import($children, $idCompany, Yii::$app->db->getLastInsertID());
        }
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_company', 'id_address', 'id_type', 'x', 'xx', 'y', 'yy', 'z', 'zz', 'floor', 'max_load', 'active', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['gps_lat', 'gps_lng'], 'number'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'descr'], 'string', 'max' => 255],
            [['id_address'], 'exist', 'skipOnError' => true, 'targetClass' => Address::class, 'targetAttribute' => ['id_address' => 'id']],
            [['id_company'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['id_company' => 'id']],
            [['id_type'], 'exist', 'skipOnError' => true, 'targetClass' => LocationType::class, 'targetAttribute' => ['id_type' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['created_by' => 'id']],
            [['deleted_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['deleted_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_parent' => Yii::t('app', 'Parent location'),
            'id_company' => Yii::t('app', 'Company'),
            'id_address' => Yii::t('app', 'Address'),
            'id_type' => Yii::t('app', 'Type'),
            'name' => Yii::t('app', 'Name'),
            'descr' => Yii::t('app', 'Descr'),
            'x' => Yii::t('app', 'Left [mm]'),
            'xx' => Yii::t('app', 'Left-right dimension [mm]'),
            'y' => Yii::t('app', 'Top [mm]'),
            'yy' => Yii::t('app', 'Top-bottom dimension [mm]'),
            'z' => Yii::t('app', 'Elevation [mm]'),
            'zz' => Yii::t('app', 'Height [mm]'),
            'floor' => Yii::t('app', 'Floor'),
            'rotation' => Yii::t('app', 'Rotation'),
            'fill_color' => Yii::t('app', 'Fill color'),
            'stroke_color' => Yii::t('app', 'Stroke color'),
            'max_load' => Yii::t('app', 'Max Load'),
            'gps_lat' => Yii::t('app', 'Gps Lat'),
            'gps_lng' => Yii::t('app', 'Gps Lng'),
            'svg' => Yii::t('app', 'SVG'),
            'zidnex' => Yii::t('app', 'z-index'),
            'children_offset_x' => Yii::t('app', 'X offset for all child locations'),
            'children_offset_y' => Yii::t('app', 'Y offset for all child locations'),
            'active' => Yii::t('app', 'Active'),
            'created_by' => Yii::t('app', 'Created By'),
            'updated_by' => Yii::t('app', 'Updated By'),
            'deleted_by' => Yii::t('app', 'Deleted By'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'deleted_at' => Yii::t('app', 'Deleted At'),
        ];
    }

    /**
     * Gets query for [[Address]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\AddressQuery
     */
    public function getAddress()
    {
        return $this->hasOne(Address::class, ['id' => 'id_address']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\CompanyQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'id_company']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\UserQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * Gets query for [[DeletedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\UserQuery
     */
    public function getDeletedBy()
    {
        return $this->hasOne(User::class, ['id' => 'deleted_by']);
    }

    /**
     * Gets query for [[Children]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\LocationQuery
     */
    public function getChildren()
    {
        return $this->hasMany(Location::class, ['id_parent' => 'id']);
    }

    /**
     * Gets query for [[Parent]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\LocationQuery
     */
    public function getParent()
    {
        return $this->hasOne(Location::class, ['id' => 'id_parent']);
    }

    public function getHierarchy()
    {
        return $this->hasOne(VLocationHierarchy::class, ['id' => 'id']);
    }
    /**
     * Gets query for [[UpdatedBy]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\location\models\query\UserQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    public function getType()
    {
        return $this->hasOne(LocationType::class, ['id' => 'id_type']);
    }

    public function getGpsCoordinates(string $separator = ', '): ?string
    {
        if ($this->gps_lat && $this->gps_lng) {
            return $this->gps_lat . $separator . $this->gps_lng;
        }
        return null;
    }

    public function getAvailableFloorNumbers()
    {
        $result = [];
        /** @var Location $child */
        foreach ($this->children as $child) {
            $result[$child->floor] = 1;
        }
        return array_keys($result);
    }

    public function isType($type)
    {
        return $this->id_type == $type;
    }
}
