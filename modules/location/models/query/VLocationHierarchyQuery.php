<?php

namespace app\modules\location\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\VLocationHierarchy]].
 *
 * @see \app\models\VLocationHierarchy
 */
class VLocationHierarchyQuery extends \yii\db\ActiveQuery
{
    public function byId($idLocation)
    {
        return $this->andWhere(['id' => $idLocation]);
    }

    public function byType($idType)
    {
        return $this->andWhere(['id_type' => $idType]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\VLocationHierarchy[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\VLocationHierarchy|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
