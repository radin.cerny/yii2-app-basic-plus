<?php

namespace app\modules\location\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\location\models\LocationType]].
 *
 * @see \app\modules\location\models\LocationType
 */
class LocationTypeQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\location\models\LocationType[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\location\models\LocationType|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
