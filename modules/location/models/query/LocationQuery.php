<?php

namespace app\modules\location\models\query;

/**
 * This is the ActiveQuery class for [[\app\modules\location\models\Location]].
 *
 * @see \app\modules\location\models\Location
 */
class LocationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\modules\location\models\Location[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\location\models\Location|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
