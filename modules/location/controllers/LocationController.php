<?php

namespace app\modules\location\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\location\helpers\LocationRenderer;
use app\modules\location\models\Location;
use app\modules\location\models\search\LocationSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class LocationController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actionMapDetail($id)
    {
        $model = $this->findModel($id);

        if (Yii::$app->request->isAjax) {
            return $this->renderAjax('map-detail', [
                'model' => $model,
            ]);
        }

        return $this->render('map-detail', [
            'model' => $model,
        ]);
    }

    public function actionMap($id)
    {
        $model = $this->findModel($id);
        $parentLocations_listData = ArrayHelper::map($model->parents, 'id', 'name');

        return $this->render('map', [
            'model' => $model,
            'parentLocations_listData' => $parentLocations_listData,
            // Which floor should be displayed?
            'floorItems' => ArrayHelper::map($model->getAvailableFloorNumbers(), function ($floor, $default) {
                return $floor;
            }, function ($floor, $default) {
                return Yii::t('app', 'Display floor nr. {floor}', ['floor' => $floor]);
            }),
            // How many levels should be displayed?
            // 1 = only $model
            // 2 = $model + 1 level of children ...
            'displayLevelsItems' => [
                1 => Yii::t('app', 'Display {levels} level(s)', ['levels' => 1]),
                2 => Yii::t('app', 'Display {levels} level(s)', ['levels' => 2]),
                3 => Yii::t('app', 'Display {levels} level(s)', ['levels' => 3]),
                4 => Yii::t('app', 'Display {levels} level(s)', ['levels' => 4]),
                5 => Yii::t('app', 'Display {levels} level(s)', ['levels' => 5]),
            ],
            // Which levels should contain labels?
            'labelLevelsItems' => [
                1 => Yii::t('app', 'Label level {levels}', ['levels' => 1]),
                2 => Yii::t('app', 'Label level {levels}', ['levels' => 2]),
                3 => Yii::t('app', 'Label level {levels}', ['levels' => 3]),
                4 => Yii::t('app', 'Label level {levels}', ['levels' => 4]),
                5 => Yii::t('app', 'Label level {levels}', ['levels' => 5]),
            ],
            'locationRenderer' => Yii::createObject([
                'class' => LocationRenderer::class,
                'displayLevels' => Yii::$app->request->post('displayLevels', 4),
                'labelLevels' => Yii::$app->request->post('labelLevels', 4),
                'floor' => Yii::$app->request->post('floor', 0),
            ]),
        ]);
    }

    /**
     * Lists all Location models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new LocationSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Location model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Location model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Location();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Location model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Location model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Location model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return Location the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Location::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
