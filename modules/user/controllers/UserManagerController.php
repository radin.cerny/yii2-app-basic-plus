<?php

namespace app\modules\user\controllers;

use app\modules\admin\controllers\BaseController;
use app\modules\user\models\AuthItem;
use app\modules\user\models\LoginForm;
use app\modules\user\models\PasswordResetRequestForm;
use app\modules\user\models\ResetPasswordForm;
use app\modules\user\models\search\UserSearch;
use app\modules\user\models\User;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserManagerController extends BaseController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $parentBehaviors = parent::behaviors();

        if (env('RBAC_ENABLED')) {
            $parentBehaviors['access']['rules'][] = [
                'allow' => true,
                'roles' => ['?'],
                'actions' => ['login', 'request-password-reset', 'reset-password'],
            ];

            $parentBehaviors['access']['rules'][] = [
                'allow' => true,
                'roles' => ['@'],
                'actions' => ['logout'],
            ];
        }

        return array_merge(
            $parentBehaviors,
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    public function actions()
    {
        return [
            'sort' => [
                'class' => 'app\widgets\actions\GridViewSortAction',
                'sortColName' => 'position',
                'modelClass' => User::class,
            ],
        ];
    }

    /**
     * Lists all User models.
     *
     * @return string
     */
    public function actionIndex($exportTo = null)
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        if ($exportTo === 'csv') {
            $dataProvider->pagination->pageSize = -1;
            Yii::$app->csvExporter->toResponse($dataProvider->models, 'Users_' . date('Y-m-d_H-i-s') . '.csv');
            return;
        } else if ($exportTo === 'xlsx') {
            $dataProvider->pagination->pageSize = -1;
            $fileName = 'Users_' . date('Y-m-d_H-i-s') . '.xlsx';

//            return Yii::$app->xlsxExporter->createXlsx($dataProvider->models, $fileName);

            $modelIds = ArrayHelper::getColumn($dataProvider->models, 'id');
            Yii::$app->xlsxExporter->createXlsxQueue($modelIds, $fileName, Yii::$app->user->identity->getId());
            return $this->renderContent('<script>window.close();</script>');
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionPrint($id)
    {
        $model = $this->findModel($id);
        $pdf = Yii::$app->pdfExporter->createPdf();
        $pdf->writeHtml($model->getPdfHtml());
        $pdf->Output('name.pdf', 'I');
    }

    /**
     * Displays a single User model.
     * @param int $id ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new User();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Updates User roles.
     * @param int $id
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionRoles($id)
    {
        $model = $this->findModel($id);

        if ($this->request->isPost) {
            $model->setRoles($this->request->post('User')['listOfAuthRoles'] ?? []);
            return $this->redirect(['index']);
        }

        return $this->render('roles', [
            'model' => $model,
            'availableRoles' => AuthItem::getRolesListData(),
        ]);
    }

    /**
     * Verif email
     * @param string $token Verification token
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionVerifyEmail($token)
    {
        $model = User::find()->byEmailVerificationToken($token)->one();
        if (!$model) {
            throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
        }

        $model->setEmailVerified();
        Yii::$app->session->setFlash('success', 'Your email ' . $model->email . ' has been verified.');
        return $this->redirect(['/']);
    }

    /**
     * Send the verification email
     * @param int $id ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSendVerificationEmail($id)
    {
        $model = $this->findModel($id);
        $model->sendVerificationEmail();
        Yii::$app->session->setFlash('success', 'Email sent to : ' . $model->email);
        return $this->redirect(['index']);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            }

            Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id ID
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne(['id' => $id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
