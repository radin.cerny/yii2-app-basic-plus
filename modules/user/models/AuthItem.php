<?php

namespace app\modules\user\models;

use app\models\BaseModel;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "auth_item".
 *
 * @property string $name
 * @property int $type TYPE_ROLE = 1, TYPE_PERMISSION = 2
 * @property string|null $description
 * @property string|null $rule_name
 * @property resource|null $data
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $assignable Only "assignable" roles can be directly assigned to users in the UI
 *
 * @property AuthAssignment[] $authAssignments
 * @property AuthItemChild[] $authItemChildrenWhereIAmParent
 * @property AuthItemChild[] $authItemChildrenWhereIAmChild
 * @property AuthItem[] $children
 * @property AuthItem[] $parents
 * @property AuthRule $ruleName
 * @property User[] $users
 */
class AuthItem extends BaseModel
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth_item';
    }

    /**
     * {@inheritdoc}
     * @return \app\modules\user\models\query\AuthItemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\modules\user\models\query\AuthItemQuery(get_called_class());
    }

    /**
     * @param bool $addDescription
     * @param \yii\web\User|null $webUser Yii::$app->user
     * @return array
     */
    public static function getRolesListData(bool $addDescription = false, bool $onlyAssignable = true, \yii\web\User $webUser = null)
    {
        $allRoles = AuthItem::find()->onlyRoles()->assignable($onlyAssignable)->asArray()->orderBy(['name' => SORT_ASC])->all();
        $selectedRoles = $allRoles;

        if ($webUser) {
            $selectedRoles = [];
            foreach ($allRoles as $role) {
                if ($webUser->can($role['name'])) {
                    $selectedRoles[] = $role;
                }
            }
        }

        return ArrayHelper::map(
            $selectedRoles,
            'name',
            function ($array, $default) use ($addDescription) {
                return $array['name'] . ($addDescription ? (' (' . $array['description'] . ')') : '');
            }
        );
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        // Attribute "assignable" should not be present here, because RoleController expects this attribute to be ignored by Yii's "massive assignment"
        // PS: The two expressions are similar: "assignable" and "massive assignment". But it is just a coincidence. They have nothing in common.
        return [
            [['name', 'type'], 'required'],
            [['type', 'created_at', 'updated_at'], 'integer'],
            [['description', 'data'], 'string'],
            [['name', 'rule_name'], 'string', 'max' => 64],
            [['name'], 'unique'],
            [['rule_name'], 'exist', 'skipOnError' => true, 'targetClass' => AuthRule::class, 'targetAttribute' => ['rule_name' => 'name']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
            'rule_name' => Yii::t('app', 'Rule Name'),
            'data' => Yii::t('app', 'Data'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
            'assignable' => Yii::t('app', 'Assignable'),
        ];
    }

    /**
     * Gets query for [[AuthAssignments]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\AuthAssignmentQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::class, ['item_name' => 'name']);
    }

    /**
     * Gets query for [[AuthItemChildrenWhereIAmParent]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\AuthItemChildQuery
     */
    public function getAuthItemChildrenWhereIAmParent()
    {
        return $this->hasMany(AuthItemChild::class, ['parent' => 'name']);
    }

    /**
     * Gets query for [[AuthItemChildrenWhereIAmChild]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\AuthItemChildQuery
     */
    public function getAuthItemChildrenWhereIAmChild()
    {
        return $this->hasMany(AuthItemChild::class, ['child' => 'name']);
    }

    /**
     * Gets query for [[Children]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\AuthItemQuery
     */
    public function getChildren()
    {
        return $this->hasMany(AuthItem::class, ['name' => 'child'])->viaTable('auth_item_child', ['parent' => 'name']);
    }

    /**
     * Gets query for [[Parents]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\AuthItemQuery
     */
    public function getParents()
    {
        return $this->hasMany(AuthItem::class, ['name' => 'parent'])->viaTable('auth_item_child', ['child' => 'name']);
    }

    /**
     * Gets query for [[RuleName]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\AuthRuleQuery
     */
    public function getRuleName()
    {
        return $this->hasOne(AuthRule::class, ['name' => 'rule_name']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery|\app\modules\user\models\query\UserQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['id' => 'user_id'])->viaTable('auth_assignment', ['item_name' => 'name']);
    }

    public function getListOfChildRoles()
    {
        return $this->getChildRoles(true);
    }

    public function getChildRoles($onlyNames = false): array
    {
        $roles = $this->getChildren()->onlyRoles()->all();

        if (!$onlyNames) {
            return $roles;
        }

        $result = [];
        foreach ($roles as $role) {
            $result[] = $role->name;
        }

        return $result;
    }


    /**
     * Sets new set of child roles to this role.
     * @param array $newRoles
     * @return false|void
     * @throws \Exception
     */
    public function setRoles(array $newRoles)
    {
        // First I detect which roles were added or removed. (Those untouched are ignored)
        $existingRoles = $this->getListOfChildRoles();
        $toBeRevoked = array_diff($existingRoles, $newRoles);
        $toBeCreated = array_diff($newRoles, $existingRoles);

        $authManager = Yii::$app->authManager;
        $parent = $authManager->getRole($this->name);

        foreach ($toBeRevoked as $roleName) {
            $role = $authManager->getRole($roleName);
            $authManager->removeChild($parent, $role);
        }

        foreach ($toBeCreated as $roleName) {
            $role = $authManager->getRole($roleName);
            $authManager->addChild($parent, $role);
        }
    }
}
