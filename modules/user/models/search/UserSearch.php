<?php

namespace app\modules\user\models\search;

use app\modules\user\models\User as UserModel;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * User represents the model behind the search form of `app\modules\user\models\User`.
 */
class UserSearch extends UserModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'created_by', 'updated_by', 'deleted_by'], 'integer'],
            [['name', 'surname', 'username', 'email', 'phone', 'auth_key', 'password_hash', 'password_reset_token', 'email_verification_token', 'email_verified_at', 'api_access_token', 'api_password_hash', 'created_at', 'updated_at', 'deleted_at'], 'safe'],
            $this->gridPageSizeRule,
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = UserModel::find();
        $this->load($params);

        // Page size:
        // load() must be called beforehand
        if (!isset($this->gridPageSize)) {
            $this->gridPageSize = $this->gridPageSizeDefault;
        }

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => ['pageSize' => (int)$this->gridPageSize],
        ]);


        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'email_verified_at' => $this->email_verified_at,
            'status' => $this->status,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
            'deleted_by' => $this->deleted_by,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'deleted_at' => $this->deleted_at,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'surname', $this->surname])
            ->andFilterWhere(['like', 'username', $this->username])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'auth_key', $this->auth_key])
            ->andFilterWhere(['like', 'password_hash', $this->password_hash])
            ->andFilterWhere(['like', 'password_reset_token', $this->password_reset_token])
            ->andFilterWhere(['like', 'email_verification_token', $this->email_verification_token])
            ->andFilterWhere(['like', 'api_access_token', $this->api_access_token])
            ->andFilterWhere(['like', 'api_password_hash', $this->api_password_hash]);

        return $dataProvider;
    }
}
