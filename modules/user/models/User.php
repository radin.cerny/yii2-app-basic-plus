<?php

namespace app\modules\user\models;

use app\models\BaseModel;
use app\modules\message\components\ChatSender;
use app\modules\message\components\NotificationSender;
use app\modules\message\models\MessageRecipient;
use app\modules\message\models\query\MessageQuery;
use app\modules\message\models\query\MessageRecipientQuery;
use app\modules\user\models\query\AuthAssignmentQuery;
use app\modules\user\models\query\AuthItemQuery;
use app\modules\user\models\query\UserQuery;
use DateTime;
use Yii;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string|null $username
 * @property string $email
 * @property string|null $phone
 * @property string $auth_key Used to validate session and auto-login (remember me)
 * @property string|null $password_hash
 * @property string|null $password_reset_token
 * @property string|null $email_verification_token
 * @property string|null $email_verified_at
 * @property int $status
 * @property string|null $api_access_token
 * @property string|null $api_access_token_expire
 * @property string|null $api_password_hash
 * @property int|null $created_by
 * @property int|null $updated_by
 * @property int|null $deleted_by
 * @property string $created_at
 * @property string|null $updated_at
 * @property string|null $deleted_at
 *
 * @property AuthAssignment[] $authAssignments
 * @property User $createdBy
 * @property User $deletedBy
 * @property AuthItem[] $itemNames
 * @property User $updatedBy
 * @property User[] $users
 * @property User[] $users0
 * @property User[] $users1
 */
class User extends BaseModel implements \yii\web\IdentityInterface
{
    public const STATUS_INACTIVE = 50;
    public const STATUS_ACTIVE = 100;

    public $password_new;
    public $password_new_repeat;
    public $api_password_new;
    public $api_password_new_repeat;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     * @return UserQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UserQuery(get_called_class());
    }

    /**
     * IdentityInterface
     * @param int|string $id
     * @return query\User|array|\yii\web\IdentityInterface|null
     */
    public static function findIdentity($id)
    {
        return User::find()->byId($id)->active()->one();
    }

    /**
     * IdentityInterface
     * @param mixed $token
     * @param null $type
     * @return query\User|array|\yii\web\IdentityInterface|null
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return User::find()->byAccessToken($token)->active()->one();
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->auth_key = Yii::$app->security->generateRandomString();
            }
            return true;
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'email'], 'required'],

            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE]],

            [['name', 'surname', 'username', 'phone'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email'], 'unique'],

            // Passwords:
            ['password_new', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['password_new_repeat', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['api_password_new', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['api_password_new_repeat', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
            ['password_new_repeat', 'compare', 'operator' => '==', 'compareAttribute' => 'password_new'],
            ['api_password_new_repeat', 'compare', 'operator' => '==', 'compareAttribute' => 'api_password_new'],
            [['password_new'], 'setPasswordWhenNewIsSet', 'params' => ['saveToAttribute' => 'password_hash']],
            [['api_password_new'], 'setPasswordWhenNewIsSet', 'params' => ['saveToAttribute' => 'api_password_hash']],
            [['api_access_token'], 'unique'],
        ];
    }

    public function setPasswordWhenNewIsSet($attribute, $params, $validator)
    {
        if (trim($this->$attribute) === '') {
            return true;
        }

        $saveToAttribute = $params['saveToAttribute'];
        $this->$saveToAttribute = Yii::$app->security->generatePasswordHash(trim($this->$attribute));
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash(trim($password));
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'surname' => Yii::t('app', 'Surname'),
            'username' => Yii::t('app', 'Username'),
            'email' => Yii::t('app', 'Email'),
            'phone' => Yii::t('app', 'Phone'),

            'password_new' => Yii::t('app', 'New password'),
            'password_new_repeat' => Yii::t('app', 'New password repeat'),
            'api_password_new' => Yii::t('app', 'New API password'),
            'api_password_new_repeat' => Yii::t('app', 'New API password repeat'),

            'auth_key' => Yii::t('app', 'Auth key'),
            'password_hash' => Yii::t('app', 'Password hash'),
            'password_reset_token' => Yii::t('app', 'Password reset token'),
            'email_verification_token' => Yii::t('app', 'Verification token'),
            'email_verified_at' => Yii::t('app', 'Email verified at'),
            'status' => Yii::t('app', 'Status'),
            'api_access_token' => Yii::t('app', 'Api access token'),
            'api_password_hash' => Yii::t('app', 'Api password hash'),
            'created_by' => Yii::t('app', 'Created by'),
            'updated_by' => Yii::t('app', 'Updated by'),
            'deleted_by' => Yii::t('app', 'Deleted by'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Updated at'),
            'deleted_at' => Yii::t('app', 'Deleted at'),
        ];
    }

    /**
     * Gets query for [[AuthAssignments]].
     *
     * @return \yii\db\ActiveQuery|AuthAssignmentQuery
     */
    public function getAuthAssignments()
    {
        return $this->hasMany(AuthAssignment::class, ['user_id' => 'id']);
    }

    /**
     * Gets query for [[ItemNames]].
     *
     * @return \yii\db\ActiveQuery|AuthItemQuery
     */
    public function getAuthItems()
    {
        return $this->hasMany(AuthItem::class, ['name' => 'item_name'])
            ->via('authAssignments');
    }

    public function getChatMessages()
    {
        $user = User::find()
            ->where(['id' => $this->id])
            ->with(['messageRecipients' => function (MessageRecipientQuery $messageRecipientQuery) {
                $messageRecipientQuery->joinWith(['message' => function (MessageQuery $messageQuery) {
                    $messageQuery->andWhere(['channel_class' => ChatSender::class]);
                }]);
            }])
            ->one();
        return $user->messageRecipients;
    }

    public function getNotifications()
    {
        $user = User::find()
            ->where(['id' => $this->id])
            ->with(['messageRecipients' => function (MessageRecipientQuery $messageRecipientQuery) {
                $messageRecipientQuery->joinWith(['message' => function (MessageQuery $messageQuery) {
                    $messageQuery->andWhere(['channel_class' => NotificationSender::class]);
                }]);
            }])
            ->one();
        return $user->messageRecipients;
    }

    public function getMessageRecipients()
    {
        return $this->hasMany(MessageRecipient::class, ['id_user' => 'id']);
    }

    public function getListOfAuthRoles()
    {
        return $this->getAuthRoles(true);
    }

    public function getAuthRoles($onlyNames = false): array
    {
        $roles = $this->getAuthAssignments()->with([
            'item' => function (AuthItemQuery $query) {
                $query->onlyRoles();
            }
        ])->all();

        if (!$onlyNames) {
            return $roles;
        }

        $result = [];
        foreach ($roles as $role) {
            $result[] = $role->item->name;
        }

        return $result;
    }


    /**
     * Sets new set of roles to this user.
     * If you only want to add new roles, you have to name here also existing ones.
     * @param array $newRoles
     * @return false|void
     * @throws \Exception
     */
    public function setRoles(array $newRoles)
    {
        if ($this->isCurrentUser()) {
            // User cannot change his own roles
            return false;
        }

        // Users can only manipulate roles they have.
        foreach ($newRoles as $key => $role) {
            if (!Yii::$app->user->can($role)) {
                unset($newRoles[$key]);
            }
        }

        if (empty($newRoles)) {
            return false;
        }

        // First I detect which roles were added, removed. (Those untouched are ignored)
        $existingRoles = $this->getListOfAuthRoles();
        $toBeRevoked = array_diff($existingRoles, $newRoles);
        $toBeCreated = array_diff($newRoles, $existingRoles);

        $authManager = Yii::$app->authManager;

        foreach ($toBeRevoked as $roleName) {
            $role = $authManager->getRole($roleName);
            $authManager->revoke($role, $this->id);
        }

        foreach ($toBeCreated as $roleName) {
            $role = $authManager->getRole($roleName);
            $authManager->assign($role, $this->id);
        }
    }

    /**
     * Tests if current instance of the User class is now logged in
     * @return bool
     */
    public function isCurrentUser(): bool
    {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        return $this->id == Yii::$app->user->identity->id;
    }

    public function sendVerificationEmail()
    {
        $this->email_verification_token = Yii::$app->security->generateRandomString();
        $this->email_verified_at = null;
        $this->save(false, ['email_verified_at', 'email_verification_token']);

//        $url = Url::to(['verify-email', 'token' => $this->email_verification_token], true);
//        $link = Html::a('Verify email', $url);
//        $body = 'Hello. Click on the link to verify your email: ' . $link;

        return Yii::$app->mailer->compose(
            ['html' => '/../mail/emailVerify-html', 'text' => '/../mail/emailVerify-text'],
            ['user' => $this]
        )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Email verification')
            ->send();

//        Yii::$app->mailer->compose()
//            ->setFrom(Yii::$app->params['adminEmail'])
//            ->setTo($this->email)
//            ->setSubject('Email verification')
//            ->setTextBody($body)
//            ->setHtmlBody($body)
//            ->send();
    }

    public function setEmailVerified()
    {
        $this->email_verification_token = null;
        $this->email_verified_at = date('Y-m-d H:i:s');
        $this->save(false, ['email_verified_at', 'email_verification_token']);
    }

    /**
     * IdentityInterface
     * @return int|string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * IdentityInterface
     * @return string|null
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * IdentityInterface
     * @param string $authKey
     * @return bool
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }

    /**
     * IdentityInterface
     * @param $password
     * @return bool
     */
    public function validateBackendPassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    public function validateApiPassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->api_password_hash);
    }

    public function generateApiAccessToken(int $timeout, bool $save = true): bool
    {
        $expiration = (new DateTime("+ $timeout seconds"))->format('Y-m-d H:i:s');
        $this->api_access_token_expire = $expiration;

        do {
            // Why appending the time?
            // Because method generateRandomString() can technically generate identical string in the future.
            $this->api_access_token = Yii::$app->security->generateRandomString(32) . '_' . time();
        } while (!$this->validate(['api_access_token']));

        if ($save) {
            return $this->save(true, ['api_access_token', 'api_access_token_expire']);
        }

        return $this->validate(['api_access_token', 'api_access_token_expire']);
    }

    public function removeApiAccessToken($save = true)
    {
        $this->api_access_token = null;
        $this->api_access_token_expire = null;
        if ($save) {
            $this->save(false, ['api_access_token', 'api_access_token_expire']);
        }
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    public function getXlsxHeaderArray()
    {
        return [
            'id',
            'name',
            'surname',
            'username',
            'email',
            'phone',
            'email_verified_at',
            'status',
        ];
    }

    public function getXlsxRowArray()
    {
        return [
            10 + $this->id,
            $this->name,
            $this->surname,
            $this->username,
            $this->email,
            $this->phone,
            $this->email_verified_at,
            $this->status,
        ];
    }

    public function getPdfHtml()
    {
        $attributes = ['name', 'surname', 'email'];

        $html = <<<HTML
<table cellspacing="0" cellpadding="1" border="0.1">
    <tr>
        <td style="background-color: black; color:white; text-align: center;" width="45">Demo</td>
        <td style="background-color: black; color:white; text-align: center;" width="200"></td>
    </tr>
HTML;

        foreach ($attributes as $i => $attribute) {
            $back = 'background-color: ' . ($i % 2 ? 'rgb(230,230,230);' : 'white') . ';';
            $html .= '<tr>';
            $html .= '<td>';
            $html .= $this->getAttributeLabel($attribute);
            $html .= '</td>';
            $html .= '<td>';
            $html .= $this->$attribute;
            $html .= '</td>';
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }
}
