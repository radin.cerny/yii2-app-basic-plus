<?php

namespace app\modules\user\models\query;

use app\modules\user\models\User;

/**
 * This is the ActiveQuery class for [[User]].
 *
 * @see UserSearch
 */
class UserQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return User[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return User|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }

    public function active()
    {
        return $this->byStatus(User::STATUS_ACTIVE);
    }

    public function byStatus($status)
    {
        return $this->andWhere(['status' => $status]);
    }

    public function byEmailVerificationToken($token)
    {
        return $this->andWhere(['email_verification_token' => $token]);
    }

    public function byUsername($username)
    {
        return $this->andWhere(['username' => $username]);
    }

    public function byEmail($email)
    {
        return $this->andWhere(['email' => $email]);
    }

    public function byAccessToken($token)
    {
        return $this->andWhere(['api_access_token' => $token]);
    }

    public function byPasswordResetToken($token)
    {
        return $this->andWhere(['password_reset_token' => $token]);
    }

    public function byId($id)
    {
        return $this->andWhere(['id' => $id]);
    }
}
