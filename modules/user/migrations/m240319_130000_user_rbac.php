<?php

use app\modules\user\controllers\UserManagerController;
use app\modules\user\helpers\RbacHelper;
use yii\db\Migration;

/**
 * Class m240319_130000_user_rbac
 */
class m240319_130000_user_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        RbacHelper::createRbac(Yii::$app->authManager, [
            UserManagerController::class => [
                'hierarchy' => [
                    'creator' => ['actionCreate'],
                    'reader' => ['actionIndex', 'actionView'],
                    'updater' => ['actionUpdate', 'actionVerifyEmail', 'actionSendVerificationEmail'],
                    'deleter' => ['actionDelete'],
                    'admin' => ['creator', 'reader', 'updater', 'deleter'],
                ],
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";
        return false;
    }
}
