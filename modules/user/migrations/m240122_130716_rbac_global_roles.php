<?php

use app\modules\user\helpers\RbacHelper;
use app\modules\user\models\AuthItem;
use yii\db\Migration;

/**
 * Class m240122_130716_rbac_global_roles
 */
class m240122_130716_rbac_global_roles extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $authManager = Yii::$app->authManager;

        $admin = $authManager->createRole(RbacHelper::ROLE_GLOBAL_ADMIN);
        $creator = $authManager->createRole(RbacHelper::ROLE_GLOBAL_CREATOR);
        $reader = $authManager->createRole(RbacHelper::ROLE_GLOBAL_READER);
        $updater = $authManager->createRole(RbacHelper::ROLE_GLOBAL_UPDATER);
        $deleter = $authManager->createRole(RbacHelper::ROLE_GLOBAL_DELETER);

        $authManager->add($admin);
        $authManager->add($creator);
        $authManager->add($reader);
        $authManager->add($updater);
        $authManager->add($deleter);

        $authManager->addChild($admin, $creator);
        $authManager->addChild($admin, $reader);
        $authManager->addChild($admin, $updater);
        $authManager->addChild($admin, $deleter);

        AuthItem::updateAll([
            'assignable' => 1,
        ], [
            'name' => [
                RbacHelper::ROLE_GLOBAL_ADMIN,
                RbacHelper::ROLE_GLOBAL_CREATOR,
                RbacHelper::ROLE_GLOBAL_READER,
                RbacHelper::ROLE_GLOBAL_UPDATER,
                RbacHelper::ROLE_GLOBAL_DELETER,
            ],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo get_class() . " cannot be reverted.\n";
        return false;
    }
}
