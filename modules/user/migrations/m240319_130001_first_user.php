<?php

use app\migrations\BaseMigration;
use app\modules\user\helpers\RbacHelper;

/**
 * Class m240319_130001_first_user
 */
class m240319_130001_first_user extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%user}}';
        $this->insert($tableName, [
            'name' => 'Hello',
            'surname' => 'World',
            'username' => 'admin',
            'email' => 'hello@gmail.cz',
            'phone' => '+12345678',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'api_password_hash' => Yii::$app->security->generatePasswordHash('admin'),
        ]);

        $this->insert($tableName, [
            'name' => 'Chuck',
            'surname' => 'Norris',
            'username' => 'reader',
            'email' => 'gmail@chuck.norris',
            'phone' => '+12345678',
            'auth_key' => Yii::$app->security->generateRandomString(),
            'password_hash' => Yii::$app->security->generatePasswordHash('reader'),
            'api_password_hash' => Yii::$app->security->generatePasswordHash('reader'),
        ]);

        $authManager = Yii::$app->authManager;
        $admin = $authManager->getRole(RbacHelper::ROLE_GLOBAL_ADMIN);
        $reader = $authManager->getRole(RbacHelper::ROLE_GLOBAL_READER);
        $authManager->assign($admin, 1);
        $authManager->assign($reader, 2);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = '{{%user}}';
        $this->delete($tableName, ['username' => 'hello']);
    }
}
