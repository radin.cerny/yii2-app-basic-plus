<?php

use app\migrations\BaseMigration;

/**
 * Class m240122_130700_user
 */
class m240122_130700_user extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableName = '{{%user}}';
        $tableNameTrim = trim($tableName, '{%}');

        $this->createTable($tableName, [
            'name' => $this->string()->notNull(),
            'surname' => $this->string()->notNull(),
            'username' => $this->string(),
            'email' => $this->string()->notNull()->unique(),
            'phone' => $this->string(),

            // Will be done via RBAC
            // 'admin_enabled' => $this->boolean()->defaultValue(0)->notNull()->comment('Can user access the administration?'),

            // What is auth_key good for?
            // @link https://stackoverflow.com/questions/26167265/yii2-why-is-the-auth-key-in-class-user
            // @link https://www.yiiframework.com/doc/guide/2.0/en/security-authentication
            'auth_key' => $this->string()->notNull()->unique()->comment('Used to validate session and auto-login (remember me)'),

            'password_hash' => $this->string(),
            'password_reset_token' => $this->string()->unique(),
            'email_verification_token' => $this->string()->unique()->defaultValue(null),
            'email_verified_at' => $this->dateTime(),
            'status' => $this->smallInteger()->notNull()->defaultValue(100),

            // Will be done via RBAC
            // 'api_enabled' => $this->boolean()->defaultValue(0)->notNull()->comment('Can user access the REST API (PWA application)?'),

            'api_access_token' => $this->string()->unique()->comment('For API purposes'),
            'api_access_token_expire' => $this->dateTime(),
            'api_password_hash' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $tableName = '{{%user}}';
        $this->dropTable($tableName);
    }
}
