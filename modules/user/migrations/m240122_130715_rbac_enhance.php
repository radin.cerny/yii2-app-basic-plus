<?php

use yii\db\Migration;

/**
 * Class m240122_130715_rbac_enhance
 */
class m240122_130715_rbac_enhance extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%auth_assignment}}', 'user_id', $this->integer());
        $this->addColumn('{{%auth_item}}', 'assignable', $this->boolean()->defaultValue(0)->comment('Only "assignable" roles can be directly assigned to users in the UI'));
        $this->addForeignKey('fk-auth_assignment-user', '{{%auth_assignment}}', ['user_id'], '{{%user}}', ['id'], 'CASCADE', 'CASCADE',);
        $this->addCommentOnColumn('{{%auth_item}}', 'type', 'TYPE_ROLE = 1, TYPE_PERMISSION = 2');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('fk-auth_assignment-user', '{{%auth_assignment}}');
        $this->alterColumn('{{%auth_assignment}}', 'user_id', $this->string(64));
        $this->dropCommentFromColumn('{{%auth_item}}', 'type');
    }
}
