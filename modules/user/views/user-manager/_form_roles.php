<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var \app\modules\user\models\User $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $availableRoles */

yii\widgets\Pjax::begin([
    'id' => 'user-roles-Pjax',
    'enablePushState' => false,
    'enableReplaceState' => false,
    'timeout' => 5000
]); ?>

    <div class="user-form">

        <?php $form = ActiveForm::begin([
            'id' => 'user-detail-ActiveForm',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'options' => ['data-pjax' => 1]
        ]); ?>

        <?= $form
            ->field($model, 'listOfAuthRoles', [
                'options' => [
                    'class' => 'form-group container',
                ]
            ])
            ->label(false)
            ->checkboxList($availableRoles, [
                'class' => 'row',
                'item' => function ($index, $label, $name, $checked, $value) {
                    return '<div class="col-md-3">' .
                        Html::checkbox($name, $checked, [
                            'value' => $value,
                            'label' => Html::encode($label),
                            'labelOptions' => [
                                'class' => 'btn btn-light btn-sm m-1 w-100 text-start',
                            ],
                        ]) .
                        '</div>';
                },
            ]);
        ?>

        <div class="form-group text-center">
            <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>

    </div>

<?php yii\widgets\Pjax::end() ?>