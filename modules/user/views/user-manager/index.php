<?php

use app\modules\user\models\User;
use app\widgets\GridView;
use yii\grid\ActionColumn;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/** @var yii\web\View $this */
/** @var app\modules\user\models\search\UserSearch $searchModel */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(['id' => 'user-list-pjax']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'rowDragSort' => [
            'url' => Url::to(['sort', 'reloadPjaxId' => 'user-list-pjax']),
            'pkCol' => 'id',
        ],
        'topButtons' => [
            [
                'label' => Yii::t('app', 'Create User'),
                'options' => ['class' => 'btn btn-success'],
                'url' => Url::to(['create']),
            ],
            [
                'label' => 'Export',
                'options' => [
                    'class' => 'btn btn-outline-secondary p-0',
                ],
                'items' => [
                    [
                        'label' => 'CSV - Ignore grid setup',
                        'url' => Url::to(['index', 'exportTo' => 'csv']),
                        'linkOptions' => ['data-pjax' => 0],
                    ],
                    [
                        'label' => 'CSV - Respect grid setup',
                        'url' => Url::current(['exportTo' => 'csv']),
                        'linkOptions' => ['data-pjax' => 0],
                    ],
                    [
                        'label' => 'XLSX - Ignore grid setup',
                        'url' => Url::to(['index', 'exportTo' => 'xlsx']),
                        'linkOptions' => ['data-pjax' => 0, 'target' => '_blank',],
                    ],
                    [
                        'label' => 'XLSX - Respect grid setup',
                        'url' => Url::current(['exportTo' => 'xlsx']),
                        'linkOptions' => ['data-pjax' => 0, 'target' => '_blank',],
                    ],
                ],
            ],
        ],
        'columns' => [
            'id',
            'name',
            'surname',
            'username',
            'email:email',
            //'phone',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            //'email_verification_token',
            //'email_verified_at',
            //'status',
            //'api_access_token',
            //'api_password_hash',
            //'created_by',
            //'updated_by',
            //'deleted_by',
            //'created_at',
            //'updated_at',
            //'deleted_at',
            [
                'class' => ActionColumn::class,
                'template' => '{roles} {view} {update} {delete} {send-verification-email} {print}',
                'buttons' => [
                    'send-verification-email' => function ($url, User $model, $key) {
                        $colorClass = '';
                        if ($model->email_verified_at) {
                            $colorClass = 'text-success';
                        }
                        $icon = '<i class="bi bi-envelope-check"></i>';
                        return $model->email ? Html::a($icon, $url, [
                            'title' => Yii::t('app', 'Verify email'),
                            'data-pjax' => 0,
                            'class' => $colorClass,
                        ]) : '';
                    },
                    'roles' => function ($url, $model, $key) {
                        return Html::a(
                            '<i class="bi bi-person-lines-fill"></i>',
                            $url,
                            [
                                'title' => Yii::t('app', 'User roles'),
                            ]
                        );
                    },
                    'print' => function ($url, $model, $key) {
                        return Html::a(
                            '<i class="bi bi-printer-fill"></i>',
                            $url,
                            [
                                'title' => Yii::t('app', 'Print user detail'),
                                'data-pjax' => 0,
                                'target' => '_blank',
                            ]
                        );
                    },
                ],
                'urlCreator' => function ($action, User $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'id' => $model->id]);
                }
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
