<?php

/** @var yii\web\View $this */
/** @var app\modules\user\models\User $model */
/** @var array $availableRoles */

$this->title = Yii::t('app', 'Set user roles');

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">

    <?= $this->render('_form_roles', [
        'model' => $model,
        'availableRoles' => $availableRoles,
    ]) ?>

</div>
