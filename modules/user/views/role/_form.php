<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/** @var yii\web\View $this */
/** @var app\modules\user\models\AuthItem $model */
/** @var yii\widgets\ActiveForm $form */
/** @var array $availableRoles */
?>

<div class="auth-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'disabled' => !$model->getIsNewRecord()]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <h2><?= Yii::t('app', 'Child roles') ?>:</h2>

    <?= $form
        ->field($model, 'listOfChildRoles', [
            'options' => [
                'class' => 'form-group container',
            ]
        ])
        ->label(false)
        ->checkboxList($availableRoles, [
            'class' => 'row',
            'item' => function ($index, $label, $name, $checked, $value) {
                return '<div class="col-md-3">' .
                    Html::checkbox($name, $checked, [
                        'value' => $value,
                        'label' => Html::encode($label),
                        'labelOptions' => [
                            'class' => 'btn btn-light btn-sm m-1 w-100 text-start',
                        ],
                    ]) .
                    '</div>';
            },
        ]);
    ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
