<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\user\models\AuthItem $model */
/** @var array $availableRoles */

$this->title = Yii::t('app', 'Update User role: {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'name' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="auth-item-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'availableRoles' => $availableRoles,
    ]) ?>

</div>
