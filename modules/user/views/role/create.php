<?php

use yii\helpers\Html;

/** @var yii\web\View $this */
/** @var app\modules\user\models\AuthItem $model */
/** @var array $availableRoles */

$this->title = Yii::t('app', 'Create User role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'User roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="auth-item-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'availableRoles' => $availableRoles,
    ]) ?>

</div>
