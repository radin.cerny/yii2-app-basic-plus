<?php

namespace app\modules\user\helpers;

use app\models\AuthAssignment;
use app\models\AuthItem;
use app\models\AuthItemChild;

class RbacHelper
{
    public const PREFIX_ROLE_NAME = ':';
    public const PREFIX_PERMISSION_NAME = '.';

    public const ROLE_GLOBAL_ADMIN = ':admin';
    public const ROLE_GLOBAL_CREATOR = ':creator';
    public const ROLE_GLOBAL_READER = ':reader';
    public const ROLE_GLOBAL_UPDATER = ':updater';
    public const ROLE_GLOBAL_DELETER = ':deleter';

    public static function createRbac($authManager, $rbacConfig)
    {
        foreach ($rbacConfig as $classPath => $config) {
            $classPath = RbacHelper::reduceControllerClassPath($classPath);
            RbacHelper::buildRoles($authManager, $classPath, $config);
        }
    }

    public static function deleteAuthItemsForController($classPath)
    {
        $reducedClassPath = RbacHelper::reduceControllerClassPath($classPath);
        AuthItem::deleteAll(['LIKE', 'name', $reducedClassPath . '%', false]);
    }

    /**
     * Class path is modified like this:
     * app\controllers\GmlStockDataController => GmlStockData
     * app\modules\xxx\controllers\GmlStockDataController xxx\GmlStockData
     * @param string $classPath
     * @param string[] $skipParts
     * @return string
     */

    public static function reduceControllerClassPath(string $classPath, array $skipParts = ['app', 'controllers', 'modules'])
    {
        $classPath = substr($classPath, 0, -strlen('Controller')); // Cutting trailing text "Controller"
        $reduced = [];
        foreach (explode('\\', $classPath) as $part) {
            if (!in_array($part, $skipParts)) {
                $reduced[] = $part;
            }
        }
        $result = implode('\\', $reduced);
        return $result;
    }

    public static function buildRoles($authManager, $classPath, $config, $enableDeleting = false)
    {
        $roles = [];
        $permissions = [];

        // Creating RBAC items (roles and below permissions)

        /** @var string $roleName */
        /** @var array $children */
        foreach ($config['hierarchy'] as $role => $children) {
            $roleName = $classPath . self::PREFIX_ROLE_NAME . $role;
            // Try to load the role if it already exists:
            $roles[$roleName] = $authManager->getRole($roleName);
            if (!$roles[$roleName]) {
                $roles[$roleName] = $authManager->createRole($roleName);
                $roles[$roleName]->description = $config['description'][$role] ?? null;
                $authManager->add($roles[$roleName]);
            }
            /** @var string $child */
            foreach ($children as $child) {
                if (isset($config['hierarchy'][$child])) {
                    // All the keys are roles and they are taken care of above
                    continue;
                }
                $permissionName = $classPath . self::PREFIX_PERMISSION_NAME . $child;
                // Try to load the permission if it already exists:
                $permissions[$permissionName] = $authManager->getPermission($permissionName);
                if (!$permissions[$permissionName]) {
                    $permissions[$permissionName] = $authManager->createPermission($permissionName);
                    $permissions[$permissionName]->description = $config['description'][$child] ?? null;
                    $authManager->add($permissions[$permissionName]);
                }
            }
        }

        // Creating the real hierarchy of existing roles and permissions

        /** @var string $roleName */
        /** @var array $children */
        foreach ($config['hierarchy'] as $role => $children) {
            $roleName = $classPath . self::PREFIX_ROLE_NAME . $role;
            /** @var string $child */
            foreach ($children as $child) {
                // We don't know if the child is an action or a role so I am creating both names
                $childRoleName = $classPath . self::PREFIX_ROLE_NAME . $child;
                $childPermissionName = $classPath . self::PREFIX_PERMISSION_NAME . $child;
                $parent = $roles[$roleName];
                $child = $permissions[$childPermissionName] ?? $roles[$childRoleName];
                if (!$authManager->hasChild($parent, $child)) {
                    $authManager->addChild($parent, $child);
                }
            }
        }
    }
}
