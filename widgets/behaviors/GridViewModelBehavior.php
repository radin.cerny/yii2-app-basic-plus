<?php

namespace app\widgets\behaviors;

use yii\base\Behavior;
use yii\helpers\Html;

/**
 * GridViewModelBehavior enables your models to use page-size selector under the app\widgets\GridView
 *
 * To use GridViewModelBehavior, insert the following code to your ActiveRecord class (best is the BaseModel):
 *
 * ```php
 * public function behaviors()
 * {
 *    return [
 *        [
 *            'class'  => \app\widgets\behaviors\GridViewModelBehavior::class,
 *        ],
 *    ];
 * }
 * ```
 */
class GridViewModelBehavior extends Behavior
{
    public $gridPageSize = null;
    public $gridPageSizeDefault = 100;
    public $gridPageSizeRule = ['gridPageSize', 'integer', 'min' => 1];
    public $gridPageSizeValues = [
        10 => '10',
        20 => '20',
        50 => '50',
        100 => '100',
        150 => '150',
        200 => '200',
        300 => '300',
        500 => '500',
        1000 => '1000',
    ];

    public function getGridPageSizeDropDown($htmlOptions = [])
    {
        return Html::activeDropDownList($this->owner, 'gridPageSize', $this->gridPageSizeValues, $htmlOptions);
    }

    public function getGridPageSizeDropDownID($prefix = '#')
    {
        return $prefix . Html::getInputId($this->owner, 'gridPageSize');
    }
}