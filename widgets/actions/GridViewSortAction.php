<?php

namespace app\widgets\actions;

use Yii;
use yii\base\Action;

/**
 * GridViewSortAction enables your controller to perform re-ordering of items in app\widgets\GridView
 *
 * To use GridViewSortAction, use it in the actions() function inside your Controller
 *
 * ```php
 * public function actions()
 * {
 *     return [
 *         'sort' => [
 *             'class' => 'app\widgets\actions\GridViewSortAction',
 *             'sortColName' => 'position',
 *             'modelClass' => User::class,
 *         ],
 *     ];
 * ```
 */
class GridViewSortAction extends Action
{
    public string $sortColName = '';
    public string $modelIdColName = 'id';
    public string $postName = 'position';
    public string $modelClass = '';
    public string $jsOnSuccess = '';

    public function run($reloadPjaxId = null)
    {
        $idsInNewOrder = Yii::$app->request->post($this->postName);
        if (!$idsInNewOrder) {
            return;
        }

        if ($this->sortColName) {
            $sortColName = $this->sortColName;
            $modelClass = $this->modelClass;

            $models = $modelClass::find()
                ->andWhere([$this->modelIdColName => $idsInNewOrder])
                ->indexBy($this->modelIdColName)
                ->all();

            $lowestPosition = (int)$modelClass::find()
                ->andWhere([$this->modelIdColName => $idsInNewOrder])
                ->orderBy([$sortColName => SORT_ASC])
                ->select([$sortColName])
                ->scalar();

            foreach ($idsInNewOrder as $id) {
                if (isset($models[$id])) {
                    $models[$id]->$sortColName = $lowestPosition;
                    $models[$id]->save(false, [$sortColName]);
                    $lowestPosition++;
                }
            }
        }

        $js = '';

        if ($reloadPjaxId) {
            $js .= "$.pjax.reload({container:'#$reloadPjaxId'});";
        }

        if ($this->jsOnSuccess) {
            $js .= $this->jsOnSuccess;
        }

        if ($js) {
            return $js; // is processed in JS method updateOrder()
        }
    }
}