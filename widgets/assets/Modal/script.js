$(document).on('click', 'a.openInMyModal', function (e) {
    e.preventDefault(); // Prevent the default action of the link

    // Prevents the event from bubbling up the DOM
    // ... but does not stop the browsers default behaviour
    e.stopPropagation();

    // Prevents other listeners of the same event from being called
    e.stopImmediatePropagation();

    // Good idea is to set onclick='return false;' to the link if it is in a modal window

    let title = $(this).attr('data-myModalTitle');
    if (title == undefined) {
        title = '';
    }

    $('#myModal .modal-dialog').removeClass('modal-sm').removeClass('modal-lg').removeClass('modal-xl');
    let size = $(this).attr('data-myModalSize');
    if (size == undefined) {
        size = 'xl';
    }
    $('#myModal .modal-dialog').addClass('modal-' + size);


    let spinner = '<div class="d-flex justify-content-center"><div class="spinner-border text-secondary" role="status"><span class="visually-hidden">Loading...</span></div></div>';

    $('#myModal .modal-title').text(title);
    $('#myModal .modal-body').html(spinner);

    // Here is detected if the link requires POST or not. Some links might have these options set:
    // 'onclick' => 'return false;', // if in modal
    // 'class' => 'openInMyModal', // if in modal
    // 'data-method' => 'POST', // if the target action requires POST. Not really documented feature of Yii2, search for "data-method": https://www.yiiframework.com/doc/guide/2.0/en/structure-assets#common-asset-bundles

    let method = e.target.getAttribute("data-method") ? e.target.getAttribute("data-method") : "GET";
    method = method.toUpperCase();
    if (method == "GET") {
        $('#myModal .modal-body').load($(this).attr('href'));
    } else {
        // Documentation: https://api.jquery.com/load/
        // ... "The POST method is used if data is provided as an object; otherwise, GET is assumed."
        $('#myModal .modal-body').load($(this).attr('href'), {});
    }

    $('#myModal').modal('show');
    return false;
});

// This enables filtering of GridView in Modal
$(document).on('submit', 'form[data-pjax]', function (e) {
    let myModal = document.querySelector('div#myModal');
    if ($(myModal).is(":visible")) {
        // If modal is not used, this code should not be executed, otherwise the form will be submitted 2x and 2 requests will be done
        e.preventDefault();
        e.stopPropagation();
        e.stopImmediatePropagation();

        // See details in:
        // vendor/bower-asset/yii2-pjax/jquery.pjax.js
        // Search for function handleSubmit(event, container, options) + function pjax(options)
        $.pjax.submit(e, 'div.modal-body', {"push": false, "replace": false, "timeout": 0, "scrollTo": false})
    }
});