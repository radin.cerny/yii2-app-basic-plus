<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace app\widgets\assets;

use app\modules\admin\assets\AdminAsset;
use yii\web\AssetBundle;

class GridViewAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/assets/gridView';

    public $css = [
        'style.css',
    ];
    public $js = [
        'script.js',
    ];

}
