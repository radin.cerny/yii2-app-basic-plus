function fixHelper(e, ui) {
    ui.children().each(function () {
        $(this).width($(this).width());
    });
    return ui;
};

function makeRowsSortable() {
    $(".row_position").sortable({
        delay: 150,
        stop: function () {
            var selectedData = new Array();
            $('.row_position>tr').each(function () {
                selectedData.push($(this).attr("id"));
            });
            var table = $('.row_position').data('table');
            var url = $(this).attr('data-sort-url');
            updateOrder(selectedData, table, url);
        },
        handle: ".handle",
        helper: fixHelper
    });
}

makeRowsSortable();

function updateOrder(data, table, url, httpMethod) {

    if (httpMethod === undefined) {
        httpMethod = 'post';
    }

    $.ajax({
        url: url,
        type: httpMethod,
        data: {position: data, table: table},
        success: function (data) {
            eval(data); // this allows us to send a command from PHP to JS and for example refresh a div or anything
        }
    })
}

$(document).on('ready pjax:success', function () {
    // after pjax (ajax) calls we have to register all again
    // Typical problem with JS
    // https://stackoverflow.com/questions/24858549/jquery-sortable-not-functioning-when-ajax-loaded
    // https://github.com/yiisoft/yii2/issues/4814
    makeRowsSortable();
});