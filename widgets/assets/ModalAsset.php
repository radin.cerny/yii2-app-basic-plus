<?php

/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace app\widgets\assets;

use app\assets\AppAsset;
use app\modules\admin\assets\AdminAsset;
use yii\web\AssetBundle;

class ModalAsset extends AssetBundle
{
    public $sourcePath = '@app/widgets/assets/Modal';

    public $css = [
        'style.css',
    ];
    public $js = [
        'script.js',
    ];

    public $depends = [
        AppAsset::class,
    ];
}
