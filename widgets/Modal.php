<?php

namespace app\widgets;

use app\widgets\assets\ModalAsset;
use yii\helpers\Html;

/**
 * Usage:
 *
 * 1) Begin form
 * Html::beginForm('', 'post', ['data-pjax' => 1]);
 * ActiveForm::begin(['options' => ['data-pjax' => 1]]);
 * // Pjax cannot send button-value via POST! You need to use hidden inputs.
 *
 * 2) Render in controller
 * $this->renderAjax()
 * if ($model->save()) { return Modal::close('some-Pjax-id'); }
 *
 * 3) Pjax
 * // If your Form is displayed in the modal window, you should wrap the Form in Pjax
 * // If your GridView should be refreshed once the modal si closed, wrap the GridView in Pjax as well
 * use yii\widgets\Pjax;
 * Pjax::begin([
 *   'id' => 'my-Pjax',
 *   'enablePushState' => false,
 *   'enableReplaceState' => false,
 * ]);
 * Pjax::end()
 *
 * Class Modal
 * @package app\widgets
 */
class Modal extends \yii\bootstrap5\Modal
{
    public const ID = 'myModal';

    public const openInModalClass = 'openInMyModal';

    public $options = [
        'id' => self::ID,
    ];

    /**
     * Needs to be present in order to render the title bar
     * @var string
     */
    public $title = 'Modal';

    public $size = self::SIZE_EXTRA_LARGE;

    public $centerVertical = true;

    public $scrollable = true;

    public $closeButton = [
        'class' => 'btn-close btn-close-white',
    ];

    /**
     * Call this in your controller to close the modal and refresh an underlying Pjax container (for example a GridView):
     * return Modal::close('myPjaxId');
     *
     * Pjax must be called like this in order to be refreshable later:
     * `Pjax::begin(['id' => 'myPjaxId']);`
     *
     * @param null $reloadContainerId
     * @return string
     */
    public static function close($reloadContainerId = null)
    {
        $modalId = self::ID;
        $reload = '';

        if ($reloadContainerId) {
            $reload = <<<JS
if (document.getElementById("$reloadContainerId")) {
    $.pjax.reload({container:'#$reloadContainerId'});
};
JS;
        }

        return '<script>'
            . $reload
            . "$('#$modalId').modal('hide');"
            . '</script>';
    }

    public static function closeJs()
    {
        $modalId = self::ID;
        return "$('#$modalId').modal('hide');";
    }

    public static function a($text, $url = null, $options = [], $modalTitle = '', $modalSize = 'md')
    {
        if (!isset($options['class'])) {
            $options['class'] = '';
        }
        $options['class'] = trim($options['class'] . ' ' . self::openInModalClass);

        if (trim($modalTitle) != '') {
            $options['data-myModalTitle'] = $modalTitle;
        }

        if (in_array($modalSize, ['sm', 'md', 'lg'])) {
            $options['data-myModalSize'] = $modalSize;
        }

        return Html::a($text, $url, $options);
    }

    public function init()
    {
        parent::init();
        ModalAsset::register($this->getView());
    }
}
