<?php

namespace app\widgets;

use yii\base\Widget;
use yii\helpers\Html;

/**
 * Example:
 *
 * <div class="fullscreen-content">
 *
 * echo FullScreen::widget([
 *     'buttonCssClass' => 'btn btn-light position-absolute',
 *     'buttonStyle' => 'right:1rem',
 * ]);
 *
 * </div>
 *
 * <style>
 *     .when-fullscreen {
 *         padding: 1rem;
 *     }
 * </style>
 *
 * Class FullScreen
 * @package app\widgets
 */
class FullScreenButton extends Widget
{
    public string $buttonCssClass = 'btn btn-light';
    public string $buttonContent = '<i class="bi bi-arrows-fullscreen"></i>';
    public string $buttonStyle = '';
    public string $toggleSelector = '.fullscreen-content';
    public string $addClass = 'when-fullscreen';
    public bool $oneLineJs = true;

    public function run()
    {
        return Html::button($this->buttonContent, [
            'class' => $this->buttonCssClass,
            'style' => $this->buttonStyle,
            'onclick' => $this->toggleFullscreen(),
        ]);
    }

    private function toggleFullscreen()
    {
        $result = <<<JS
const el = document.querySelector('$this->toggleSelector');
if (!document.fullscreenElement) {
    el.requestFullscreen()
    .then(() => { el.classList.add('$this->addClass') })
    .catch(err => {console.log(err.message)})
} else {
    document.exitFullscreen();
    el.classList.remove('$this->addClass');
}
JS;

        if ($this->oneLineJs) {
            return preg_replace('/\s+/', ' ', $result);
        }

        return $result;
    }
}