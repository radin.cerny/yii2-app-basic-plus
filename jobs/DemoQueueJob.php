<?php

namespace app\jobs;

use yii\base\BaseObject;
use yii\queue\JobInterface;

/**
 * Usage:
 * \Yii::$app->queue->push(new \app\jobs\DemoQueueJob([
 *     'text' => 'Hello world',
 * ]));
 *
 * Class DemoQueueJob
 * @package app\jobs
 */
class DemoQueueJob extends BaseObject implements JobInterface
{
    public $text;

    public function execute($queue)
    {
        file_put_contents('/app/queueTest.txt', $this->text . ' at ' . date('Y-m-d H:i:s') . "\n", FILE_APPEND);
    }
}
