<?php
/**
 * @link https://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license https://www.yiiframework.com/license/
 */

namespace app\gii\ssh;

use Yii;
use yii\gii\CodeFile;

class Generator extends \yii\gii\Generator
{
    /**
     * @var string
     */
    public $privateKeyFilename = 'privateKey.pem';

    /**
     * @var string
     */
    public $certificateFilename = 'cert.pem';

    /**
     * @var string The controller view path
     */
    public $expirationDays = 3650;

    /**
     * @var string
     */
    public $country;

    /**
     * @var string
     */
    public $state;

    /**
     * @var string
     */
    public $locality;

    /**
     * @var string
     */
    public $organization;

    /**
     * @var string
     */
    public $organizationUnit;

    /**
     * @var string
     */
    public $commonName;

    /**
     * @var string
     */
    public $subjectAltName;


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'SSH Generator';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription()
    {
        return 'This generator generates a private key and a public certificate so you can run your web via HTTPS.<br>Command <strong>openssl</strong> will be executed. See method getCliCommand() in /gii/ssh/Generator.php<br>Files will be stored in the <strong>Yii::getAlias("@app")</strong> folder.';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['privateKeyFilename', 'certificateFilename'], 'safe'],
            [['privateKeyFilename', 'certificateFilename', 'expirationDays', 'country', 'state', 'locality', 'organization', 'organizationUnit', 'commonName', 'subjectAltName'], 'required'],
            ['expirationDays', 'integer', 'min' => 1],
            ['country', 'match', 'pattern' => '/^[A-Z]{2}$/', 'message' => Yii::t('app', 'Two upper case letters required')],
            [['state', 'locality', 'organization', 'organizationUnit', 'commonName'], 'match', 'pattern' => '/^[a-zA-Z0-9-\.]{1,64}$/', 'message' => Yii::t('app', '1-64 alfanum chars required (-. are allowed)')],
            [['subjectAltName'], 'match', 'pattern' => '/^[a-zA-Z0-9-\.:]{1,64}$/', 'message' => Yii::t('app', '1-64 alfanum chars required (-. are allowed)')],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'privateKeyFilename' => 'Private key filename (-keyout)',
            'certificateFilename' => 'Certificate filename (-out)',
            'expirationDays' => 'Expiration in days (-days)',
            'country' => 'Country (/C)',
            'state' => 'State (/ST)',
            'locality' => 'Locality (/L)',
            'organization' => 'Organization (/O)',
            'organizationUnit' => 'Organization Unit (/OU)',
            'commonName' => 'Common name, IP or domain (/CN)',
            'subjectAltName' => 'Subject alt name (IP:1.2.3.4 or DNS:myweb.com)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function hints()
    {
        return array_merge(parent::hints(), [
            'privateKeyFilename' => 'Will be stored in folder Yii::getAlias("@app")',
            'certificateFilename' => 'Will be stored in folder Yii::getAlias("@app")',
            'expirationDays' => 'Expiration in days (-days)',
            'country' => 'Country (/C)',
            'state' => 'State (/ST)',
            'locality' => 'Locality (/L)',
            'organization' => 'Organization (/O)',
            'organizationUnit' => 'Organization Unit (/OU)',
            'commonName' => 'IP or domain',
            'subjectAltName' => 'IP:1.2.3.4 or DNS:myweb.com',
        ]);
    }

//    /**
//     * {@inheritdoc}
//     */
//    public function stickyAttributes()
//    {
//        return array_merge(parent::stickyAttributes(), array_keys($this->attributeLabels()));
//    }

    /**
     * {@inheritdoc}
     */
    public function generate()
    {

        $cmd = $this->getCliCommand();
        exec($cmd);

        $files = [];

        $files[] = new CodeFile(
            $this->getPrivateKeyFilePath(),
            file_exists($this->getPrivateKeyFilePath()) ? file_get_contents($this->getPrivateKeyFilePath()) : "Following command didnt work:\n" . $cmd
        );

        $files[] = new CodeFile(
            $this->getCertificateFilePath(),
            file_exists($this->getCertificateFilePath()) ? file_get_contents($this->getCertificateFilePath()) : "Following command didnt work:\n" . $cmd
        );

        return $files;
    }

    private function getPrivateKeyFilePath()
    {
        return Yii::getAlias('@app/' . $this->privateKeyFilename);
    }

    private function getCertificateFilePath()
    {
        return Yii::getAlias('@app/' . $this->certificateFilename);
    }

    private function getCliCommand()
    {
        $privateKeyFilePath = $this->getPrivateKeyFilePath();
        $certificateFilePath = $this->getCertificateFilePath();

        // Correct command should look ca like this:
        // openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 3650 -nodes -subj "/C=CZ/ST=KV/L=CITY/O=RBSOFT/OU=IT/CN=192.168.1.1" -addext "subjectAltName = IP:192.168.1.1" -addext "basicConstraints=CA:TRUE"
        // - CA:True ... is needed so the certificate can be installed on Android.
        // - If you are not using IP, but domain, use "DNS:foo.co.uk" instead of "IP:192.168.1.1"

        return <<<CMD
openssl req -x509 -newkey rsa:4096 -keyout $privateKeyFilePath -out $certificateFilePath -sha256 -days $this->expirationDays -nodes -subj "/C=$this->country/ST=$this->state/L=$this->locality/O=$this->organization/OU=$this->organizationUnit/CN=$this->commonName" -addext "subjectAltName = $this->subjectAltName" -addext "basicConstraints=CA:TRUE"
CMD;
    }
}
