<?php
/** @var yii\web\View $this */
/** @var yii\widgets\ActiveForm $form */
/** @var yii\gii\generators\crud\Generator $generator */

echo $form->field($generator, 'privateKeyFilename');
echo $form->field($generator, 'certificateFilename');
echo $form->field($generator, 'expirationDays');
echo $form->field($generator, 'country');
echo $form->field($generator, 'state');
echo $form->field($generator, 'locality');
echo $form->field($generator, 'organization');
echo $form->field($generator, 'organizationUnit');
echo $form->field($generator, 'commonName');
echo $form->field($generator, 'subjectAltName');

//echo $form->field($generator, 'indexWidgetType')->dropDownList([
//    'grid' => 'GridView',
//    'list' => 'ListView',
//]);
//echo $form->field($generator, 'enableI18N')->checkbox();
//echo $form->field($generator, 'enablePjax')->checkbox();
//echo $form->field($generator, 'messageCategory');
