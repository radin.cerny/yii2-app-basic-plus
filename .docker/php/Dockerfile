# https://hub.docker.com/r/yiisoftware/yii2-php/tags
# https://github.com/yiisoft/yii2-docker
FROM yiisoftware/yii2-php:8.3-apache as http

# Because of RabbitMQ
# wait-for-it is used in docker-compose.yml
RUN docker-php-ext-install sockets && apt-get update -y && apt-get install -y wait-for-it

# Installing DB client so PHP can backup DB if needed in all containers based on this Dockerfile
#RUN apt-get update && apt-get install -y default-mysql-client && rm -rf /var/lib/apt
#RUN apt-get update && apt-get install -y mysql-client && rm -rf /var/lib/apt
RUN apt-get update && apt-get install -y mariadb-client && rm -rf /var/lib/apt

# RUN pecl install xdebug-3.2.1 ... already present in the Yii image
# RUN docker-php-ext-enable xdebug ... can be used to enable xdebug, but better is the environment variable PHP_ENABLE_XDEBUG in docker-compose.yml

COPY xdebug.ini /usr/local/etc/php/conf.d/xdebug2.ini
COPY php.ini /usr/local/etc/php/conf.d/custom.ini

# ===================================
# HTTPS version of the same server:
# ===================================

# Note to HTTPS:
# - You will need self-signed certificates for dev purposes. They are created like this:
# cd {into folder .docker/php}
# openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -sha256 -days 3650 -nodes -subj "/C=CZ/ST=KV/L=VARY/O=COMPANY/OU=IT/CN=192.168.141.74" -addext "subjectAltName = IP:192.168.141.74" -addext "basicConstraints=CA:TRUE"
# - Just update the IP
# - If you are using mydomain.com, use "DNS:mydomain.com" in the subjectAltName
# - CA:True ... is needed so the certificate can be installed to Android
# - Certificate can be tested using command: openssl x509 -in cert.pem -noout -text
# - Do not forget to copy `cert.pem` to certificates in your OS (Postman can use HTTPS without the certificate, see its Settings)
# - Once done, open `docker-compose.yml and change "target" to "https" and call `docker-compose up --build`
# - If there is some problem, you can add "ServerName 1.2.3.4" to `default-ssl.conf`. But for me it works without this value.

FROM yiisoftware/yii2-php:8.3-apache as https

# Because of RabbitMQ
# wait-for-it is used in docker-compose.yml
RUN docker-php-ext-install sockets && apt-get update -y && apt-get install -y wait-for-it

# Installing DB client so PHP can backup DB if needed in all containers based on this Dockerfile
#RUN apt-get update && apt-get install -y default-mysql-client && rm -rf /var/lib/apt
#RUN apt-get update && apt-get install -y mysql-client && rm -rf /var/lib/apt
RUN apt-get update && apt-get install -y mariadb-client && rm -rf /var/lib/apt

COPY xdebug.ini /usr/local/etc/php/conf.d/xdebug2.ini
COPY php.ini /usr/local/etc/php/conf.d/custom.ini

# Enabling the SSL module in Apache
# socache_shmcb is for caching SSL sessions which reduces the overhead of SSL handshakes etc.
RUN a2enmod ssl && a2enmod socache_shmcb

# Install SSL/TLS certificates
COPY cert.pem /etc/ssl/certs/server.crt
COPY key.pem /etc/ssl/private/server.key

# Configure Apache to use SSL/TLS
COPY default-ssl.conf /etc/apache2/sites-available/default-ssl.conf
RUN a2ensite default-ssl.conf

# Expose port 443 for HTTPS
EXPOSE 443