<?php

use app\modules\user\models\User;

return [
    'User1' => [
        'id' => 100,
        'name' => 'admin',
        'surname' => 'admin',
        'username' => 'admin',
        'email' => 'admin@gmail.com',
        'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
        'status' => User::STATUS_ACTIVE,
        'auth_key' => '100-auth-key',
        'api_access_token' => '100-api-token',
    ],
];
