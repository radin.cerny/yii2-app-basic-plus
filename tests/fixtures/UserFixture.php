<?php

namespace app\tests\fixtures;

use app\modules\user\models\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = User::class;
    public $dataFile = '@app/tests/fixtures/data/User.php';

}