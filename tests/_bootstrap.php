<?php
define('YII_ENV', 'test');
defined('YII_DEBUG') or define('YII_DEBUG', true);

require_once __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../config/env.php';

// How to run migrations for the test DB:
// /app/tests/bin/yii migrate/fresh --interactive=0