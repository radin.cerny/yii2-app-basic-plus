<?php

use app\tests\fixtures\UserFixture;
use yii\helpers\Url;

class LoginCest
{
    public function _fixtures()
    {
        return [
            'User' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function ensureThatLoginWorks(AcceptanceTester $I)
    {
        $I->amOnPage(Url::toRoute('/admin/login'));
        $I->see('Login', 'h1');

        $I->amGoingTo('try to login with correct credentials');
        $I->fillField('input[name="LoginForm[username]"]', 'admin');
        $I->fillField('input[name="LoginForm[password]"]', 'admin');
        $I->click('login-button');
        $I->wait(2); // wait for button to be clicked

        $I->expectTo('see user info');
        $I->see('Logout (admin)');
    }
}
