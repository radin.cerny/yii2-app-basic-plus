<?php


namespace Api;

use ApiTester;

class DemoCest
{
    public function _before(ApiTester $I)
    {
    }

    public function tryToTest(ApiTester $I)
    {
        $I->haveHttpHeader('accept', 'application/json');
        $I->haveHttpHeader('content-type', 'application/json');
        $I->sendGet('/default');
        $I->seeResponseCodeIs(200);
        $I->seeResponseIsJson();
        $response = \yii\helpers\Json::decode($I->grabResponse());
        $I->assertEquals($response[0], 1);
    }
}
