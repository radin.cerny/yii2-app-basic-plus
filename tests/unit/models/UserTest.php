<?php

namespace tests\unit\models;

use app\modules\user\models\User;
use app\tests\fixtures\UserFixture;

class UserTest extends \Codeception\Test\Unit
{

    public function _fixtures()
    {
        return [
            'User' => [
                'class' => UserFixture::class,
            ],
        ];
    }

    public function testFindUserById()
    {
        verify($user = User::findIdentity(100))->notEmpty();
        verify($user->username)->equals('admin');

        verify(User::findIdentity(999))->empty();
    }

    public function testFindUserByAccessToken()
    {
        verify($user = User::findIdentityByAccessToken('100-api-token'))->notEmpty();
        verify($user->username)->equals('admin');

        verify(User::findIdentityByAccessToken('non-existing'))->empty();
    }

    public function testFindUserByUsername()
    {
        verify($user = User::findByUsername('admin'))->notEmpty();
        verify(User::findByUsername('not-admin'))->empty();
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser()
    {
        $user = User::findByUsername('admin');
        verify($user->validateAuthKey('100-auth-key'))->notEmpty();
        verify($user->validateAuthKey('wrong-auth-key'))->empty();

        verify($user->validateBackendPassword('admin'))->notEmpty();
        verify($user->validateBackendPassword('123456'))->empty();
    }
}
