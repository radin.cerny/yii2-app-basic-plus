<?php

return [
    'class' => 'yii\rbac\DbManager',
    // uncomment if you want to cache RBAC items hierarchy
    // 'cache' => 'cache',
];
