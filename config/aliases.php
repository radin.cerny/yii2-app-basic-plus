<?php

return [
    '@bower' => '@vendor/bower-asset',
    '@npm' => '@vendor/npm-asset',
    '@tests' => '@app/tests',
    '@admin' => '/' . env('BACKEND_ALIAS'),
    '@api' => '/' . env('API_ALIAS'),
];
