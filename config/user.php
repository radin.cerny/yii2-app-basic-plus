<?php

return [
    'identityClass' => 'app\modules\user\models\User',
    'enableAutoLogin' => true,
    'loginUrl' => ['@admin/login'],
];
