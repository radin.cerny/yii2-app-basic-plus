<?php

return [
    env('BACKEND_ALIAS') => [
        'class' => 'app\modules\admin\Module',
        'modules' => [
            'location' => [
                'class' => 'app\modules\location\Module',
            ],
            'user' => [
                'class' => 'app\modules\user\Module',
            ],
            'intl' => [
                'class' => 'app\modules\intl\Module',
            ],
            'documents' => [
                'class' => 'app\modules\documents\Module',
            ],
            'settings' => [
                'class' => 'app\modules\settings\Module',
            ],
            'message' => [
                'class' => 'app\modules\message\Module',
            ],
        ],
    ],
    env('API_ALIAS') => [
        'class' => 'app\modules\api\Module',
    ],
];
