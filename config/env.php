<?php

// Load environment variables from .env file
// The file should not be present in Git as every environment will provide a different content
// .env.example is used instead if you haven't created your .env yet
try {
    Dotenv\Dotenv::createImmutable([__DIR__ . '/../'], ['.env'])->load();
} catch (Dotenv\Exception\InvalidPathException $ex) {
    Dotenv\Dotenv::createImmutable([__DIR__ . '/../'], ['.env.example'])->load();
}

function env($key, $default = false)
{

    $value = isset($_ENV[$key]) ? $_ENV[$key] : getenv($key);

    if ($value === false) {
        return $default;
    }

    switch (strtolower($value)) {
        case 'true':
        case '(true)':
            return true;

        case 'false':
        case '(false)':
            return false;

        case 'null':
        case '(null)':
            return null;
    }

    return $value;
}
