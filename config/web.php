<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$queue = require __DIR__ . '/queue.php';
$modules = require __DIR__ . '/modules.php';
$urlManager = require __DIR__ . '/urlManager.php';
$authManager = require __DIR__ . '/authManager.php';
$log = require __DIR__ . '/log.php';
$mailer = require __DIR__ . '/mailer.php';
$container = require __DIR__ . '/di_container.php';
$user = require __DIR__ . '/user.php';
$aliases = require __DIR__ . '/aliases.php';
$api = env('API_ALIAS');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'queue', $api], // Why bootstrapping module API? See its file Module.php
    'aliases' => $aliases,

    // language used in the PHP code
    'sourceLanguage' => 'en',

    // To what language we want to translate the web.
    // Can be set programmatically using \Yii::$app->language = 'cs';
    // 'language' => 'cs',

    'modules' => $modules,
    'container' => $container,
    'components' => [
        'queue' => $queue,
        'authManager' => $authManager,
        'csvExporter' => [
            'class' => 'app\modules\admin\components\CsvExporterComponent',
        ],
        'xlsxExporter' => [
            'class' => 'app\modules\admin\components\XlsxExporterComponent',
        ],
        'pdfExporter' => [
            'class' => 'app\modules\admin\components\PdfExporterComponent',
        ],
        'settings' => [
            'class' => 'app\modules\settings\components\SettingsComponent',
        ],
        'messenger' => [
            'class' => 'app\modules\message\components\MessengerComponent',
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => yii\i18n\DbMessageSource::class,
                    'sourceLanguage' => 'en',
                    'sourceMessageTable' => '{{%intl_translation_source}}',
                    'messageTable' => '{{%intl_translation}}',
                    'cachingDuration' => 86400,
                    'enableCaching' => !YII_DEBUG,
                    'forceTranslation' => true,
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => env('COOKIE_VALIDATION_KEY'),
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => $user,
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => $mailer,
        'log' => $log,
        'db' => $db,
        'urlManager' => $urlManager,
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['127.0.0.1', '::1', '*'],
        'generators' => [
            // All generators are listed in method coreGenerators() in vendor/yiisoft/yii2-gii/src/Module.php
            // Below are only used those that need to be modified or added.
            // The new generators are placed in folder /gii in this project.
            // They can be copied from folder vendor/yiisoft/yii2-gii/src/generators and then modified
            'ssh' => [
                // This generator allows you to create public and private key based on your IP or domain.
                // Then you will be able to run your web via HTTPS even locally or inside your company.
                // Resulting files must be installed to the server and to the devices that will use the web.
                // See README.md for details.
                'class' => 'app\gii\ssh\Generator',
                'templates' => [
                    'project' => '@app/gii/ssh/project', // key = folder name in /gii/generators/crud
                ]
            ],
        ],
    ];
}

return $config;
