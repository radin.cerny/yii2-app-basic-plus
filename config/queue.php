<?php

/** @var $params array */

// Use this file like this in web.php and console.php:

// $queue = require __DIR__ . '/queue.php';
// 'components' => [
//   'queue' => $queue,

// Note that different drivers have different CLI API. In case of AMQP only works command "yii queue/listen".
// See: https://github.com/yiisoft/yii2-queue/tree/master/src/drivers

switch (env('QUEUE')) {
    case 'rabbitmq':
        // call: "php yii queue/run" or "php yii queue/listen" in a parallel process (see docker-compose.yml)
        return [
            // composer require yiisoft/yii2-queue ... https://github.com/yiisoft/yii2-queue
            // composer require enqueue/amqp-lib ... is wrapper for: php-amqplib/php-amqplib
            // Official manual below, plus see the menu and Introduction on the left!
            // https://www.yiiframework.com/extension/enqueue/amqp-lib/doc/guide/2.0/en/driver-amqp-interop

            // You PHP server needs "ext-sockets".
            // - Plus locally you might use "wait-for-it" - for waiting for RabbitMQ to be up. See docker-compose.yml.
            // So type this to the Dockerfile:
            // RUN docker-php-ext-install sockets && apt-get update -y && apt-get install -y wait-for-it
            // - If you do not install ext-sockets composer will download older enqueue/amqp-lib and it wont be compatible with yiisoft/yii2-queue
            // - I am using these version: "enqueue/amqp-lib": "^0.10.0@dev" + "yiisoft/yii2-queue": "^2.0@dev"

            'class' => \yii\queue\amqp_interop\Queue::class,
            'host' => 'rabbitmq',
            'port' => 5672,
            'user' => env('RMQ_USER'),
            'password' => env('RMQ_PASS'),
            'queueName' => 'queue',
            'driver' => yii\queue\amqp_interop\Queue::ENQUEUE_AMQP_LIB,

            // or
            // 'dsn' => 'amqp://guest:guest@rabbitmq:5672/%2F',
            // or, same as above
            // 'dsn' => 'amqp:',

            'as log' => \yii\queue\LogBehavior::class,
        ];

    case 'sync':
        // runs in standard synchronous way, user will have to wait and browser may stop responding for a while
        return [
            'class' => \yii\queue\sync\Queue::class,
            'handle' => true, // whether tasks should be executed immediately on EVENT_AFTER_REQUEST
        ];
    case 'file':
        // Is saved to a file, later needs to be started for example by cron like this: php yii queue/run
        return [
            'class' => \yii\queue\file\Queue::class,
            'path' => '@runtime/queue',
        ];
    default:
        return [];
}
