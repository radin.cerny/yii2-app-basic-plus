<?php
$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/test_db.php';
$modules = require __DIR__ . '/modules.php';
$urlManager = require __DIR__ . '/urlManager.php';
$user = require __DIR__ . '/user.php';
$aliases = require __DIR__ . '/aliases.php';

// In tests the entry script must be index-test.php
// If it is hidden, all requests are redirected to index.php using .htaccess
$urlManager['showScriptName'] = true;

/**
 * Application configuration shared by all test types
 */
return [
    'id' => 'basic-tests',
    'basePath' => dirname(__DIR__),
    'aliases' => $aliases,
    'language' => 'en-US',
    'modules' => $modules,
    'components' => [
        'db' => $db,
        'mailer' => [
            'class' => \yii\symfonymailer\Mailer::class,
            'viewPath' => '@app/mail',
            // send all mails to a file by default.
            'useFileTransport' => true,
            'messageClass' => 'yii\symfonymailer\Message'
        ],
        'assetManager' => [
            'basePath' => __DIR__ . '/../web/assets',
        ],
        'urlManager' => $urlManager,
        'user' => $user,
        'request' => [
            'cookieValidationKey' => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
    ],
    'params' => $params,
];
