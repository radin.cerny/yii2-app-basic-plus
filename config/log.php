<?php

return [
    'traceLevel' => YII_DEBUG ? 3 : 0,
    'targets' => [
        [
            'class' => 'yii\log\DbTarget',
            'levels' => ['error', 'warning'],
            'logTable' => '{{%yii_log}}',
        ],
//        [
//            'class' => 'yii\log\EmailTarget',
//            'levels' => ['error', 'warning'],
//            'message' => [
//                'from' => $params['adminEmail'],
//                'to' => $params['adminEmail'],
//                'subject' => 'Web Error',
//            ],
//        ],
    ],
];
