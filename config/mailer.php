<?php

// composer require symfony/google-mailer
// Search for Yii::$app->mailer in this application for a demo
//
return [
    'class' => \yii\symfonymailer\Mailer::class,
    'viewPath' => '@app/mail',

    // Save all mails to folder runtime/mail if TRUE
    'useFileTransport' => env('EMAIL_SAVE_TO_FILE'),

    'transport' => [
        // Gmail settings: https://github.com/symfony/symfony-docs/issues/17115
        //'dsn' => 'gmail://{your@gmail.com}:{appPassword}@default',
        // Other SMTP servers:
        //'dsn' => 'smtp://user:pass@smtp.example.com:465',
        'dsn' => env('EMAIL_DSN'),

        // Or you can use these values instead of DSN
        // ... DSN has precedence, see createTransport() in yii\symfonymailer\Mailer
        // 'scheme' => env('EMAIL_SCHEME'),
        // 'host' => env('EMAIL_HOST'),
        // 'username' => env('EMAIL_USERNAME'),
        // 'password' => env('EMAIL_PASSWORD'),
        // 'port' => env('EMAIL_PORT'),

        // Documentation:
        // https://www.yiiframework.com/doc/guide/2.0/en/tutorial-mailing
        // https://symfony.com/doc/current/mailer.html#using-built-in-transports
    ],

];
