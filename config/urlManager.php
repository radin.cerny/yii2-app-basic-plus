<?php

// TODO: env('BACKEND_ONLY') was temporarily disabled do simplify the situation with languages. See history in Git.

$backend = env('BACKEND_ALIAS');

return [
    'class' => 'app\modules\intl\components\UrlManager',
    'langConversion' => ['cz' => 'cs'],
    'supportedLanguages' => ['en', 'cs'],
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // More rules are added in following files
        // /modules/api/Module.php
        // /modules/intl/components/UrlManager.php
        // You can also look for "->addRules(" or "'rules'"

        // Shortcut URLs without the language code:
        $backend . '/login' => $backend . '/user/user-manager/login',
        $backend . '/logout' => $backend . '/user/user-manager/logout',
        $backend . '/request-password-reset' => $backend . '/user/user-manager/request-password-reset',
        $backend . '/reset-password' => $backend . '/user/user-manager/reset-password',
        $backend . '/verify-email' => $backend . '/user/user-manager/verify-email',
    ],
];