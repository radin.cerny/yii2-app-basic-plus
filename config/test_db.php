<?php
$db = require __DIR__ . '/db.php';
// test database! Important not to run tests on production or development databases
$db['dsn'] = sprintf('mysql:host=%s;dbname=%s', env('DB_HOST_TEST'), env('DB_DATABASE_TEST'));

return $db;
