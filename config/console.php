<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';
$queue = require __DIR__ . '/queue.php';
$modules = require __DIR__ . '/modules.php';
$authManager = require __DIR__ . '/authManager.php';
$log = require __DIR__ . '/log.php';
$mailer = require __DIR__ . '/mailer.php';
$urlManager = require __DIR__ . '/urlManager.php';
$aliases = require __DIR__ . '/aliases.php';

$urlManager['hostInfo'] = env('HOST_INFO');
$urlManager['baseUrl'] = env('BASE_URL');

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'queue'],
    'controllerNamespace' => 'app\commands',
    'aliases' => $aliases,
    'modules' => $modules,
    'components' => [
        'queue' => $queue,
        'authManager' => $authManager,
        'xlsxExporter' => [
            'class' => 'app\modules\admin\components\XlsxExporterComponent',
        ],
        'messenger' => [
            'class' => 'app\modules\message\components\MessengerComponent',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'urlManager' => $urlManager,
        'mailer' => $mailer,
        'log' => $log,
        'db' => $db,
    ],
    'params' => $params,
    'controllerMap' => [
//        'fixture' => [ // Fixture generation command line.
//            'class' => 'yii\faker\FixtureController',
//        ],
        'migrate' => [
            // https://github.com/samdark/yii2-cookbook/blob/master/book/using-custom-migration-template.md
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => '@app/migrations/template.php',
            'migrationTable' => '{{%yii_migration}}',
            'migrationPath' => [
                '@app/migrations',
                '@app/modules/admin/migrations',
                '@app/modules/documents/migrations',
                '@app/modules/intl/migrations',
                '@app/modules/location/migrations',
                '@app/modules/message/migrations',
                '@app/modules/settings/migrations',
                '@app/modules/user/migrations',
                '@vendor/yiisoft/yii2/log/migrations',
                '@vendor/yiisoft/yii2/rbac/migrations',
            ],
        ],
    ],
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
    // configuration adjustments for 'dev' environment
    // requires version `2.1.21` of yii2-debug module
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        //'allowedIPs' => ['127.0.0.1', '::1'],
    ];
}

return $config;
