<?php

// https://www.yiiframework.com/doc/guide/2.0/en/concept-di-container
return [
    'definitions' => [
        \yii\widgets\LinkPager::class => [
            // Pager is not formatted without this class defined. See class BaseListView and attribute $pager
            'class' => 'yii\bootstrap5\LinkPager',
        ],
        \yii\grid\DataColumn::class => [
            'contentOptions' => ['style' => 'text-align:center;'],
            'filterInputOptions' => ['class' => 'form-control', 'style' => 'text-align:center;'],
            'headerOptions' => ['style' => 'text-align:center;'],
        ],
        \yii\jui\DatePicker::class => [
            'dateFormat' => 'yyyy-MM-dd',
            'options' => ['class' => 'form-control'],
        ],
        \yii\widgets\Pjax::class => [
            'enablePushState' => false,
            'enableReplaceState' => false,
            'timeout' => 5000, // If the timeout is too short, Yii will try to fix it by sending a 2nd request.
        ],
        \yii\widgets\ActiveForm::class => [
//            'enableAjaxValidation' => false,
//            'enableClientValidation' => false,
            'options' => ['data-pjax' => 1], // All ActiveForms will be processed by Pjax if wrapped by the Pjax widget.
        ],
    ],
];
