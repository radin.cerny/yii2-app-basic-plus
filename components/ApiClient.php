<?php


namespace app\components;

use Exception;
use yii\base\BaseObject;
use yii\httpclient\Client;

abstract class ApiClient extends BaseObject
{
    /**
     * Beginning of the absolute URL which is shared by all the endpoints
     *
     * @var string
     */
    public string $apiBaseUrl;

    /**
     * Relative path to the login-endpoint
     * @var string
     */
    public string $loginEndpoint;

    /**
     * Example: ['username' => 'user', 'password' => 'pass']
     * @var array
     */
    public array $loginCredentials;

    private Client $httpClient;

    /**
     * The goal is to extract accessToken and tokenExpiry from the response.
     * Example:
     *   private string $accessToken = '';
     *   private int $tokenExpiry = 0;
     *   public function processLoginResponseData($data)
     *   {
     *       $this->accessToken = $data['accessToken'];
     *       $this->tokenExpiry = $data['tokenExpiry'];
     *   }
     * @param $data
     * @return mixed
     */
    public abstract function processLoginResponseData($data);

    /**
     * Example:
     * public function getAuthorizationHeaders()
     * {
     *     $accessToken = $this->accessToken; // Or can be obtained from $this->authenticate() if empty or expired
     *     return ['Authorization' => "Bearer {$accessToken}"];
     * }
     * @return mixed
     */
    public abstract function getAuthorizationHeaders();

    public function init()
    {
        parent::init();
        $this->httpClient = new Client();
    }

    public function sendRequest($method, $endpoint, $data = [])
    {
        $request = $this->httpClient->createRequest()
            ->setMethod($method)
            ->setUrl($this->apiBaseUrl . $endpoint)
            ->addHeaders($this->getAuthorizationHeaders())
            ->setData($data);

        $response = $request->send();

        if (in_array($response->statusCode, [401, 403])) {
            $this->authenticate();
            $request->addHeaders($this->getAuthorizationHeaders());
            $response = $request->send();
        }

        return $response;
    }


    protected function authenticate()
    {
        $response = $this->httpClient->post($this->apiBaseUrl . $this->loginEndpoint, $this->loginCredentials)->send();

        if ($response->isOk) {
            $this->processLoginResponseData($response->data);
        } else {
            throw new Exception('Authentication failed (is VPN on?): ' . $response->content);
        }
    }
}