<?php

namespace app\models;

use app\behaviors\ChangeLogBehavior;
use app\widgets\behaviors\GridViewModelBehavior;
use DateTime;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class BaseModel extends ActiveRecord
{
    /**
     * Returns list of values from a table in format [$key => $value]
     * Shouldn't be used in case of large tables.
     * See how $from and $to values are treated in ArrayHelper::map() to understand $key and $value arguments
     *
     * Example of usage:
     *
     * 1) Selection of all unique values form one table:
     *    User::getFilterData('id', 'email')
     *
     * 2) This scenario will return only those parent records (UserGroup) that are used in the child table (User). Useful for filters in GridView.
     *    User::getFilterData(function ($model, $defaultValue) {
     *      return $model->userGroup->id;
     *    }, function ($model, $defaultValue) {
     *      return $model->userGroup->name;
     *    }, 'userGroup')
     *
     * @param string|Closure $key callback: function ($model, $defaultValue) { return $model->name; }
     * @param string|Closure $value callback: function ($model, $defaultValue) { return $model->name; }
     * @param array $with
     * @param array $extraKeyValues
     * @return array
     */
    public static function getFilterData($key, $value, $with = [], $extraKeyValues = []): array
    {
        $result = ArrayHelper::map(self::find()->with($with)->all(), $key, $value);
        foreach ($extraKeyValues as $key => $value) {
            $result[$key] = $value;
        }
        return $result;
    }

    public function behaviors()
    {
        return [
            [
                'class' => GridViewModelBehavior::class,
                // because by app\widgets\GridView - page size support
            ],
            [
                'class' => BlameableBehavior::class,
                // requires columns: created_by + updated_by
            ],
            [
                'class' => TimestampBehavior::class,
                'value' => (new DateTime('now'))->format('Y-m-d H:i:s'),
                // requires columns: created_at + updated_at
            ],
            [
                'class' => ChangeLogBehavior::class,
            ],
        ];
    }

    public function isScenario($scenario)
    {
        return $this->getScenario() === $scenario;
    }

    public function getCsvHeaderArray()
    {
        return array_keys($this->attributes);
    }

    public function getCsvRowArray()
    {
        return $this->attributes;
    }

    public function getXlsxHeaderArray()
    {
        return array_keys($this->attributes);
    }

    public function getXlsxRowArray()
    {
        return $this->attributes;
    }

    public function getPdfHtml()
    {
        $data = ['Hello', 'World'];

        $html = <<<HTML
<table cellspacing="0" cellpadding="1" border="0.1">
    <tr>
        <td style="background-color: black; color:white; text-align: center;" width="45">Demo</td>
    </tr>
HTML;

        foreach ($data as $i => $text) {
            $back = 'background-color: ' . ($i % 2 ? 'rgb(230,230,230);' : 'white') . ';';
            $html .= '<tr>';
            $html .= '<td style="text-align: center;' . $back . '">' . $text . '</td>';
            $html .= '</tr>';
        }

        $html .= '</table>';

        return $html;
    }
}
