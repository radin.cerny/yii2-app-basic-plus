<p align="center">
    <a href="https://github.com/yiisoft" target="_blank">
        <img src="https://avatars0.githubusercontent.com/u/993323" height="100px">
    </a>
    <h1 align="center">Yii 2 Basic Project Template PLUS</h1>
    <br>
</p>

This project extends the [Basic Yii application](https://github.com/yiisoft/yii2-app-basic)
and adds many features that I need in every project. 
If you need basic info about the original application, open their repository and scroll down.

> This project is meant for skilled developers who understand and like what I did. 
> Otherwise, please, use the original Basic Yii application.

# Available URLs
Note that ports must correspond to `docker-compose.yml`
- [http://localhost:6080](http://localhost:6080) = Frontend
- [http://localhost:6080/admin](http://localhost:6080/admin) = Backend
  - Note: If you disable the frontend (see value `BACKEND_ONLY` in `.env` and its usage) then backend will be on the shorter URL
- [http://localhost:6080/api](http://localhost:6080/api) = REST API
- [http://localhost:6080/debug](http://localhost:6080/debug) = Debugger
- [http://localhost:6080/gii](http://localhost:6080/gii) = Gii
- [http://localhost:6082](http://localhost:6082) = Rabbit MQ manager (login is in `docker-compose.yml`)
- [http://localhost:6081](http://localhost:6081) = Adminer. Quick login-link is: 
  - [http://localhost:6081/?server=db&username=db&db=db](http://localhost:6081/?server=db&username=db&db=db) for WEB DB 
  - [http://localhost:6081/?server=db_test&username=db&db=db](http://localhost:6081/?server=db_test&username=db&db=db) for TEST DB 
  - See `docker-compose.yml` for correct values.

If HTTPS is enabled (see `.docker/php/Dockerfile`, you can access it using:
- [https://localhost:6443](https://localhost:6443)

# Changelog

>I started on June 14, 2024 and will be adding new features regularly. 
Below are described all the changes and instructions on how to use them


- More containers in docker-compose.yml:
  - **php** container with the Yii application
  - **composer** container is the same as **php**, but only runs "composer install" and terminates
  - **db** container is for standard DB
  - **db_test** container is DB for tests
  - **adminer** is an in-browser DB manager. Single PHP file, see their [web](https://www.adminer.org/en/).
  - **rabbitmq** is the RabbitMQ (See `DemoQueueJob.php` for an example)
  - **rabbitmq_worker** is the same as **php**, but runs just `yii queue/listen`
  - See folder .docker for details
- Dependency **vlucas/phpdotenv** was used
  - File `config/env.php` defines method `env()` which reads environmental variables. But autoload must be called beforehand.
  - File `config/env.php` is `required` in all 4 entry-scripts (`web/index.php`, `web/index-test.php`, `yii`, `tests/bin/yii`)
  - But constants `YII_DEBUG` and `YII_ENV` should be set (in entry-scripts) before the autoload is called,
    so I read their value from array in `.env.php`. Read explanation in `web/index.php`.
- Modules `Gii` and `Debug` were enabled in Docker
- Pretty URLs enabled
- Besides `RabbitMQ` you can use more queue types. See file `config/queue.php` 


- Modules `admin`, `user`, `api` were added. 
  - URL of the `admin` module can be renamed using `BACKEND_ALIAS`
  - The `api` module returns JSON to Postman + Ajax, but XML if called from browser's address bar.
- UrlRules were defined in `urlRules.php`
  - If you only want to use backend (admin module), you can disable the frontend and use shorter URL - see value `BACKEND_ONLY` in `.env` and its usage 
- Auth manager is defined - RBAC will be stored in DB
- Logs are saved to DB
- Migrations for Log and Rbac modules are executed
- Migrations extend from `BaseMigration` and new gii-template was created, see folder `app/migrations`
- Added demo actions in modules/admin/controllers/DefaultController.php
- Emails enabled (via Gmail SMTP), see file `.env` and the demo controller
  
- PHP container has the DB CLI client so you can back-up your DB. see `.docker/php/Dockerfile`

2024-07-16:

- Added UserManager (CRUD including email verification and forgotten password)
- Some UI problems of the Yii application fixed
- Bootstrap Icons added
- BaseModel added, should be used for all models
- Login enabled

2024-07-30

- Enabled HTTPS for local development. See `.docker/php/Dockerfile` for details and instructions.
- Why?
  - If you are developing a PWA JS client, you need HTTPS so that Chrome allows for installation
  - HTTPS is also needed if you need to access hardware (camera) of your mobile device
  - If your client is using HTTPS, all rest requests (done by Axios for example) must also point to a HTTPS REST server

2024-10-16

- Added the "intl" module = editable list of languages and translations
- `Yii::t('app', 'My text')` can now be used for translating the web
- The language code is in the URL. See the new `app\modules\intl\components\UrlManager`.
- Added the "doc" module = files can be uploaded and managed
- Note: Modules "intl" and "doc" are just a draft, they need to be double-checked.
- ENV variable `BACKEND_ONLY` was temporarily disabled
- Certificates for HTTPS can be generated via Gii (PWA then can be installed on your phone and your server can use HTTPS)

2024-10-17

- RBAC was used:
  - Globally available roles are created in the user module.
  - Each controller then has its own set of permissions and roles.
  - In the admin module all these roles are assigned to global roles.
  - Global roles are then assigned to users in the user module.
  - See migrations, `RbacHelper` and the `BaseController`.

2024-10-18

- Roles can be assigned in the UI
- Roles can be created and edited via UI including their child-roles.

2024-10-19

- UI for role assigning and management was a little enhanced

2024-10-21

- Unit, functional and API tests, fixtures and code coverage are working
- To run tests, call for example:
  - `vendor/bin/codecept run`
  - `vendor/bin/codecept run unit`
  - `vendor/bin/codecept run unit models/UserTest`
  - `vendor/bin/codecept run unit models/UserTest:testFindUserById`
- If you also want the coverage, append these params `--coverage --coverage-xml --coverage-html`
  - Results will be in folder `tests/_output`
  - Coverage considers only files that are included in `codeception.yml`
- Also acceptance tests (using PhpBrowser) are now working
- Following dependencies are needed:
  - API tests: `composer require codeception/module-rest --dev`
  - Acceptance tests:
    - PhpBrowser `composer require codeception/module-phpbrowser --dev`
    - WebDriver`composer require codeception/module-webdriver --dev`
- Notes:
  - During tests `index-test.php` must be used, therefore the `showScriptName` must be `false`, otherwise all is
    redirected to index.php in `.htaccess`.
  - In file `index-test.php` there should be hardcoded `define('YII_ENV', 'test')` because then `captcha` will accept
    value `testme`. See `fixedVerifyCode` in the code.

2024-10-22

- The `Settings` module was added.
- Each value is defined by its `section` , `key` and the `language` (see the DB table)
- Value is obtained using: `Yii::$app->settings->get('section', 'key')`
- If the value supports more languages, then:
  - You can specify the language as the 3rd parameter, or
  - If not specified, language is picked automatically.
  - See the `SettingsComponent`
- DatePicker and DateTimePicker were added
  - Dependency `yiisoft/yii2-jui` used, it contains the DatePicker
  - DatePicker usage (2 ways):
    - `<?= $form->field($model, 'date')->widget(\yii\jui\DatePicker::class) ?>`
    - `<?= $form->field($model, 'date')->textInput(['class' => 'form-control datepicker']) ?>`
  - DateTimePicker usage:
    - `<?= $form->field($model, 'datetime')->textInput(['class' => 'form-control datetimepicker']) ?>`
  - See `DateTimePickerAsset` for details
  - See `di_container.php` and `init.js` for default values
- `PHP_CodeSniffer` and [yiisoft/yii2-coding-standards](https://github.com/yiisoft/yii2-coding-standards) were added.
  - To run the Sniffer, use command `vendor/bin/phpcs`
  - To run the Sniffer in particular folders, append them: `vendor/bin/phpcs {folder1} {folder2}`
  - To run the Sniffer and autofix errors, use command `vendor/bin/phpcbf`
  - Configuration is saved in file `phcs.xml`:
    - Yii2 coding standards are applied
    - Folders that should be scanned are defined
  - If you want, you can also use `gskema/phpcs-type-sniff` to enforce data types. 
    This dependency is present in composer.json, but is commented out in `phpcs.xml`. 
    Just uncomment, and you are ready to go! 
  - Note: Linter only checks if the code is valid, Sniffer also checks if it is beautiful.

2024-10-23

- Module `Messages` was added.
  - Can send email, chat messages and notifications.
  - Later also Whatsapp messages (needs Facebook configuration)
  - All messages should be sent on background by a job.
  - Demo:

```php
$user = \app\modules\user\models\User::findOne(1);
$user->email = 'enterExisting@address.com';
$user->save();
Yii::$app->messenger
    ->email('welcome', ['name' => 'Your name']) // You can also use methods chat() and notification()
    ->setSender($user)
    ->addRecipientTo($user)
    ->send(); // You can omit this row if you want to send the message later: `Message::findOne(1)->send()`
```

2024-10-24

- List of chat-messages and notifications can be displayed by clicking on icons in the main menu

2024-10-25

- Chat messages can be created via a form
- Modal window added, can be used to open list of chat-messages and notifications

2024-10-29

- `app\widgets\GridView` was added. It enhances standard GridView:
  - Above the GridView a row of buttons can be defined.
  - Below the GridView the page-size selector is placed. See its usage in the `search()` method below
  - Each row can contain a special "drag-sort" column. 
    You can drag and drop rows to change their order (which triggers an ajax call).
  - All is demonstrated in the list of users. 
    But as users do not have any order-column in the DB, the sorting algorithm will not be triggered.
  - Dependency `jquery-ui` had to be defined at the top of the page, see `JqueryUiAsset.php`.
  - To enable all these functions `GridViewModelBehavior` and `GridViewSortAction` were added.
- `CsvExporterComponent` was added - now you can export data to CSV using `Yii::$app->csvExporter`
  - Example is in the list of users
- Excel files can be created using `PhpSpreadsheet` and PDFs using `TCPDF`
  - Example in the list of users (PDF on each line, XLS in the Export menu)
  - Why older TCPDF? Because PhpSpreadsheets can natively use it to convert files to PDF.
    See [here](https://github.com/PHPOffice/PhpSpreadsheet/tree/master/src/PhpSpreadsheet/Writer/Pdf).
    Plus it has some advantages over MPDF and DOMPDF.

```php
public function search($params)
{       
  $query = User::find();
  $this->load($params);

  // Page size:
  // Call load() beforehand
  // ... and set 'pagination' below in the ActiveDataProvider
  if (!isset($this->gridPageSize)) {
      $this->gridPageSize = $this->gridPageSizeDefault;
  }

  // add conditions that should always apply here

  $dataProvider = new ActiveDataProvider([
      'query' => $query,
      'pagination' => ['pageSize' => (int)$this->gridPageSize],
  ]);
// ...
}
```

2024-10-30

- XLS can be created using the Queue job. Then notification is sent to the user.
- Env variable HOST_INFO is used in the console application for absolute URL creation.

2024-12

- API has alias, see env variable API_ALIAS
- API login enabled, `api_access_token` is then created
- Bearer token is processed and validated in demo API calls

2025-01

- Added module Location which contains Addresses, Companies and Locations within each company - Basics of stock maintenance.
- URL alias `@admin` is based on env variable `BACKEND_ALIAS`
- URL alias `@api` is based on env variable `API_ALIAS`
- Main menu enhanced, simple vertical menu enabled. See the layout file `main.php`
- Added dependency `yiisoft/yii2-httpclient` for making HTTP requests

# Used dependencies
- composer.json
  - yiisoft/yii2-queue 
  - enqueue/amqp-lib
  - vlucas/phpdotenv
  - symfony/google-mailer
  - twbs/bootstrap-icons
  - enyo/dropzone
  - yiisoft/yii2-imagine  
  - codeception/module-rest
  - codeception/module-phpbrowser
  - yiisoft/yii2-jui
  - squizlabs/php_codesniffer
  - yiisoft/yii2-coding-standards
  - gskema/phpcs-type-sniff
  - tecnickcom/tcpdf
  - phpoffice/phpspreadsheet
  - yiisoft/yii2-httpclient

# What you should do
- Copy file `.env.example` => `.env` and set correct data for your project. Do not save `.env` in git.
- Copy file `.env.example.php` => `.env.php` and set correct data for your project. Do not save `.env.php` in git.
- Check values in `.docker/php/php.ini` and `.docker/php/xdebug.ini` 
- Check values in `config/queue.php` (some values must correspond to `docker-compose.yml`)
- Call `docker-compose up` to start the web
- Run migrations in Docker `docker exec -it yii2plus_php yii migrate --interactive=0`
- If you want to use the Gmail SMTP, you need to enable 2-factor-authorization and use an App Password in your Google account. See [https://support.google.com/accounts/answer/185833](https://support.google.com/accounts/answer/185833). (Your gmail will then be used as the "from" address)

# Planned features
- Prepare a command that will process the queue if RabbitMQ is not used
- JsonFieldBehavior
- Not all users can use API. Add special permission (or user attribute). Can be used for disabling any client-application.
- Different themes for backend and frontend
    - Plus different colors for PROD and TEST environment.
- Enable Selenium ([WebDriver](https://codeception.com/docs/AcceptanceTests)) in acceptance tests. Now PhpBrowser is used.
- API can have more versions = submodules. Prepare /api/v1 instead of just /api?
- Demo of SQL: WITH RECURSIVE
- Create a widget for showing hundreds of checkboxes. Only modified will be sent via POST. (hidden 2nd form) 
- Each user can belong to a different company - more suppliers on one B2B portal. They need to share the system. 
  - Note: Multi-tenant can be done by more identical databases, this is not the case.

- Write documentation
- Demonstrate usage of Document and DocumentGroup by implementing user-photo (1x per user) and user-galleries or folders (many files per 1 gallery)

- `Message` module
  - Rename sys_message.id_user to id_sender
  - Rename sys_message_recipient.id_user to id_receiver
  - How to send messages to users without accounts? How do we find and store their phone or email?
  - User should have column "default language" and this should be used when sending automated messages/emails
  
- `Users` module
  - Move the "Role assignment" from the user-list to user-detail.

- Prepare new BootstrapComponent that will allow connection of independent modules via Event handlers, DI etc.
  - Note: Dependency injection is already used in di_container.php

- Does it make a sense to separate all the modules so they are isolated and can be easily removed?
  - They need to communicate, so it would require usage of the BootstrapComponent and DI.
  
- Write tests as soon as all functionalities are finished and approved.
- When exporting GridView to XLS, do not open new tab, but open the link in modal window with message "Success"
  
- Maybe later
  - Prepare config and orchestration for K8S (Helm, Helm charts)
  - Basic demo of compatible VueJS PWA application is [HERE](https://gitlab.com/radin.cerny/vuejs3-pwa-demo-plus)
    - Planned functionalities: the login screen, camera, photo upload, HTTPS, Axios, Pinia, Router, basic Boootstrap 5 layout - identical to PHP.
  - Prepare copy of the Gii-folder so programmers can easily modify gii-templates.
  - Demos of basic PDFs (CMR, invoice, VDA labels)
