<?php

namespace app\migrations;

use yii\db\Expression;
use yii\db\Migration;

/**
 * Class BaseMigration
 */
class BaseMigration extends Migration
{
    public $createColumnUserId = false;

    public $createColumnActive = false;

    public $activeDefaultValue = 1;

    public $userTable = '{{%user}}';

    public $userPk = ['id'];

    public $userIdCol = 'id_user';

    public $createColumnPosition = false;

    public $createColumnLang = false;

    public $columnLangRequired = false;

    public $langTable = '{{%intl_language}}';

    public $langPk = ['code'];

    public $idColName = 'id';

    public function createTable($table, $columns, $options = null)
    {
        $columns = array_merge($this->getDefaultColumns()['start'], $columns, $this->getDefaultColumns()['end']);

        parent::createTable($table, $columns, $this->getDefaultTableOptions() . $options);

        $tableName = trim($table, '{%}');

        if ($this->createColumnUserId) {
            $this->addForeignKey("fk-$tableName-x-$this->userIdCol", $table, $this->userIdCol, $this->userTable, $this->userPk);
        }

        $this->addForeignKey("fk-$tableName-x-created_by", $table, 'created_by', $this->userTable, $this->userPk);
        $this->addForeignKey("fk-$tableName-x-updated_by", $table, 'updated_by', $this->userTable, $this->userPk);
        $this->addForeignKey("fk-$tableName-x-deleted_by", $table, 'deleted_by', $this->userTable, $this->userPk);

        if ($this->createColumnLang) {
            $this->addForeignKey("fk-$tableName-x-lang", $table, 'lang', $this->langTable, $this->langPk);
        }
    }

    public function getDefaultColumns()
    {
        $result = [
            'start' => [
                $this->idColName => $this->primaryKey(),
                $this->userIdCol => $this->integer(),
            ],
            'end' => [
                'lang' => $this->columnLangRequired ? $this->string()->notNull() : $this->string(),
                'active' => $this->tinyInteger()->notNull()->defaultValue($this->activeDefaultValue),
                'position' => $this->integer()->notNull()->defaultValue(1), // PositionBehavior
                'created_by' => $this->integer(), // BlameableBehavior
                'updated_by' => $this->integer(), // BlameableBehavior
                'deleted_by' => $this->integer(), // SoftDeleteBehavior
                'created_at' => $this->dateTime()->notNull()->defaultValue(new Expression('NOW()')), // TimestampBehavior
                'updated_at' => $this->dateTime(), // TimestampBehavior
                'deleted_at' => $this->dateTime(), // SoftDeleteBehavior
            ]
        ];

        if (!$this->createColumnUserId) {
            unset($result['start'][$this->userIdCol]);
        }

        if (!$this->createColumnLang) {
            unset($result['end']['lang']);
        }

        if (!$this->createColumnActive) {
            unset($result['end']['active']);
        }

        if (!$this->createColumnPosition) {
            unset($result['end']['position']);
        }

        return $result;
    }

    public function getDefaultTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            // https://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            return 'CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE=InnoDB';
        }
        return '';
    }

    public function dropTable($table)
    {
        $tableName = trim($table, '{%}');

        if ($this->createColumnUserId) {
            $this->dropForeignKey("fk-$tableName-x-$this->userIdCol", $table);
        }

        $this->dropForeignKey("fk-$tableName-x-created_by", $table);
        $this->dropForeignKey("fk-$tableName-x-updated_by", $table);
        $this->dropForeignKey("fk-$tableName-x-deleted_by", $table);

        if ($this->createColumnUserId) {
            $this->dropForeignKey("fk-$tableName-x-lang", $table);
        }

        parent::dropTable($table);
    }
}
