<?php

use app\behaviors\ChangeLogBehavior;
use app\modules\user\models\User;
use yii\db\Migration;

/**
 * Handles the creation of table `change_log`.
 */
class m240205_140000_create_change_log_table extends Migration
{
    public function safeUp()
    {
        $this->createTable(ChangeLogBehavior::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'table_name' => $this->string(255)->notNull(),
            'record_id' => $this->integer()->notNull(),
            'attribute' => $this->string(255)->notNull(),
            'old_value' => $this->text(),
            'new_value' => $this->text(),
            'updated_by' => $this->integer(),
            'updated_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('idx-change_log-table_name', ChangeLogBehavior::TABLE_NAME, 'table_name');
        $this->createIndex('idx-change_log-record_id', ChangeLogBehavior::TABLE_NAME, 'record_id');
        $this->createIndex('idx-change_log-updated_by', ChangeLogBehavior::TABLE_NAME, 'updated_by');

        $this->addForeignKey('fk-change_log-updated_by', ChangeLogBehavior::TABLE_NAME, 'updated_by', User::tableName(), 'id');
    }

    public function safeDown()
    {
        $this->dropTable(ChangeLogBehavior::TABLE_NAME);
    }
}