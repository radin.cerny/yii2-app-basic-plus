<?php

/**
 * This view is used by console/controllers/MigrateController.php.
 *
 * The following variables are available in this view:
 */

/* @var $className string the new migration class name without namespace */
/* @var $namespace string the new migration class namespace */
/* @var $name string the name of the table you used in the migrate/create command */
/* @var $table string usually: {{%}} */
/* @var $tableComment string usually: "" */
/* @var $fields array usually: [] */
/* @var $foreignKeys array usually: [] */

echo "<?php\n";
if (!empty($namespace)) {
    echo "\nnamespace {$namespace};\n";
}
?>

// use app\migrations\BaseMigration;
use yii\db\Migration;

/**
 * Class <?= $className . "\n" ?>
 */
class <?= $className ?> extends Migration // BaseMigration
{

    // public $createColumnUserId = true;
    // public $createColumnActive = true;
    // public $createColumnPosition = true;
    // public $createColumnLang = true;

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // $tableName = '{{%<?= $name ?>}}';
        // $tableNameTrim = '<?= $name ?>'; // trim($tableName, '{%}')

        // $this->createTable($tableName, [
        //     'id_parent' => $this->integer(),
        //     'name' => $this->string(),
        //     'age' => $this->integer(),
        //     'active' => $this->boolean(),
        //     'from' => $this->dateTime(),
        // ]);

        // $this->addForeignKey("fk-$tableNameTrim-parent", $tableName, 'id_parent', '{{%ref_table}}', 'id');
        // $this->createIndex("unique-$tableNameTrim-name", $tableName, ['name'], true);

        // $this->batchInsert($tableName, ['name', 'age', 'active', 'id_parent'], [
        //     ['Name1', 25, 1, null],
        //     ['Name2', 35, 1, null],
        // ]);

        // // getLastInsertID() returns ID of the first batch-inserted row above
        // $firstId = $this->db->getLastInsertID();

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "<?= $className ?> cannot be reverted.\n";

        // $tableName = '{{%<?= $name ?>}}';
        // $this->dropTable($tableName);

        return false;
    }
}
