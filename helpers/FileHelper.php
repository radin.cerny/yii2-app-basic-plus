<?php


namespace app\helpers;


class FileHelper extends \yii\helpers\FileHelper
{
    public static function getFilename($path, $removeExtension = false)
    {
        if ($removeExtension) {
            $info = pathinfo($path);
            return basename($path, '.' . $info['extension']);
        }

        return basename($path);
    }

    public static function getExtension($file)
    {
        if (($ext = pathinfo($file, PATHINFO_EXTENSION)) !== '') {
            return strtolower($ext);
        }
        return '';
    }
}