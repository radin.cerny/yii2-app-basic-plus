<?php


namespace app\helpers;

class Html extends \yii\helpers\Html
{
    public static function getMenuIconLink($itemCount, $iconClass, $url, $title)
    {
        $text = '';
        if ($itemCount > 0) {
            $text = '<span class="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-danger p-1">' . $itemCount . '</span>';
        }
        return Html::a('<span class="bi ' . $iconClass . ' position-relative">' . $text . '</span>', $url, [
            'class' => 'nav-link openInMyModal',
            'title' => $title,
        ]);
    }

    public static function getMenuLangLink($langCode, $flagPath)
    {
        return Html::img($flagPath, ['style' => 'width:20px;margin-bottom:0.2rem;']) . '&nbsp;' . $langCode;
    }

    public static function h(int $level, string $content, array $options = [])
    {
        $tag = 'h' . $level;
        return self::beginTag($tag, $options) . $content . self::endTag($tag);
    }

}