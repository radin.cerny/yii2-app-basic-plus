<?php

namespace app\assets;

use yii\web\AssetBundle;
use yii\web\View;

class JqueryUiAsset extends AssetBundle
{
    public $sourcePath = '@vendor/bower-asset/jquery-ui';

    public $js = [
        'jquery-ui.min.js',
    ];

    public $jsOptions = ['position' => View::POS_HEAD];

    public $depends = [
        'app\assets\AppAsset',
    ];
}
