<?php

/** @var yii\web\View $this */

/** @var string $content */

use app\assets\AppAsset;
use app\assets\BootstrapIconsAsset;
use app\assets\DateTimePickerAsset;
use app\assets\JqueryUiAsset;
use app\helpers\Html as myHtml;
use app\modules\intl\models\Language;
use app\widgets\Alert;
use app\widgets\Modal;
use yii\bootstrap5\Breadcrumbs;
use yii\bootstrap5\Html;
use yii\bootstrap5\Nav;
use yii\bootstrap5\NavBar;

AppAsset::register($this);
BootstrapIconsAsset::register($this);
DateTimePickerAsset::register($this);
JqueryUiAsset::register($this);

$this->registerCsrfMetaTags();
$this->registerMetaTag(['charset' => Yii::$app->charset], 'charset');
$this->registerMetaTag(['name' => 'viewport', 'content' => 'width=device-width, initial-scale=1, shrink-to-fit=no']);
$this->registerMetaTag(['name' => 'description', 'content' => $this->params['meta_description'] ?? '']);
$this->registerMetaTag(['name' => 'keywords', 'content' => $this->params['meta_keywords'] ?? '']);
$this->registerLinkTag(['rel' => 'icon', 'type' => 'image/x-icon', 'href' => Yii::getAlias('@web/favicon.ico')]);

$mainMenuVertical = Yii::$app->settings->get('app', 'mainMenuVertical');

$mainMenuItems = [
    ['label' => 'Home', 'url' => ['/site/index']],
    ['label' => 'About', 'url' => ['/site/about']],
    ['label' => 'Contact', 'url' => ['/site/contact']],
    ['label' => 'Admin', 'url' => ['@admin/default/index']],
    ['label' => 'API', 'url' => ['@api/default/index']],
    ['label' => 'Users', 'url' => ['@admin/user/user-manager']],
    ['label' => 'Roles', 'url' => ['@admin/user/role']],
    ['label' => 'Settings', 'url' => ['@admin/settings/settings/index']],
    ['label' => 'Messages', 'url' => ['@admin/message/message/index']],
    ['label' => 'Intl', 'items' => [
        ['label' => 'Languages', 'url' => ['@admin/intl/language']],
        ['label' => 'Translations', 'url' => ['@admin/intl/translation']],
        ['label' => 'Translations src', 'url' => ['@admin/intl/translation-source']],
    ]],
    ['label' => 'Documents', 'items' => [
        ['label' => 'Document', 'url' => ['@admin/documents/document']],
        ['label' => 'Document Group', 'url' => ['@admin/documents/document-group']],
        ['label' => 'Document Group Item', 'url' => ['@admin/documents/document-group-item']],
    ]],
    ['label' => 'Locations', 'items' => [
        ['label' => 'Addresses', 'url' => ['@admin/location/address']],
        ['label' => 'Companies', 'url' => ['@admin/location/company']],
        ['label' => 'Locations', 'url' => ['@admin/location/location']],
    ]],
];

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <link rel="stylesheet" href="/style.css">
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header id="header">
    <?php
    $navbarExpandClass = $mainMenuVertical ? 'navbar-expand' : 'navbar-expand-md';
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => $navbarExpandClass . ' navbar-dark bg-dark fixed-top',
            'style' => 'z-index:999;',
        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav me-auto'],
        'items' => $mainMenuVertical ? [] : $mainMenuItems,
    ]);

    $langCode = strtoupper(Yii::$app->urlManager->convertLanguageCodeForUi(Yii::$app->language));

    $rightMenuItems = [
        [
            'label' => myHtml::getMenuLangLink($langCode, Language::getFlagPathByLangCode(Yii::$app->language)),
            'encode' => false,
            'items' => Language::getLanguageSwitchData(),
        ]
    ];

    if (Yii::$app->user->isGuest) {
        $loginLink = Html::a(Yii::t('app', 'Login'), ['@admin/login'], ['class' => 'nav-link']);
        $rightMenuItems[] = $loginLink;
    } else {
        $messagesLink = myHtml::getMenuIconLink(count(Yii::$app->user->identity->getChatMessages()), 'bi-chat-fill', ['/admin/message/chat'], Yii::t('app', 'Chat messages'));
        $rightMenuItems[] = $messagesLink;

        $notificationsLink = myHtml::getMenuIconLink(count(Yii::$app->user->identity->getNotifications()), 'bi-bell-fill', ['/admin/message/notification'], Yii::t('app', 'Notifications'));
        $rightMenuItems[] = $notificationsLink;

        $username = ' (' . Yii::$app->user->identity->username . ')';
        $logoutLink = Html::a(Yii::t('app', 'Logout') . $username, ['@admin/logout'], [
            'title' => Yii::t('app', 'Logout'),
            'class' => 'nav-link',
        ]);
        $rightMenuItems[] = $logoutLink;
    }

    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ms-auto'],
        'items' => $rightMenuItems,
    ]);

    NavBar::end();
    ?>
</header>

<!-- This wrapper is needed only if the vertical menu is ON -->
<div class="d-flex flex-row flex-grow-1">
    <?php if ($mainMenuVertical): ?>
        <div style="top: 0;left: 0;padding-top: 20px;">
            <?php
            NavBar::begin([
                'options' => ['class' => 'navbar-expand navbar-dark bg-dark h-100 align-items-start']
            ]);
            echo Nav::widget([
                'options' => ['class' => 'navbar-nav flex-column'],
                'items' => $mainMenuItems,
            ]);
            NavBar::end();
            ?>
        </div>
    <?php endif; ?>

    <!-- flex-grow-1 is needed only if the wrapper is present -->
    <!-- Why was originally used also class "flex-shrink-0" ? ... it causes wide forms to grow beyond the page edges when <row> and <col-***> is used, For example modules/location/views/location/_form.php -->
    <main id="main" class="flex-grow-1" role="main">
        <div class="container">
            <?php if (!empty($this->params['breadcrumbs'])): ?>
                <?= Breadcrumbs::widget(['links' => $this->params['breadcrumbs']]) ?>
            <?php endif ?>
            <?= Alert::widget() ?>
            <?= $content ?>
        </div>
    </main>
</div>

<!--<footer id="footer" class="mt-auto py-3 bg-light fixed-bottom">-->
<!--    <div class="container">-->
<!--        <div class="row text-muted">-->
<!--            <div class="col-md-6 text-center text-md-start">&copy; My Company --><? //= date('Y') ?><!--</div>-->
<!--        </div>-->
<!--    </div>-->
<!--</footer>-->

<?= Modal::widget(); ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
