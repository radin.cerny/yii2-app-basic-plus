// https://trentrichardson.com/examples/timepicker/
// https://github.com/trentrichardson/jQuery-Timepicker-Addon
$(document).on('click', 'input.datetimepicker', function (event) {
    // Why onclick? Because some inputs are later created by ajax.
    // $('input.datetime').attr('autocomplete', 'off');
    if (!$(this).hasClass('hasDatepicker')) {
        $(this).datetimepicker({
            dateFormat: 'yy-mm-dd',
            timeFormat: 'HH:mm:00',
            beforeShow: function () {
                $(".ui-datepicker").css('font-size', 12)
            }
        })
    }
    $(this).datetimepicker('show');
})
$(document).on('click', 'input.datepicker', function (event) {
    // Why onclick? Because some inputs are later created by ajax.
    // $('input.datetime').attr('autocomplete', 'off');
    if (!$(this).hasClass('hasDatepicker')) {
        $(this).datepicker({
            dateFormat: 'yy-mm-dd',
            beforeShow: function () {
                $(".ui-datepicker").css('font-size', 12)
            }
        })
    }
    $(this).datepicker('show');
})
