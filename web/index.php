<?php

// Explanation:
// I cannot use "vlucas/phpdotenv" now, as autoload hasn't been called yet.
// Because dependencies can execute some initialization code during autoload,
// I didnt want ta place "autoload" before these constants are correctly set.
// I also wanted to avoid using the "init" command that is in the advanced Yii2 application.
// Why not "init"? Because when my "RabbitMQ worker container" is starting, it calls "yii queue/listen" and ...
// ... if I use the "init" script then file "yii" will not exist when containers are starting. Problem.
$env = require __DIR__ . '/../.env.example.php';
if (file_exists(__DIR__ . '/../.env.php')) {
    $env = require __DIR__ . '/../.env.php';
}
defined('YII_DEBUG') or define('YII_DEBUG', $env['YII_DEBUG']);
defined('YII_ENV') or define('YII_ENV', $env['YII_ENV']);

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';
require __DIR__ . '/../config/env.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
